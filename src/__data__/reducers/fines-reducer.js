import _ from 'lodash'

import {
    GET_FINES_COMPLETED,
    GET_FINES_STARTED,
    GET_FINES_FAILED,
    SELECT_FINE,
    DESELECT_FINE,
    SELECT_ALL_FINES,
    DESELECT_ALL_FINES,
    SET_ORGANIZATION_FILTER,
} from '../action-types'

const initialState = {
    isLoading: true,
    responseFines: null,
    prevParams: null,
    organizationFilter: null,
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_FINES_STARTED:
            return {
                ...state,
                prevParams: action.payload.params,
                isLoading: true,
            }
        case GET_FINES_FAILED:
            return {
                ...state,
                prevParams: null,
                isLoading: false,
            }
        case GET_FINES_COMPLETED:
            return {
                ...state,
                responseFines: action.payload.response,
                isLoading: false,
                organizationFilter: null,
            }

        case SELECT_ALL_FINES: {
            const responseFinesClone = _.cloneDeep(state.responseFines)
            const fines = _.get(responseFinesClone, 'body.fines', [])
            responseFinesClone.body.fines = fines.map((item) => ({ ...item, checked: true, }))

            return {
                ...state,
                responseFines: responseFinesClone
            }
        }
        case DESELECT_ALL_FINES: {
            const responseFinesClone = _.cloneDeep(state.responseFines)
            const fines = _.get(responseFinesClone, 'body.fines', [])
            responseFinesClone.body.fines = fines.map((item) => ({ ...item, checked: false, }))

            return {
                ...state,
                responseFines: responseFinesClone
            }
        }
        case SELECT_FINE: {
            const responseFinesClone = _.cloneDeep(state.responseFines)
            const fines = _.get(responseFinesClone, 'body.fines', [])
            const foundFine = fines.find((item) => item.uin === action.uin)

            if (foundFine) {
                foundFine.checked = true
            }

            return {
                ...state,
                responseFines: responseFinesClone
            }
        }
        case DESELECT_FINE: {
            const responseFinesClone = _.cloneDeep(state.responseFines)
            const fines = _.get(responseFinesClone, 'body.fines', [])
            const foundFine = fines.find((item) => item.uin === action.uin)

            if (foundFine) {
                foundFine.checked = false
            }

            return {
                ...state,
                responseFines: responseFinesClone
            }
        }

        case SET_ORGANIZATION_FILTER: {
            return {
                ...state,
                organizationFilter: action.param.organizationId
            }
        }
        default:
            return state
    }
}
