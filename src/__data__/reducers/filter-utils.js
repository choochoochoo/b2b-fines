import dayjs from 'dayjs'

import { formatDate } from '../../components/general'

export const filterToForm = (filters) => {
    const newForm = {}

    filters.forEach((filter) => {

        switch (filter.type) {
            case 'calendar':
                newForm[`${filter.key}__startDate`] = formatDate(filter.startDate)
                newForm[`${filter.key}__endDate`] = formatDate(filter.endDate)
                break
            case 'checkbox':
                newForm[filter.key] = filter.selected
                break
            default: {
                filter.set.forEach((item) => {
                    newForm[`${filter.key}__${item.key}`] = item.selected
                })
                break
            }
        }
    })

    return newForm
}

export const toServerDate = (date) => {
    const parsedDate = dayjs(date, 'DD.MM.YYYY')
    if (parsedDate.isValid()) {
        return parsedDate.format('YYYY-MM-DD')
    }

    return null
}

export const formToFilter = (form, originalFilters) => {
    const filters = []

    originalFilters.forEach((filter) => {
        switch (filter.type) {
            case 'calendar':
                filters.push({
                    key: filter.key,
                    startDate: toServerDate(form[`${filter.key}__startDate`]),
                    endDate: toServerDate(form[`${filter.key}__endDate`]),
                })
                break
            case 'checkbox':
                filters.push({
                    key: filter.key,
                    selected: form[filter.key],
                })

                break
            default: {
                const set = filter.set.map((item) => ({
                    key: item.key,
                    selected: form[`${filter.key}__${item.key}`]
                }))

                filters.push({
                    key: filter.key,
                    set,
                })
                break
            }
        }
    })

    return filters
}
