import {
    LOGIN_FAILED,
} from '../action-types'

const initialState = {
    responseLogin: null,
}

export default (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_FAILED:
            return {
                ...state,
                responseLogin: action.payload,
            }

        default:
            return state
    }
}
