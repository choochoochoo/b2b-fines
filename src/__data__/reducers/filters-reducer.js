import _ from 'lodash'

import {
    GET_FINES_COMPLETED,
    SET_DEFAULT_FILTER,
    SET_FILTER,
    CLEAR_FILTER,
    CLEAR_ALL_FILTERS,
    SET_ERROR_FILTER,
} from '../action-types'

import { filterToForm } from './filter-utils'

const initialState = {
    form: {},
    filterError: {},
}

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_FILTER: {
            const newForm = { ...state.form }
            newForm[action.param.key] = action.param.value

            return {
                ...state,
                form: newForm,
            }
        }
        case SET_DEFAULT_FILTER: {
            return {
                ...state,
                form: filterToForm(action.filters),
            }
        }
        case CLEAR_FILTER: {

            const newForm1 = { ...state.form }

            // eslint-disable-next-line no-plusplus
            for (let i = 0; i < action.param.list.length; i++) {
                const item = action.param.list[i]
                newForm1[item.key] = item.value
            }

            return {
                ...state,
                form: newForm1,
            }
        }
        case CLEAR_ALL_FILTERS: {
            const newForm2 = { ...state.form }
            const keys = Object.keys(newForm2)

            // eslint-disable-next-line no-plusplus
            for (let i = 0; i < keys.length; i++) {
                const currentKey = keys[i]
                if (_.isBoolean(newForm2[currentKey])) {
                    newForm2[currentKey] = false
                } else {
                    newForm2[currentKey] = null
                }
            }

            return {
                ...state,
                form: newForm2,
            }
        }
        case SET_ERROR_FILTER: {
            return {
                ...state,
                filterError: action.filterError,
            }
        }
        case GET_FINES_COMPLETED: {
            const filters = _.get(action, 'payload.response.body.filters')

            return {
                ...state,
                form: filters ? filterToForm(filters) : {}
            }
        }

        default:
            return state
    }
}
