import {
    GET_FINE_COMPLETED,
    GET_FINE_STARTED,
    GET_FINE_FAILED,
    OPEN_FINE_DETAILS_DIALOG,
    CLOSE_FINE_DETAILS_DIALOG,
} from '../action-types'

const initialState = {
    response: {},
    isLoading: false,
    isError: false,
    isDialogOpen: false,
    uin: null,
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_FINE_STARTED: {
            return {
                ...state,
                response: {},
                isError: false,
                isLoading: true,
            }
        }
        case GET_FINE_FAILED: {
            return {
                ...state,
                response: {},
                isError: true,
                isLoading: false,
            }
        }
        case GET_FINE_COMPLETED: {
            return {
                ...state,
                response: action.payload,
                isLoading: false,
                isError: false,
                isDialogOpen: true,
            }
        }
        case OPEN_FINE_DETAILS_DIALOG: {
            return {
                ...state,
                uin: action.uin,
            }
        }
        case CLOSE_FINE_DETAILS_DIALOG: {
            return {
                ...state,
                isDialogOpen: false,
                uin: null,
            }
        }

        default:
            return state
    }
}
