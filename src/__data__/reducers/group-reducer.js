import _ from 'lodash'

import {
    GET_GROUP_COMPLETED,
    GET_GROUP_STARTED,
    OPEN_REMOVE_GROUP_DIALOG,
    CLOSE_REMOVE_GROUP_DIALOG,
    OPEN_EDIT_GROUP_DIALOG,
    CLOSE_EDIT_GROUP_DIALOG,
    EDIT_GROUP_COMPLETED,
    SELECT_GROUP_VEHICLE,
    DESELECT_GROUP_VEHICLE,
    SELECT_ALL_GROUP_VEHICLES,
    DESELECT_ALL_GROUP_VEHICLES,
} from '../action-types'

const initialState = {
    responseGroup: null,
    groupId: null,
    isLoading: false,
    isOpenRemoveGroupDialog: false,
    isOpenEditGroupDialog: false,
}

// eslint-disable-next-line complexity
export default (state = initialState, action) => {
    switch (action.type) {
        case GET_GROUP_COMPLETED:
            return {
                ...state,
                responseGroup: action.payload.response,
                groupId: action.payload.requestParam.groupId,
                isLoading: false,
            }
        case GET_GROUP_STARTED:
            return {
                ...state,
                isLoading: true,
            }
        case OPEN_REMOVE_GROUP_DIALOG:
            return {
                ...state,
                isOpenRemoveGroupDialog: true,
            }
        case CLOSE_REMOVE_GROUP_DIALOG:
            return {
                ...state,
                isOpenRemoveGroupDialog: false,
            }
        case OPEN_EDIT_GROUP_DIALOG:
            return {
                ...state,
                isOpenEditGroupDialog: true,
            }
        case CLOSE_EDIT_GROUP_DIALOG:
            return {
                ...state,
                isOpenEditGroupDialog: false,
            }
        case EDIT_GROUP_COMPLETED: {
            const responseGroupClone = _.cloneDeep(state.responseGroup)
            const groupName = _.get(action, 'payload.requestParam.name')
            const groupId = _.get(action, 'payload.requestParam.groupId')

            const groups = _.get(responseGroupClone, 'body.groups', [])
            const foundGroup = groups.find((item) => item.id.toString() === groupId.toString())

            if (foundGroup) {
                foundGroup.name = groupName
            }

            const vehicles = _.get(responseGroupClone, 'body.vehicles')

            vehicles.forEach((item) => {
                _.set(item, 'groupsIdentifiers[0]', groupName)
            })

            return {
                ...state,
                responseGroup: responseGroupClone
            }
        }
        case SELECT_ALL_GROUP_VEHICLES: {
            const responseGroupClone = _.cloneDeep(state.responseGroup)
            const vehicles = _.get(responseGroupClone, 'body.vehicles', [])
            responseGroupClone.body.vehicles = vehicles.map((item) => ({ ...item, checked: true, }))

            return {
                ...state,
                responseGroup: responseGroupClone
            }
        }
        case DESELECT_ALL_GROUP_VEHICLES: {
            const responseGroupClone = _.cloneDeep(state.responseGroup)
            const vehicles = _.get(responseGroupClone, 'body.vehicles', [])
            responseGroupClone.body.vehicles = vehicles.map((item) => ({ ...item, checked: false, }))

            return {
                ...state,
                responseGroup: responseGroupClone
            }
        }
        case SELECT_GROUP_VEHICLE: {
            const responseGroupClone = _.cloneDeep(state.responseGroup)
            const vehicles = _.get(responseGroupClone, 'body.vehicles', [])
            const foundVehicle = vehicles.find((item) => item.id === action.id)

            if (foundVehicle) {
                foundVehicle.checked = true
            }

            return {
                ...state,
                responseGroup: responseGroupClone
            }
        }
        case DESELECT_GROUP_VEHICLE: {
            const responseGroupClone = _.cloneDeep(state.responseGroup)
            const vehicles = _.get(responseGroupClone, 'body.vehicles', [])
            const foundVehicle = vehicles.find((item) => item.id === action.id)

            if (foundVehicle) {
                foundVehicle.checked = false
            }

            return {
                ...state,
                responseGroup: responseGroupClone
            }
        }
        default:
            return state
    }
}
