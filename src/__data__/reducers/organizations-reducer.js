import _ from 'lodash'

import {
    GET_ORGANIZATIONS_COMPLETED,
    CREATE_ORGANIZATIONS_COMPLETED,
    REMOVE_ORGANIZATIONS_COMPLETED,
    EDIT_ORGANIZATIONS_COMPLETED,
    OPEN_EDIT_ORGANIZATION_DIALOG,
    CLOSE_EDIT_ORGANIZATION_DIALOG,
    OPEN_REMOVE_ORGANIZATION_DIALOG,
    CLOSE_REMOVE_ORGANIZATION_DIALOG,

    CREATE_BRANCH_COMPLETED,
    EDIT_BRANCH_COMPLETED,
    REMOVE_BRANCH_COMPLETED,
} from '../action-types'

const initialState = {
    responseOrganizations: null,

    isOpenEditOrganizationDialog: false,
    editOrganizationDialogId: null,

    isOpenRemoveOrganizationDialog: false,
    removeOrganizationDialogId: null,
    removeOrganizationDialogName: null,
}

// eslint-disable-next-line complexity
export default (state = initialState, action) => {
    switch (action.type) {
        case GET_ORGANIZATIONS_COMPLETED:
            return {
                ...state,
                responseOrganizations: action.payload.response,
            }
        case CREATE_ORGANIZATIONS_COMPLETED: {
            const responseOrganizationsClone = _.cloneDeep(state.responseOrganizations)
            const newOrganization = _.get(action, 'payload.response.body')
            const organizations = _.get(responseOrganizationsClone, 'body.organizations')

            organizations.push(newOrganization)

            return {
                ...state,
                responseOrganizations: responseOrganizationsClone
            }
        }
        case EDIT_ORGANIZATIONS_COMPLETED: {
            const responseOrganizationsClone = _.cloneDeep(state.responseOrganizations)

            const organizationId = _.get(action, 'payload.requestParam.organizationId')
            const name = _.get(action, 'payload.requestParam.name')
            const address = _.get(action, 'payload.requestParam.address')
            const kpp = _.get(action, 'payload.requestParam.kpp')

            const organizations = _.get(responseOrganizationsClone, 'body.organizations')
            const editedOrganization = organizations.find((item) => item.id.toString() === organizationId.toString())
            editedOrganization.name = name
            editedOrganization.address = address
            editedOrganization.kpp = kpp

            return {
                ...state,
                responseOrganizations: responseOrganizationsClone
            }
        }
        case REMOVE_ORGANIZATIONS_COMPLETED: {
            const responseOrganizationsClone = _.cloneDeep(state.responseOrganizations)

            const organizationId1 = _.get(action, 'payload.requestParam.organizationId')
            const organizations1 = _.get(responseOrganizationsClone, 'body.organizations')

            _.remove(organizations1, (item) => item.id.toString() === organizationId1.toString())

            return {
                ...state,
                responseOrganizations: responseOrganizationsClone
            }
        }
        case OPEN_EDIT_ORGANIZATION_DIALOG:
            return {
                ...state,
                isOpenEditOrganizationDialog: true,
                editOrganizationDialogId: action.id
            }
        case CLOSE_EDIT_ORGANIZATION_DIALOG:
            return {
                ...state,
                isOpenEditOrganizationDialog: false,
                editOrganizationDialogId: null,
            }
        case OPEN_REMOVE_ORGANIZATION_DIALOG:
            return {
                ...state,
                isOpenRemoveOrganizationDialog: true,
                removeOrganizationDialogId: action.organization.id,
                removeOrganizationDialogName: action.organization.name,
            }
        case CLOSE_REMOVE_ORGANIZATION_DIALOG:
            return {
                ...state,
                isOpenRemoveOrganizationDialog: false,
                removeOrganizationDialogId: null,
                removeOrganizationDialogName: null,
            }
        case CREATE_BRANCH_COMPLETED:
        case EDIT_BRANCH_COMPLETED: {
            const responseOrganizationsClone2 = _.cloneDeep(state.responseOrganizations)
            const organizationId2 = _.get(action, 'payload.response.body.id')
            const newBranches = _.get(action, 'payload.response.body.branches')

            const organizations2 = _.get(responseOrganizationsClone2, 'body.organizations')
            const foundOrganization2 = organizations2.find((item) => item.id === organizationId2)

            foundOrganization2.branches = newBranches

            return {
                ...state,
                responseOrganizations: responseOrganizationsClone2
            }
        }
        case REMOVE_BRANCH_COMPLETED: {
            const responseOrganizationsClone3 = _.cloneDeep(state.responseOrganizations)

            const branchId = _.get(action, 'payload.requestParam.branchId')
            const organizationId3 = _.get(action, 'payload.requestParam.organizationId')
            const organizations3 = _.get(responseOrganizationsClone3, 'body.organizations')
            const foundOrganization3 = organizations3.find((item) => item.id.toString() === organizationId3.toString())

            _.remove(foundOrganization3.branches, (item) => item.id.toString() === branchId.toString())

            return {
                ...state,
                responseOrganizations: responseOrganizationsClone3
            }
        }
        default:
            return state
    }
}
