import {
    GET_PROFILE_COMPLETED,
    OPEN_DIALOG_ERROR,
    CLOSE_DIALOG_ERROR,
    SHOW_LOADER,
    HIDE_LOADER,
    LOGIN_COMPLETED,
} from '../action-types'

const initialState = {
    responseProfile: null,
    dialogOpen: false,
    dialogTitle: null,
    dialogMessage: null,
    loaderOpened: false,
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_PROFILE_COMPLETED:
            return {
                ...state,
                responseProfile: action.payload,
            }
        case OPEN_DIALOG_ERROR:
            return {
                ...state,
                dialogOpen: true,
                dialogTitle: action.title,
                dialogMessage: action.message
            }
        case LOGIN_COMPLETED:
        case CLOSE_DIALOG_ERROR:
            return {
                ...state,
                dialogOpen: false,
                dialogTitle: null,
                dialogMessage: null
            }
        case SHOW_LOADER:
            return {
                ...state,
                loaderOpened: true,
            }
        case HIDE_LOADER:
            return {
                ...state,
                loaderOpened: false,
            }
        default:
            return state
    }
}
