import _ from 'lodash'

export const changeVehiclesGroup = (documentsIds, vehicles, groupName) => {
    documentsIds.forEach((id) => {
        const foundVehicle = vehicles.find((item) => item.id === id)
        _.set(foundVehicle, 'groupsIdentifiers[0]', groupName)
    })
}

export const getDataForEditGroup = (action, responseVehiclesClone) => {
    const groups = _.get(action, 'payload.response.body.groups')
    const documentsIds = _.get(action, 'payload.requestParam.documentsIds')
    const vehicles = _.get(responseVehiclesClone, 'body.vehicles')
    const groupId = _.get(action, 'payload.requestParam.groupId')
    const groupName = _.get(action, 'payload.requestParam.name')

    return {
        groups,
        documentsIds,
        vehicles,
        groupId,
        groupName,
    }
}
