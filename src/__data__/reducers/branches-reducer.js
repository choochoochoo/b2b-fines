import {
    OPEN_CREATE_BRANCH_DIALOG,
    CLOSE_CREATE_BRANCH_DIALOG,
    OPEN_EDIT_BRANCH_DIALOG,
    CLOSE_EDIT_BRANCH_DIALOG,
    OPEN_REMOVE_BRANCH_DIALOG,
    CLOSE_REMOVE_BRANCH_DIALOG,
} from '../action-types'

const initialState = {
    isOpenCreateBranchDialog: false,

    isOpenEditBranchDialog: false,
    editBranchDialogId: null,

    isOpenRemoveBranchDialog: false,
    removeBranchDialogId: null,
    removeBranchDialogName: null,
}

export default (state = initialState, action) => {
    switch (action.type) {
        case OPEN_CREATE_BRANCH_DIALOG:
            return {
                ...state,
                isOpenCreateBranchDialog: true,
            }
        case CLOSE_CREATE_BRANCH_DIALOG:
            return {
                ...state,
                isOpenCreateBranchDialog: false,
            }
        case OPEN_EDIT_BRANCH_DIALOG:
            return {
                ...state,
                isOpenEditBranchDialog: true,
                editBranchDialogId: action.id,
            }
        case CLOSE_EDIT_BRANCH_DIALOG:
            return {
                ...state,
                isOpenEditBranchDialog: false,
                editBranchDialogId: null,
            }
        case OPEN_REMOVE_BRANCH_DIALOG:
            return {
                ...state,
                isOpenRemoveBranchDialog: true,
                removeBranchDialogId: action.branch.id,
                removeBranchDialogName: action.branch.name,
                removeBranchDialogOrganizationId: action.branch.organizationId,
            }
        case CLOSE_REMOVE_BRANCH_DIALOG:
            return {
                ...state,
                isOpenRemoveBranchDialog: false,
                removeBranchDialogId: null,
                removeBranchDialogName: null,
                removeBranchDialogOrganizationId: null,
            }
        default:
            return state
    }
}
