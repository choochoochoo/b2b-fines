import _ from 'lodash'

import {
    GET_VEHICLES_COMPLETED,
    GET_VEHICLES_STARTED,
    OPEN_CREATE_GROUP_DIALOG,
    CLOSE_CREATE_GROUP_DIALOG,
    OPEN_MOVE_VEHICLES_DIALOG,
    CLOSE_MOVE_VEHICLES_DIALOG,
    OPEN_MOVE_CREATE_VEHICLES_DIALOG,
    CLOSE_MOVE_CREATE_VEHICLES_DIALOG,
    SELECT_ALL_VEHICLES,
    DESELECT_ALL_VEHICLES,
    SELECT_VEHICLE,
    DESELECT_VEHICLE,
    CREATE_GROUP_COMPLETED,
    EDIT_GROUP_ADDING_VEHICLES_COMPLETED,
} from '../action-types'

import { changeVehiclesGroup, getDataForEditGroup } from './utils'

const initialState = {
    responseVehicles: null,
    isLoading: true,
    isOpenCreateGroupDialog: false,
    isOpenMoveVehiclesDialog: false,
    isOpenMoveCreateVehiclesDialog: false,
}

// eslint-disable-next-line complexity
export default (state = initialState, action) => {
    switch (action.type) {
        case GET_VEHICLES_COMPLETED:
            return {
                ...state,
                responseVehicles: action.payload,
                isLoading: false,
            }
        case GET_VEHICLES_STARTED:
            return {
                ...state,
                isLoading: true,
            }
        case OPEN_CREATE_GROUP_DIALOG:
            return {
                ...state,
                isOpenCreateGroupDialog: true,
            }
        case CLOSE_CREATE_GROUP_DIALOG:
            return {
                ...state,
                isOpenCreateGroupDialog: false,
            }
        case OPEN_MOVE_VEHICLES_DIALOG:
            return {
                ...state,
                isOpenMoveVehiclesDialog: true,
            }
        case CLOSE_MOVE_VEHICLES_DIALOG:
            return {
                ...state,
                isOpenMoveVehiclesDialog: false,
            }
        case OPEN_MOVE_CREATE_VEHICLES_DIALOG:
            return {
                ...state,
                isOpenMoveCreateVehiclesDialog: true,
            }
        case CLOSE_MOVE_CREATE_VEHICLES_DIALOG:
            return {
                ...state,
                isOpenMoveCreateVehiclesDialog: false,
            }
        case SELECT_ALL_VEHICLES: {
            const responseVehiclesClone = _.cloneDeep(state.responseVehicles)
            const vehicles = _.get(responseVehiclesClone, 'body.vehicles', [])
            responseVehiclesClone.body.vehicles = vehicles.map((item) => ({ ...item, checked: true, }))

            return {
                ...state,
                responseVehicles: responseVehiclesClone
            }
        }
        case DESELECT_ALL_VEHICLES: {
            const responseVehiclesClone = _.cloneDeep(state.responseVehicles)
            const vehicles = _.get(responseVehiclesClone, 'body.vehicles', [])
            responseVehiclesClone.body.vehicles = vehicles.map((item) => ({ ...item, checked: false, }))

            return {
                ...state,
                responseVehicles: responseVehiclesClone
            }
        }
        case SELECT_VEHICLE: {
            const responseVehiclesClone = _.cloneDeep(state.responseVehicles)
            const vehicles = _.get(responseVehiclesClone, 'body.vehicles', [])
            const foundVehicle = vehicles.find((item) => item.id === action.id)

            if (foundVehicle) {
                foundVehicle.checked = true
            }

            return {
                ...state,
                responseVehicles: responseVehiclesClone
            }
        }
        case DESELECT_VEHICLE: {
            const responseVehiclesClone = _.cloneDeep(state.responseVehicles)
            const vehicles = _.get(responseVehiclesClone, 'body.vehicles', [])
            const foundVehicle = vehicles.find((item) => item.id === action.id)

            if (foundVehicle) {
                foundVehicle.checked = false
            }

            return {
                ...state,
                responseVehicles: responseVehiclesClone
            }
        }
        case CREATE_GROUP_COMPLETED: {
            const responseVehiclesClone = _.cloneDeep(state.responseVehicles)

            const {
                groups,
                vehicles,
                groupName,
                documentsIds,
            } = getDataForEditGroup(action, responseVehiclesClone)

            if (responseVehiclesClone) {
                responseVehiclesClone.body.groups = groups

                if (documentsIds && documentsIds.length > 0) {
                    changeVehiclesGroup(documentsIds, vehicles, groupName)
                }
            }

            return {
                ...state,
                responseVehicles: responseVehiclesClone
            }
        }
        case EDIT_GROUP_ADDING_VEHICLES_COMPLETED: {
            const responseVehiclesClone = _.cloneDeep(state.responseVehicles)

            const {
                groups,
                vehicles,
                groupId,
                documentsIds,
            } = getDataForEditGroup(action, responseVehiclesClone)

            if (groups) {
                const foundGroup = groups.find((item) => item.id.toString() === groupId)
                changeVehiclesGroup(documentsIds, vehicles, foundGroup.name)

                responseVehiclesClone.body.groups = groups
            }

            return {
                ...state,
                responseVehicles: responseVehiclesClone
            }
        }
        default:
            return state
    }
}
