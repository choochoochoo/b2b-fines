import axios from 'axios'
import _ from 'lodash'

import {
    UPLOAD_VEHICLES_COMPLETED,
    UPLOAD_VEHICLES_FAILED,
    UPLOAD_VEHICLES_STARTED,
    OPEN_DIALOG_ERROR,
    SHOW_LOADER,
    HIDE_LOADER,
} from '../action-types'
import config from '../../../config.json'

import { showMessage, showMessageByError } from './global'

const URL = '/api/profiles/files/vehicles'

export const uploadVehiclesPure = (param, dispatch) => {
    dispatch({ type: UPLOAD_VEHICLES_STARTED })
    dispatch({ type: SHOW_LOADER })

    const formData = new FormData()
    formData.append('file', param.file, param.file.name)

    axios
        .post(`${config.host}${URL}`, formData, {
            withCredentials: true,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then((response) => {
            if (_.get(response, 'data.status', false)) {
                if (param.onSuccessHandler) {
                    param.onSuccessHandler()
                }

                dispatch({
                    type: UPLOAD_VEHICLES_COMPLETED,
                    payload: _.get(response, 'data', {}),
                })

            } else {
                dispatch({
                    type: OPEN_DIALOG_ERROR,
                    payload: _.get(response, 'data.error', '')
                })
            }

            dispatch({ type: HIDE_LOADER })

            showMessage(response, dispatch)
        })
        .catch((e) => {
            dispatch({ type: UPLOAD_VEHICLES_FAILED, e })
            dispatch({ type: HIDE_LOADER })
            showMessageByError(e, dispatch)
        })
}

export const uploadVehicles = (param) => (dispatch) => uploadVehiclesPure(param, dispatch)
