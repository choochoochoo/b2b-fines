import _ from 'lodash'
import i18next from 'i18next'

import {
    OPEN_DIALOG_ERROR,
    CLOSE_DIALOG_ERROR,
    SHOW_LOADER,
    HIDE_LOADER,
    SELECT_FINE,
    DESELECT_FINE,
    SELECT_ALL_FINES,
    DESELECT_ALL_FINES,
} from '../action-types'

export const openDialog = (title, message) => (dispatch) => dispatch({ type: OPEN_DIALOG_ERROR, title, message })

export const closeDialog = () => (dispatch) => dispatch({ type: CLOSE_DIALOG_ERROR })

export const selectFine = (uin) => (dispatch) => dispatch({ type: SELECT_FINE, uin })

export const deselectFine = (uin) => (dispatch) => dispatch({ type: DESELECT_FINE, uin })

export const selectAllFines = () => (dispatch) => dispatch({ type: SELECT_ALL_FINES })

export const deselectAllFines = () => (dispatch) => dispatch({ type: DESELECT_ALL_FINES })

export const showLoader = () => (dispatch) => dispatch({ type: SHOW_LOADER })

export const hideLoader = () => (dispatch) => dispatch({ type: HIDE_LOADER })

export const showMessageSimpleError = (error, dispatch) => {

    dispatch({
        type: OPEN_DIALOG_ERROR,
        title: i18next.t('dialog.error.title'),
        message: error
    })
}

export const showMessage = (response, dispatch) => {
    const data = _.get(response, 'data', {})

    if (_.has(data, 'message')) {
        dispatch({
            type: OPEN_DIALOG_ERROR,
            title: _.get(data, 'message.title', ''),
            message: _.get(data, 'message.body', '')
        })
    }
}

export const showMessageByError = (e, dispatch) => {
    if (_.get(e, 'response.status') === 500) {
        dispatch({
            type: OPEN_DIALOG_ERROR,
            title: i18next.t('dialog.error.505.title'),
            message: i18next.t('dialog.error.505.message'),
        })
    } else {
        dispatch({ type: OPEN_DIALOG_ERROR })
    }
}
