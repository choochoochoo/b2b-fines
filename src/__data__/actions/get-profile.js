import axios from 'axios'
import _ from 'lodash'

import {
    GET_PROFILE_STARTED,
    GET_PROFILE_COMPLETED,
    GET_PROFILE_FAILED,
} from '../action-types'
import config from '../../../config.json'

import { showMessage } from './global'

const URL = '/api/profiles'

export const getProfilePure = (param, dispatch) => {
    dispatch({ type: GET_PROFILE_STARTED })

    axios
        .get(`${config.host}${URL}`, {}, {
            withCredentials: true,
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then((response) => {

            if (param.onSuccessHandler) {
                param.onSuccessHandler()
            }

            dispatch({
                type: GET_PROFILE_COMPLETED,
                payload: _.get(response, 'data', {})
            })

            showMessage(response, dispatch)
        })
        .catch((e) => {

            if (param.onFailHandler) {
                param.onFailHandler(_.get(e, 'response', {}))
            }

            dispatch({ type: GET_PROFILE_FAILED, e })
        })
}

export const getProfile = (param) => (dispatch) => getProfilePure(param, dispatch)
