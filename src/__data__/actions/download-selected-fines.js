import axios from 'axios'
import _ from 'lodash'
import download from 'downloadjs'

import {
    DOWNLOAD_SELECTED_FINES_STARTED,
    DOWNLOAD_SELECTED_FINES_COMPLETED,
    DOWNLOAD_SELECTED_FINES_FAILED,
    SHOW_LOADER,
    HIDE_LOADER,
} from '../action-types'
import config from '../../../config.json'

import { showMessage, showMessageByError } from './global'

const URL = '/api/profiles/files/fines'

export const getFileName = (xhr) => {
    let filename = ''
    const disposition = xhr.headers['content-disposition']
    if (disposition && disposition.indexOf('attachment') !== -1) {
        const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/
        const matches = filenameRegex.exec(disposition)
        if (matches != null && matches[1]) {
            filename = matches[1].replace(/['"]/g, '')
        }
    }

    return filename
}

export const downloadSelectedFinesPure = (param, dispatch) => {
    dispatch({ type: DOWNLOAD_SELECTED_FINES_STARTED })
    dispatch({ type: SHOW_LOADER })

    axios
        .post(`${config.host}${URL}`, { list: param.data.join(',') }, {
            withCredentials: true,
            headers: {
                'Content-Type': 'application/json'
            },
            responseType: 'blob',
        })
        .then((response) => {
            download(response.data, getFileName(response), 'application/octet-stream')

            dispatch({
                type: DOWNLOAD_SELECTED_FINES_COMPLETED,
                payload: _.get(response, 'data', {})
            })

            dispatch({ type: HIDE_LOADER })

            showMessage(response, dispatch)
        })
        .catch((e) => {
            dispatch({ type: DOWNLOAD_SELECTED_FINES_FAILED, e })
            dispatch({ type: HIDE_LOADER })
            showMessageByError(e, dispatch)
        })
}

export const downloadSelectedFines = (param) => (dispatch) => downloadSelectedFinesPure(param, dispatch)
