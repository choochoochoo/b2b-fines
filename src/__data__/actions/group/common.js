import {
    OPEN_CREATE_GROUP_DIALOG,
    CLOSE_CREATE_GROUP_DIALOG,
    OPEN_MOVE_VEHICLES_DIALOG,
    CLOSE_MOVE_VEHICLES_DIALOG,
    OPEN_MOVE_CREATE_VEHICLES_DIALOG,
    CLOSE_MOVE_CREATE_VEHICLES_DIALOG,
    OPEN_EDIT_GROUP_DIALOG,
    CLOSE_EDIT_GROUP_DIALOG,
    OPEN_REMOVE_GROUP_DIALOG,
    CLOSE_REMOVE_GROUP_DIALOG,
    SELECT_GROUP_VEHICLE,
    DESELECT_GROUP_VEHICLE,
    SELECT_ALL_GROUP_VEHICLES,
    DESELECT_ALL_GROUP_VEHICLES,
} from '../../action-types'

export const openCreateGroupDialog = () => (dispatch) => dispatch({ type: OPEN_CREATE_GROUP_DIALOG, })

export const closeCreateGroupDialog = () => (dispatch) => dispatch({ type: CLOSE_CREATE_GROUP_DIALOG })

export const openMoveVehiclesDialog = () => (dispatch) => dispatch({ type: OPEN_MOVE_VEHICLES_DIALOG })

export const closeMoveVehiclesDialog = () => (dispatch) => dispatch({ type: CLOSE_MOVE_VEHICLES_DIALOG })

export const openMoveCreateVehiclesDialog = () => (dispatch) => dispatch({ type: OPEN_MOVE_CREATE_VEHICLES_DIALOG })

export const closeMoveCreateVehiclesDialog = () => (dispatch) => dispatch({ type: CLOSE_MOVE_CREATE_VEHICLES_DIALOG })

export const openEditGroupDialog = () => (dispatch) => dispatch({ type: OPEN_EDIT_GROUP_DIALOG, })

export const closeEditGroupDialog = () => (dispatch) => dispatch({ type: CLOSE_EDIT_GROUP_DIALOG, })

export const openRemoveGroupDialog = () => (dispatch) => dispatch({ type: OPEN_REMOVE_GROUP_DIALOG, })

export const closeRemoveGroupDialog = () => (dispatch) => dispatch({ type: CLOSE_REMOVE_GROUP_DIALOG, })

export const selectGroupVehicle = (id) => (dispatch) => dispatch({ type: SELECT_GROUP_VEHICLE, id })

export const deselectGroupVehicle = (id) => (dispatch) => dispatch({ type: DESELECT_GROUP_VEHICLE, id })

export const selectAllGroupVehicles = () => (dispatch) => dispatch({ type: SELECT_ALL_GROUP_VEHICLES })

export const deselectAllGroupVehicles = () => (dispatch) => dispatch({ type: DESELECT_ALL_GROUP_VEHICLES })
