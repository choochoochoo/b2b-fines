import axios from 'axios'
import _ from 'lodash'

import {
    REMOVE_GROUP_COMPLETED,
    REMOVE_GROUP_FAILED,
    REMOVE_GROUP_STARTED,
    SHOW_LOADER,
    HIDE_LOADER,
    CLOSE_REMOVE_GROUP_DIALOG,
} from '../../action-types'
import config from '../../../../config.json'
import { showMessage, showMessageByError } from '../global'

const URL = '/api/groups'

export const removeGroupPure = (param, dispatch) => {
    dispatch({ type: REMOVE_GROUP_STARTED })
    dispatch({ type: SHOW_LOADER })
    dispatch({ type: CLOSE_REMOVE_GROUP_DIALOG })

    axios
        .delete(`${config.host}${URL}/${param.groupId}`,
            {
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        )
        .then((response) => {
            dispatch({
                type: REMOVE_GROUP_COMPLETED,
                payload: _.get(response, 'data', {}),
            })

            dispatch({ type: HIDE_LOADER })

            if (param.onSuccessHandler) {
                param.onSuccessHandler()
            }

            showMessage(response, dispatch)
        })
        .catch((e) => {
            dispatch({ type: REMOVE_GROUP_FAILED, e })
            dispatch({ type: HIDE_LOADER })

            showMessageByError(e, dispatch)
        })
}

export const removeGroup = (param) => (dispatch) => removeGroupPure(param, dispatch)
