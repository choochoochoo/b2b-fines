import axios from 'axios'
import _ from 'lodash'

import {
    CREATE_GROUP_COMPLETED,
    CREATE_GROUP_FAILED,
    CREATE_GROUP_STARTED,
    SHOW_LOADER,
    HIDE_LOADER,
    CLOSE_CREATE_GROUP_DIALOG,
    CLOSE_MOVE_VEHICLES_DIALOG,
    CLOSE_MOVE_CREATE_VEHICLES_DIALOG,
} from '../../action-types'
import config from '../../../../config.json'
import { showMessage, showMessageByError } from '../global'

const URL = '/api/groups'

export const createGroupPure = (param, dispatch) => {
    dispatch({ type: CREATE_GROUP_STARTED })
    dispatch({ type: SHOW_LOADER })
    dispatch({ type: CLOSE_CREATE_GROUP_DIALOG })
    dispatch({ type: CLOSE_MOVE_VEHICLES_DIALOG })
    dispatch({ type: CLOSE_MOVE_CREATE_VEHICLES_DIALOG })

    axios
        .post(`${config.host}${URL}`,
            {
                name: param.name,
                documentsIds: param.documentsIds,
            },
            {
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        )
        .then((response) => {
            dispatch({
                type: CREATE_GROUP_COMPLETED,
                payload: {
                    requestParam: param,
                    response: _.get(response, 'data', {}),
                }
            })

            dispatch({ type: HIDE_LOADER })

            if (param.onSuccessHandler) {
                param.onSuccessHandler()
            }

            showMessage(response, dispatch)
        })
        .catch((e) => {
            dispatch({ type: CREATE_GROUP_FAILED, e })
            dispatch({ type: HIDE_LOADER })

            showMessageByError(e, dispatch)
        })
}

export const createGroup = (param) => (dispatch) => createGroupPure(param, dispatch)
