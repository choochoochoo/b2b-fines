import axios from 'axios'
import _ from 'lodash'

import {
    EDIT_GROUP_ADDING_VEHICLES_STARTED,
    EDIT_GROUP_ADDING_VEHICLES_COMPLETED,
    EDIT_GROUP_ADDING_VEHICLES_FAILED,
    SHOW_LOADER,
    HIDE_LOADER,
    CLOSE_MOVE_VEHICLES_DIALOG,
} from '../../action-types'
import config from '../../../../config.json'
import { showMessage, showMessageByError } from '../global'

const URL = '/api/groups'

export const editGroupAddingVehiclesPure = (param, dispatch) => {
    dispatch({ type: EDIT_GROUP_ADDING_VEHICLES_STARTED })
    dispatch({ type: SHOW_LOADER })
    dispatch({ type: CLOSE_MOVE_VEHICLES_DIALOG })

    axios
        .put(`${config.host}${URL}/${param.groupId}/documents/adding`,
            {
                groupId: param.groupId,
                documentsIds: param.documentsIds
            },
            {
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        )
        .then((response) => {
            dispatch({
                type: EDIT_GROUP_ADDING_VEHICLES_COMPLETED,
                payload: {
                    requestParam: param,
                    response: _.get(response, 'data', {}),
                }
            })

            dispatch({ type: HIDE_LOADER })

            if (param.onSuccessHandler) {
                param.onSuccessHandler()
            }

            showMessage(response, dispatch)
        })
        .catch((e) => {
            dispatch({ type: EDIT_GROUP_ADDING_VEHICLES_FAILED, e })
            dispatch({ type: HIDE_LOADER })

            if (param.onErrorHandler) {
                param.onErrorHandler()
            }

            showMessageByError(e, dispatch)
        })
}

export const editGroupAddingVehicles = (param) => (dispatch) => editGroupAddingVehiclesPure(param, dispatch)
