import axios from 'axios'
import _ from 'lodash'

import {
    EDIT_GROUP_COMPLETED,
    EDIT_GROUP_FAILED,
    EDIT_GROUP_STARTED,
    SHOW_LOADER,
    HIDE_LOADER,
    CLOSE_EDIT_GROUP_DIALOG,
} from '../../action-types'
import config from '../../../../config.json'
import { showMessage, showMessageByError } from '../global'

const URL = '/api/groups'

export const editGroupPure = (param, dispatch) => {
    dispatch({ type: EDIT_GROUP_STARTED })
    dispatch({ type: SHOW_LOADER })
    dispatch({ type: CLOSE_EDIT_GROUP_DIALOG })

    axios
        .put(`${config.host}${URL}/${param.groupId}`,
            {},
            {
                params: {
                    name: param.name,
                },
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        )
        .then((response) => {
            dispatch({
                type: EDIT_GROUP_COMPLETED,
                payload: {
                    requestParam: param,
                    response: _.get(response, 'data', {}),
                }
            })

            dispatch({ type: HIDE_LOADER })

            if (param.onSuccessHandler) {
                param.onSuccessHandler()
            }

            showMessage(response, dispatch)
        })
        .catch((e) => {
            dispatch({ type: EDIT_GROUP_FAILED, e })
            dispatch({ type: HIDE_LOADER })

            if (param.onErrorHandler) {
                param.onErrorHandler()
            }

            showMessageByError(e, dispatch)
        })
}

export const editGroup = (param) => (dispatch) => editGroupPure(param, dispatch)
