import axios from 'axios'
import _ from 'lodash'

import {
    GET_GROUP_COMPLETED,
    GET_GROUP_FAILED,
    GET_GROUP_STARTED,
    SHOW_LOADER,
    HIDE_LOADER,
} from '../../action-types'
import config from '../../../../config.json'
import { showMessage, showMessageByError } from '../global'

const URL = '/api/groups'

export const getGroupPure = (param, dispatch) => {
    dispatch({ type: GET_GROUP_STARTED })
    dispatch({ type: SHOW_LOADER })

    axios
        .get(`${config.host}${URL}/${param.groupId}`,
            {
                params: {
                    paginationSize: config.pageSize,
                    paginationOffset: (param.page - 1) * config.pageSize,
                },
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        )
        .then((response) => {
            dispatch({
                type: GET_GROUP_COMPLETED,
                payload: {
                    requestParam: param,
                    response: _.get(response, 'data', {}),
                }
            })

            dispatch({ type: HIDE_LOADER })

            showMessage(response, dispatch)
        })
        .catch((e) => {
            dispatch({ type: GET_GROUP_FAILED, e })
            dispatch({ type: HIDE_LOADER })

            showMessageByError(e, dispatch)
        })
}

export const getGroup = (param) => (dispatch) => getGroupPure(param, dispatch)

