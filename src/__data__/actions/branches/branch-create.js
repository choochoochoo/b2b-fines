import axios from 'axios'
import _ from 'lodash'

import {
    CREATE_BRANCH_COMPLETED,
    CREATE_BRANCH_FAILED,
    CREATE_BRANCH_STARTED,
    SHOW_LOADER,
    HIDE_LOADER,
    CLOSE_CREATE_BRANCH_DIALOG,
} from '../../action-types'
import config from '../../../../config.json'
import { showMessage, showMessageByError } from '../global'

const URL = '/api/organizations'

export const createBranchPure = (param, dispatch) => {
    dispatch({ type: CREATE_BRANCH_STARTED })
    dispatch({ type: SHOW_LOADER })
    dispatch({ type: CLOSE_CREATE_BRANCH_DIALOG })

    axios
        .post(`${config.host}${URL}/${param.organizationId}/branches`,
            {
                name: param.name,
                address: param.address,
                kpp: param.kpp,
            },
            {
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        )
        .then((response) => {
            dispatch({
                type: CREATE_BRANCH_COMPLETED,
                payload: {
                    requestParam: param,
                    response: _.get(response, 'data', {}),
                }
            })

            dispatch({ type: HIDE_LOADER })

            if (param.onSuccessHandler) {
                param.onSuccessHandler()
            }

            showMessage(response, dispatch)
        })
        .catch((e) => {
            dispatch({ type: CREATE_BRANCH_FAILED, e })
            dispatch({ type: HIDE_LOADER })

            showMessageByError(e, dispatch)
        })
}

export const createBranch = (param) => (dispatch) => createBranchPure(param, dispatch)
