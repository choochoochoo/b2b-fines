import axios from 'axios'
import _ from 'lodash'

import {
    REMOVE_BRANCH_COMPLETED,
    REMOVE_BRANCH_FAILED,
    REMOVE_BRANCH_STARTED,
    SHOW_LOADER,
    HIDE_LOADER,
    CLOSE_REMOVE_BRANCH_DIALOG,
} from '../../action-types'
import config from '../../../../config.json'
import { showMessage, showMessageByError } from '../global'

const URL = '/api/organizations'

export const removeBranchPure = (param, dispatch) => {
    dispatch({ type: REMOVE_BRANCH_STARTED })
    dispatch({ type: SHOW_LOADER })
    dispatch({ type: CLOSE_REMOVE_BRANCH_DIALOG })

    axios
        .delete(`${config.host}${URL}/branches/${param.branchId}`,
            {
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        )
        .then((response) => {
            dispatch({
                type: REMOVE_BRANCH_COMPLETED,
                payload: {
                    requestParam: param,
                    response: _.get(response, 'data', {}),
                }
            })

            dispatch({ type: HIDE_LOADER })

            if (param.onSuccessHandler) {
                param.onSuccessHandler()
            }

            showMessage(response, dispatch)
        })
        .catch((e) => {
            dispatch({ type: REMOVE_BRANCH_FAILED, e })
            dispatch({ type: HIDE_LOADER })

            showMessageByError(e, dispatch)
        })
}

export const removeBranch = (param) => (dispatch) => removeBranchPure(param, dispatch)
