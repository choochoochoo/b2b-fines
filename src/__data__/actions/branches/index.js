import {
    openCreateBranchDialog,
    closeCreateBranchDialog,
    openEditBranchDialog,
    closeEditBranchDialog,
    openRemoveBranchDialog,
    closeRemoveBranchDialog,
} from './common'
import { editBranch } from './branch-edit'
import { createBranch } from './branch-create'
import { removeBranch } from './branch-remove'

export {
    openCreateBranchDialog,
    closeCreateBranchDialog,
    openEditBranchDialog,
    closeEditBranchDialog,
    openRemoveBranchDialog,
    closeRemoveBranchDialog,

    editBranch,
    createBranch,
    removeBranch,
}
