import {
    OPEN_CREATE_BRANCH_DIALOG,
    CLOSE_CREATE_BRANCH_DIALOG,
    OPEN_EDIT_BRANCH_DIALOG,
    CLOSE_EDIT_BRANCH_DIALOG,
    OPEN_REMOVE_BRANCH_DIALOG,
    CLOSE_REMOVE_BRANCH_DIALOG,
} from '../../action-types'

export const openCreateBranchDialog = (id) => (dispatch) => dispatch({ type: OPEN_CREATE_BRANCH_DIALOG, id })
export const closeCreateBranchDialog = () => (dispatch) => dispatch({ type: CLOSE_CREATE_BRANCH_DIALOG })

export const openEditBranchDialog = (id) => (dispatch) => dispatch({ type: OPEN_EDIT_BRANCH_DIALOG, id })
export const closeEditBranchDialog = () => (dispatch) => dispatch({ type: CLOSE_EDIT_BRANCH_DIALOG })

export const openRemoveBranchDialog = (branch) => (dispatch) => dispatch({ type: OPEN_REMOVE_BRANCH_DIALOG, branch })
export const closeRemoveBranchDialog = () => (dispatch) => dispatch({ type: CLOSE_REMOVE_BRANCH_DIALOG })
