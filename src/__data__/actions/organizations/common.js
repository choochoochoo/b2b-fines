import {
    OPEN_EDIT_ORGANIZATION_DIALOG,
    CLOSE_EDIT_ORGANIZATION_DIALOG,
    OPEN_REMOVE_ORGANIZATION_DIALOG,
    CLOSE_REMOVE_ORGANIZATION_DIALOG,
} from '../../action-types'

export const openEditOrganizationDialog = (id) => (dispatch) => dispatch({ type: OPEN_EDIT_ORGANIZATION_DIALOG, id })
export const closeEditOrganizationDialog = () => (dispatch) => dispatch({ type: CLOSE_EDIT_ORGANIZATION_DIALOG })

export const openRemoveOrganizationDialog = (organization) => (dispatch) => dispatch({ type: OPEN_REMOVE_ORGANIZATION_DIALOG, organization })
export const closeRemoveOrganizationDialog = () => (dispatch) => dispatch({ type: CLOSE_REMOVE_ORGANIZATION_DIALOG })

