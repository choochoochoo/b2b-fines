import axios from 'axios'
import _ from 'lodash'

import {
    EDIT_ORGANIZATIONS_STARTED,
    EDIT_ORGANIZATIONS_FAILED,
    EDIT_ORGANIZATIONS_COMPLETED,
    SHOW_LOADER,
    HIDE_LOADER,
} from '../../action-types'
import config from '../../../../config.json'
import { showMessage, showMessageByError } from '../global'

const URL = '/api/organizations'

export const editOrganizationPure = (param, dispatch) => {
    dispatch({ type: EDIT_ORGANIZATIONS_STARTED })
    dispatch({ type: SHOW_LOADER })

    axios
        .put(`${config.host}${URL}/${param.organizationId}`,
            {
                name: param.name,
                address: param.address,
                kpp: param.kpp,
            },
            {
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        )
        .then((response) => {
            dispatch({
                type: EDIT_ORGANIZATIONS_COMPLETED,
                payload: {
                    requestParam: param,
                    response: _.get(response, 'data', {}),
                }
            })

            dispatch({ type: HIDE_LOADER })

            if (param.onSuccessHandler) {
                param.onSuccessHandler()
            }

            showMessage(response, dispatch)
        })
        .catch((e) => {
            dispatch({ type: EDIT_ORGANIZATIONS_FAILED, e })
            dispatch({ type: HIDE_LOADER })

            if (param.onErrorHandler) {
                param.onErrorHandler()
            }

            showMessageByError(e, dispatch)
        })
}

export const editOrganization = (param) => (dispatch) => editOrganizationPure(param, dispatch)
