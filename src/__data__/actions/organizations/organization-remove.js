import axios from 'axios'
import _ from 'lodash'

import {
    REMOVE_ORGANIZATIONS_COMPLETED,
    REMOVE_ORGANIZATIONS_FAILED,
    REMOVE_ORGANIZATIONS_STARTED,
    CLOSE_REMOVE_ORGANIZATION_DIALOG,
    SHOW_LOADER,
    HIDE_LOADER,
} from '../../action-types'
import config from '../../../../config.json'
import { showMessage, showMessageByError } from '../global'

const URL = '/api/organizations'

export const removeOrganizationPure = (param, dispatch) => {
    dispatch({ type: REMOVE_ORGANIZATIONS_STARTED })
    dispatch({ type: SHOW_LOADER })

    axios
        .delete(`${config.host}${URL}/${param.organizationId}`,
            {
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        )
        .then((response) => {
            dispatch({
                type: REMOVE_ORGANIZATIONS_COMPLETED,
                payload: {
                    requestParam: param,
                    response: _.get(response, 'data', {}),
                }
            })

            dispatch({ type: HIDE_LOADER })
            dispatch({ type: CLOSE_REMOVE_ORGANIZATION_DIALOG })

            if (param.onSuccessHandler) {
                param.onSuccessHandler()
            }

            showMessage(response, dispatch)
        })
        .catch((e) => {
            dispatch({ type: REMOVE_ORGANIZATIONS_FAILED, e })
            dispatch({ type: HIDE_LOADER })
            dispatch({ type: CLOSE_REMOVE_ORGANIZATION_DIALOG })

            showMessageByError(e, dispatch)
        })
}

export const removeOrganization = (param) => (dispatch) => removeOrganizationPure(param, dispatch)
