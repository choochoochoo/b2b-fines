import axios from 'axios'
import _ from 'lodash'

import {
    GET_ORGANIZATIONS_COMPLETED,
    GET_ORGANIZATIONS_FAILED,
    GET_ORGANIZATIONS_STARTED,
    SHOW_LOADER,
    HIDE_LOADER,
} from '../../action-types'
import config from '../../../../config.json'
import { showMessage, showMessageByError } from '../global'

const URL = '/api/organizations'

export const getOrganizationsPure = (param, dispatch) => {
    dispatch({ type: GET_ORGANIZATIONS_STARTED })
    dispatch({ type: SHOW_LOADER })

    axios
        .get(`${config.host}${URL}`,
            {
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        )
        .then((response) => {

            const data = _.get(response, 'data', {})

            dispatch({
                type: GET_ORGANIZATIONS_COMPLETED,
                payload: {
                    requestParam: param,
                    response: data,
                }
            })

            dispatch({ type: HIDE_LOADER })

            if (param.onSuccessHandler) {
                param.onSuccessHandler(data)
            }

            showMessage(response, dispatch)
        })
        .catch((e) => {
            dispatch({ type: GET_ORGANIZATIONS_FAILED, e })
            dispatch({ type: HIDE_LOADER })

            if (param.onErrorHandler) {
                param.onErrorHandler()
            }

            showMessageByError(e, dispatch)
        })
}

export const getOrganizations = (param) => (dispatch) => getOrganizationsPure(param, dispatch)

