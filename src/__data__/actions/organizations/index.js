import {
    openEditOrganizationDialog,
    closeEditOrganizationDialog,
    openRemoveOrganizationDialog,
    closeRemoveOrganizationDialog,
} from './common'
import { getOrganizations } from './organization-get'
import { editOrganization } from './organization-edit'
import { createOrganization } from './organization-create'
import { removeOrganization } from './organization-remove'

export {
    openEditOrganizationDialog,
    closeEditOrganizationDialog,
    openRemoveOrganizationDialog,
    closeRemoveOrganizationDialog,

    getOrganizations,
    editOrganization,
    createOrganization,
    removeOrganization,
}
