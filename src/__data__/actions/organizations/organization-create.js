import axios from 'axios'
import _ from 'lodash'

import {
    CREATE_ORGANIZATIONS_COMPLETED,
    CREATE_ORGANIZATIONS_FAILED,
    CREATE_ORGANIZATIONS_STARTED,
    SHOW_LOADER,
    HIDE_LOADER,
} from '../../action-types'
import config from '../../../../config.json'
import { showMessageByError, showMessageSimpleError } from '../global'

const URL = '/api/organizations'

export const createOrganizationPure = (param, dispatch) => {
    dispatch({ type: CREATE_ORGANIZATIONS_STARTED })
    dispatch({ type: SHOW_LOADER })

    axios
        .post(`${config.host}${URL}`,
            {
                inn: param.inn,
            },
            {
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        )
        .then((response) => {

            const data = _.get(response, 'data', {})

            if (data.status) {
                dispatch({
                    type: CREATE_ORGANIZATIONS_COMPLETED,
                    payload: {
                        requestParam: param,
                        response: data,
                    }
                })

                if (param.onSuccessHandler) {
                    param.onSuccessHandler(data)
                }

            } else {
                dispatch({ type: CREATE_ORGANIZATIONS_FAILED, error: data.error })
                showMessageSimpleError(data.error, dispatch)
            }

            dispatch({ type: HIDE_LOADER })

        })
        .catch((e) => {
            dispatch({ type: CREATE_ORGANIZATIONS_FAILED, e })
            dispatch({ type: HIDE_LOADER })

            showMessageByError(e, dispatch)
        })
}

export const createOrganization = (param) => (dispatch) => createOrganizationPure(param, dispatch)
