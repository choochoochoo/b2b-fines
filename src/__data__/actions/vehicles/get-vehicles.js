import axios from 'axios'
import _ from 'lodash'

import {
    GET_VEHICLES_COMPLETED,
    GET_VEHICLES_FAILED,
    GET_VEHICLES_STARTED,
    HIDE_LOADER,
    SHOW_LOADER,
} from '../../action-types'
import config from '../../../../config.json'
import { showMessage, showMessageByError } from '../global'

const URL = '/api/vehicles'

export const getVehiclesPure = (param, dispatch) => {
    dispatch({ type: GET_VEHICLES_STARTED })
    dispatch({ type: SHOW_LOADER })

    axios
        .get(`${config.host}${URL}`, {
            params: {
                paginationSize: config.pageSize,
                paginationOffset: (param.page - 1) * config.pageSize,
            },
            withCredentials: true,
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then((response) => {
            dispatch({
                type: GET_VEHICLES_COMPLETED,
                payload: _.get(response, 'data', {})
            })

            dispatch({ type: HIDE_LOADER })

            showMessage(response, dispatch)
        })
        .catch((e) => {
            dispatch({ type: GET_VEHICLES_FAILED, e })
            dispatch({ type: HIDE_LOADER })
            showMessageByError(e, dispatch)
        })
}

export const getVehicles = (param) => (dispatch) => getVehiclesPure(param, dispatch)
