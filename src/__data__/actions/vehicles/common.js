import {
    SELECT_VEHICLE,
    DESELECT_VEHICLE,
    SELECT_ALL_VEHICLES,
    DESELECT_ALL_VEHICLES,
} from '../../action-types'

export const selectVehicle = (id) => (dispatch) => dispatch({ type: SELECT_VEHICLE, id })

export const deselectVehicle = (id) => (dispatch) => dispatch({ type: DESELECT_VEHICLE, id })

export const selectAllVehicles = () => (dispatch) => dispatch({ type: SELECT_ALL_VEHICLES })

export const deselectAllVehicles = () => (dispatch) => dispatch({ type: DESELECT_ALL_VEHICLES })
