import axios from 'axios'
import _ from 'lodash'

import {
    LOGOUT_COMPLETED,
    LOGOUT_FAILED,
    LOGOUT_STARTED,
    SHOW_LOADER,
    HIDE_LOADER,
    OPEN_DIALOG_ERROR,
} from '../action-types'
import config from '../../../config.json'

const URL = '/api/logout'

export const logoutPure = (param, dispatch) => {
    dispatch({ type: LOGOUT_STARTED })
    dispatch({ type: SHOW_LOADER })

    axios
        .get(`${config.host}${URL}`, {}, {
            withCredentials: true,
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then((response) => {
            if (param.onSuccessHandler) {
                param.onSuccessHandler()
            }

            dispatch({
                type: LOGOUT_COMPLETED,
                payload: _.get(response, 'data', {})
            })

            dispatch({ type: HIDE_LOADER })
        })
        .catch((e) => {
            dispatch({ type: LOGOUT_FAILED, e })
            dispatch({ type: HIDE_LOADER })
            dispatch({ type: OPEN_DIALOG_ERROR })
        })
}

export const logout = (param) => (dispatch) => logoutPure(param, dispatch)
