import axios from 'axios'
import _ from 'lodash'

import {
    LOGIN_COMPLETED,
    LOGIN_FAILED,
    LOGIN_STARTED,
    SHOW_LOADER,
    HIDE_LOADER,
} from '../action-types'
import config from '../../../config.json'

const URL = '/api/login'

export const loginPure = (param, dispatch) => {
    dispatch({ type: LOGIN_STARTED })
    dispatch({ type: SHOW_LOADER })

    const params = new URLSearchParams()
    params.append('username', param.login)
    params.append('password', param.password)

    axios
        .post(`${config.host}${URL}`, params, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
        .then((response) => {
            const data = _.get(response, 'data', {})
            if (data.status) {
                if (param.onSuccessHandler) {
                    param.onSuccessHandler()
                }

                dispatch({
                    type: LOGIN_COMPLETED,
                    payload: data
                })
            } else {
                dispatch({
                    type: LOGIN_FAILED,
                    payload: _.get(response, 'data', {})
                })
            }

            dispatch({ type: HIDE_LOADER })
        })
        .catch((e) => {
            dispatch({ type: LOGIN_FAILED, e })
            dispatch({ type: HIDE_LOADER })
        })
}

export const login = (param) => (dispatch) => loginPure(param, dispatch)
