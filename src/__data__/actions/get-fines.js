import axios from 'axios'
import _ from 'lodash'

import {
    GET_FINES_COMPLETED,
    GET_FINES_FAILED,
    GET_FINES_STARTED,
    SHOW_LOADER,
    HIDE_LOADER,
} from '../action-types'
import config from '../../../config.json'

import { showMessage, showMessageByError } from './global'

export const DEFAULT_SORT = 'actDate_desc'

const URL = '/api/fines'

export const getFinesPure = (param, dispatch) => {
    dispatch({ type: GET_FINES_STARTED, payload: { params: param, } })
    dispatch({ type: SHOW_LOADER })

    const bodyParam = _.isEmpty(param.filters) ? null : { filters: param.filters, }

    axios
        .post(`${config.host}${URL}`, bodyParam, {
            params: {
                paginationSize: config.pageSize,
                paginationOffset: (param.page - 1) * config.pageSize,
                sort: param.sort || DEFAULT_SORT,
            },
            withCredentials: true,
            headers: {
                'Content-Type': 'application/json'
            }
        }
        )
        .then((response) => {
            dispatch({
                type: GET_FINES_COMPLETED,
                payload: {
                    response: _.get(response, 'data', {}),
                },
            })

            if (param.onSuccessHandler) {
                param.onSuccessHandler()
            }

            dispatch({ type: HIDE_LOADER })

            showMessage(response, dispatch)
        })
        .catch((e) => {
            dispatch({ type: GET_FINES_FAILED, e })
            dispatch({ type: HIDE_LOADER })

            if (param.onErrorHandler) {
                param.onErrorHandler()
            }

            showMessageByError(e, dispatch)
        })
}

export const getFines = (param) => (dispatch) => getFinesPure(param, dispatch)
