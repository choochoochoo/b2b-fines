import {
    SET_FILTER,
    SET_ORGANIZATION_FILTER,
    CLEAR_FILTER,
    CLEAR_ALL_FILTERS,
    SET_DEFAULT_FILTER,
    SET_ERROR_FILTER,
} from '../action-types'

export const setFilter = (param) => (dispatch) => dispatch({ type: SET_FILTER, param })

export const clearFilter = (param) => (dispatch) => dispatch({ type: CLEAR_FILTER, param })

export const clearAllFilters = () => (dispatch) => dispatch({ type: CLEAR_ALL_FILTERS })

export const setDefaultFilter = (filters) => (dispatch) => dispatch({ type: SET_DEFAULT_FILTER, filters })

export const setErrorFilter = (filterError) => (dispatch) => dispatch({ type: SET_ERROR_FILTER, filterError })

export const setOrganizationFilter = (param) => (dispatch) => dispatch({ type: SET_ORGANIZATION_FILTER, param })

