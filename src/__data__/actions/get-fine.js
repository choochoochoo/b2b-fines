import axios from 'axios'
import _ from 'lodash'

import {
    GET_FINE_COMPLETED,
    GET_FINE_FAILED,
    GET_FINE_STARTED,
    SHOW_LOADER,
    HIDE_LOADER,
    OPEN_FINE_DETAILS_DIALOG,
    CLOSE_FINE_DETAILS_DIALOG,
} from '../action-types'
import config from '../../../config.json'

import { showMessage, showMessageByError } from './global'

const URL = '/api/fines'

export const getFinePure = (param, dispatch) => {
    dispatch({ type: GET_FINE_STARTED })
    dispatch({ type: SHOW_LOADER })

    axios
        .get(`${config.host}${URL}/${param.uin}`, {
            withCredentials: true,
            headers: {
                'Content-Type': 'application/json'
            }
        }
        )
        .then((response) => {
            dispatch({
                type: GET_FINE_COMPLETED,
                payload: _.get(response, 'data', {}),
            })

            dispatch({ type: HIDE_LOADER })

            showMessage(response, dispatch)
        })
        .catch((e) => {
            dispatch({ type: GET_FINE_FAILED, e })
            dispatch({ type: HIDE_LOADER })

            showMessageByError(e, dispatch)
        })
}

export const getFine = (param) => (dispatch) => getFinePure(param, dispatch)

export const openFineDetailsDialog = (uin) => (dispatch) => dispatch({ type: OPEN_FINE_DETAILS_DIALOG, uin })

export const closeFineDetailsDialog = () => (dispatch) => dispatch({ type: CLOSE_FINE_DETAILS_DIALOG })
