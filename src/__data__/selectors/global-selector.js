import { createSelector } from 'reselect'
import _ from 'lodash'

export const getStore = createSelector((state) => _.get(state, 'global'), (state) => state)

export const getProfile = createSelector(getStore, (state) => _.get(state, 'responseProfile.body', {}))

export const getEmail = createSelector(getProfile, (state) => _.get(state, 'email', ''))

export const getIsUploadVehiclesFile = createSelector(getProfile, (state) => _.get(state, 'uploadVehiclesFile', false))

export const getUploadVehiclesFileName = createSelector(getProfile, (state) => _.get(state, 'vehiclesUserFileName', ''))

export const getNextCheckingFinesDate = createSelector(getProfile, (state) => _.get(state, 'nextCheckingFinesDate', ''))

export const getLastCheckingFinesDate = createSelector(getProfile, (state) => _.get(state, 'lastCheckingFinesDate', ''))

export const getIsDialogOpen = createSelector(getStore, (state) => _.get(state, 'dialogOpen', false))

export const getIsDialogMessage = createSelector(getStore, (state) => _.get(state, 'dialogMessage', ''))

export const getIsDialogTitle = createSelector(getStore, (state) => _.get(state, 'dialogTitle', ''))

export const getLoaderOpened = createSelector(getStore, (state) => _.get(state, 'loaderOpened', false))
