import { createSelector } from 'reselect'
import _ from 'lodash'

export const getStore = createSelector((state) => _.get(state, 'login'), (state) => state)

export const getError = createSelector(getStore, (state) => _.get(state, 'responseLogin.error', null))
