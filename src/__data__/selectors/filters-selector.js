import { createSelector } from 'reselect'
import _ from 'lodash'

export const getStore = createSelector((state) => _.get(state, 'filters'), (state) => state)

export const getForm = createSelector(getStore, (state) => _.get(state, 'form', {}))

export const getHasAppliedFilter = createSelector(getForm, (state) => !!Object.keys(state).find((key) => !!state[key]))

export const getFilterError = createSelector(getStore, (state) => _.get(state, 'filterError', {}))
