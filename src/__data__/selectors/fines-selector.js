import { createSelector } from 'reselect'
import _ from 'lodash'

export const getStore = createSelector((state) => _.get(state, 'fines'), (state) => state)

export const getFines = createSelector(getStore, (state) => _.get(state, 'responseFines.body.fines', []))

export const getIsLoading = createSelector(getStore, (state) => _.get(state, 'isLoading', true))

export const getFinesQuantity = createSelector(getStore, (state) => _.get(state, 'responseFines.body.quantity', 0))

export const getFinesTotalAmount = createSelector(getStore, (state) => _.get(state, 'responseFines.body.totalAmount', 0))

export const getHasMore = createSelector(getStore, (state) => _.get(state, 'responseFines.body.hasMore', false))

export const getCheckedFines = createSelector(getFines, (state) => state.filter((item) => item.checked))

export const getCheckedFinesArray = createSelector(getCheckedFines, (state) => state.filter((item) => item.checked).map((item) => item.uin))

export const getCheckedFinesQuantity = createSelector(getCheckedFines, (state) => state.length)

export const getFilters = createSelector(getStore, (state) => _.get(state, 'responseFines.body.filters', []))

export const getPrevParams = createSelector(getStore, (state) => _.get(state, 'prevParams', null))

export const getOrganizationFilter = createSelector(getStore, (state) => _.get(state, 'organizationFilter', null))
