import { createSelector } from 'reselect'
import _ from 'lodash'

export const getStore = createSelector((state) => _.get(state, 'group'), (state) => state)

export const getGroupId = createSelector(getStore, (state) => _.get(state, 'groupId', null))

export const getVehicles = createSelector(getStore, (state) => _.get(state, 'responseGroup.body.vehicles', []))

export const getOriginalGroups = createSelector(getStore, (state) => _.get(state, 'responseGroup.body.groups', []))

export const getGroup = createSelector(getOriginalGroups, getGroupId, (originalGroups, groupId) =>
    originalGroups.find((item) => item.id.toString() === groupId.toString()))

export const getGroupsWithoutCurrent = createSelector(getOriginalGroups, getGroupId, (originalGroups, groupId) =>
    originalGroups.filter((item) => item.id.toString() !== groupId.toString()))

export const getIsLoading = createSelector(getStore, (state) => _.get(state, 'isLoading', true))

export const getHasMore = createSelector(getStore, (state) => _.get(state, 'responseGroup.body.hasMore', false))

export const getIsOpenRemoveGroupDialog = createSelector(getStore, (state) => _.get(state, 'isOpenRemoveGroupDialog', false))

export const getIsOpenEditGroupDialog = createSelector(getStore, (state) => _.get(state, 'isOpenEditGroupDialog', false))

export const getCheckedVehicles = createSelector(getVehicles, (state) => state.filter((item) => item.checked))

export const getCheckedVehiclesArray = createSelector(getCheckedVehicles, (state) => state.filter((item) => item.checked).map((item) => item.id))

export const getCheckedVehiclesQuantity = createSelector(getCheckedVehicles, (state) => state.length)
