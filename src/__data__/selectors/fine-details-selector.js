import { createSelector } from 'reselect'
import _ from 'lodash'
import i18next from 'i18next'

export const getStore = createSelector((state) => _.get(state, 'fineDetails'), (state) => state)

export const getResponse = createSelector(getStore, (state) => _.get(state, 'response', {}))

export const getBody = createSelector(getResponse, (state) => _.get(state, 'body', {}))

export const getTitle = createSelector(getBody, (state) => _.get(state, 'title', ''))

export const getDiscountDate = createSelector(getBody, (state) => _.get(state, 'discountDate', ''))

export const getActDate = createSelector(getBody, (state) => _.get(state, 'order.actDate', ''))

export const getVehicle = createSelector(getBody, (state) => _.get(state, 'vehicle', ''))

export const getOrder = createSelector(getBody, (state) => _.get(state, 'order', ''))

export const getRequisite = createSelector(getBody, (state) => _.get(state, 'requisite', ''))

export const getIsFoundByOrganization = createSelector(getVehicle, (state) => _.get(state, 'findByOrganization', false))

export const getIsNotVehicleInDB = createSelector(getVehicle, (state) => !_.get(state, 'regSign'))

export const getHasPhoto = createSelector(getBody, (state) => _.get(state, 'order.hasPhoto', false))

export const getWithValue = (list) => list.filter((item) => item.value)

export const getVehiclesFields = createSelector(getVehicle, (state) =>
    getWithValue([
        {
            key: 'group',
            label: 'fines.details.vehicle.group',
            value: _.get(state, 'groupsIdentifiers[0]', null)
        },
        {
            key: 'reg-sign',
            label: 'fines.details.vehicle.reg.sign',
            value: _.get(state, 'regSign', i18next.t('fines.common.tooltip.default')),
        },
        {
            key: 'sts',
            label: 'fines.details.vehicle.sts',
            value: _.get(state, 'sts', i18next.t('fines.common.tooltip.default')),
            type: 'sts',
        },
        {
            key: 'comment',
            label: 'fines.details.vehicle.comment',
            value: _.get(state, 'comment', null),
        },
    ])
)

export const getViolationFields = createSelector(getOrder, (state) =>
    getWithValue([
        {
            key: 'violation-date',
            label: 'fines.details.violation.panel.violation.date',
            value: _.get(state, 'violationDate', null),
            type: 'date',
        },
        {
            key: 'act-date',
            label: 'fines.details.violation.panel.act.date',
            value: _.get(state, 'actDate', null),
            type: 'date',
        },
        {
            key: 'total-amount',
            label: 'fines.details.violation.panel.total.amount',
            value: _.get(state, 'totalAmount', null),
            type: 'sum',
        },
        {
            key: 'total-amount-with-discount',
            label: 'fines.details.violation.panel.total.amount.with.discount',
            value: _.get(state, 'payAmount', null),
            type: 'mainSum',
        },
        {
            key: 'discount-date',
            label: 'fines.details.violation.panel.discount.date',
            value: _.get(state, 'discountDate', null),
            type: 'discountDate',
        },
        {
            key: 'violation-address',
            label: 'fines.details.violation.panel.violation.address',
            value: _.get(state, 'violationAddress', null),
        },
    ])
)

export const getPaymentRequisitesFields = createSelector(getRequisite, (state) =>
    getWithValue([
        {
            key: 'uin',
            label: 'fines.details.payment.requisites.panel.uin',
            value: _.get(state, 'uin', null),
        },
        {
            key: 'inn-kpp',
            label: 'fines.details.payment.requisites.panel.inn.kpp',
            value: `${_.get(state, 'inn', null)} / ${_.get(state, 'kpp', null)}`,
        },
        {
            key: 'department-name',
            label: 'fines.details.payment.requisites.panel.department.name',
            value: _.get(state, 'recipient', null),
        },
        {
            key: 'receiver',
            label: 'fines.details.payment.requisites.panel.receiver',
            value: _.get(state, 'subdivision', null),
        },
        {
            key: 'bank',
            label: 'fines.details.payment.requisites.panel.bank',
            value: _.get(state, 'payeesBank', null),
        },
        {
            key: 'account',
            label: 'fines.details.payment.requisites.panel.account',
            value: _.get(state, 'recipientsAccountNumber', null),
        },
        {
            key: 'bik',
            label: 'fines.details.payment.requisites.panel.bik',
            value: _.get(state, 'bicPayeesBank', null),
        },
        {
            key: 'kbk',
            label: 'fines.details.payment.requisites.panel.kbk',
            value: _.get(state, 'kbk', null),
        },
        {
            key: 'oktmo',
            label: 'fines.details.payment.requisites.panel.oktmo',
            value: _.get(state, 'okato', null),
        },
    ])
)

export const getOtherRequisitesFields = createSelector(getBody, (state) =>
    getWithValue([
        {
            key: 'payment-target',
            label: 'fines.details.other.requisites.panel.payment.target',
            value: _.get(state, 'order.purpose', null),
        },
        {
            key: 'violation-person',
            label: 'fines.details.other.requisites.panel.violation.person',
            value: _.get(state, 'vehicle.organizationName', null),
        },
    ])
)

export const getIsDialogOpen = createSelector(getStore, (state) => _.get(state, 'isDialogOpen', false))

export const getUin = createSelector(getStore, (state) => _.get(state, 'uin', false))
