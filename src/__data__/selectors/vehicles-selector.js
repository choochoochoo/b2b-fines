import { createSelector } from 'reselect'
import _ from 'lodash'

export const getStore = createSelector((state) => _.get(state, 'vehicles'), (state) => state)

export const getVehicles = createSelector(getStore, (state) => _.get(state, 'responseVehicles.body.vehicles', []))

export const getGroups = createSelector(getStore, (state) => _.get(state, 'responseVehicles.body.groups', []))

export const getIsLoading = createSelector(getStore, (state) => _.get(state, 'isLoading', true))

export const getVehiclesQuantity = createSelector(getStore, (state) => _.get(state, 'responseVehicles.body.quantity', 0))

export const getHasMore = createSelector(getStore, (state) => _.get(state, 'responseVehicles.body.hasMore', false))

export const getIsOpenCreateGroupDialog = createSelector(getStore, (state) => _.get(state, 'isOpenCreateGroupDialog', false))

export const getIsOpenMoveVehiclesDialog = createSelector(getStore, (state) => _.get(state, 'isOpenMoveVehiclesDialog', false))

export const getIsOpenMoveCreateVehiclesDialog = createSelector(getStore, (state) => _.get(state, 'isOpenMoveCreateVehiclesDialog', false))

export const getCheckedVehicles = createSelector(getVehicles, (state) => state.filter((item) => item.checked))

export const getCheckedVehiclesArray = createSelector(getCheckedVehicles, (state) => state.filter((item) => item.checked).map((item) => item.id))

export const getCheckedVehiclesQuantity = createSelector(getCheckedVehicles, (state) => state.length)

