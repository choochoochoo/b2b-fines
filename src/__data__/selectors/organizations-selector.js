import { createSelector } from 'reselect'
import _ from 'lodash'

export const getStore = createSelector((state) => _.get(state, 'organizations'), (state) => state)

export const getOrganizations = createSelector(getStore, (state) =>
    _.get(state, 'responseOrganizations.body.organizations', []))

export const getFirstOrganization = createSelector(getStore, (state) =>
    _.get(state, 'responseOrganizations.body.organizations[0]', {}))

export const getIsOpenEditOrganizationDialog = createSelector(getStore, (state) =>
    _.get(state, 'isOpenEditOrganizationDialog', false))

export const getEditOrganizationDialogId = createSelector(getStore, (state) =>
    _.get(state, 'editOrganizationDialogId', null))

export const getIsOpenRemoveOrganizationDialog = createSelector(getStore, (state) =>
    _.get(state, 'isOpenRemoveOrganizationDialog', false))

export const getRemoveOrganizationDialogId = createSelector(getStore, (state) =>
    _.get(state, 'removeOrganizationDialogId', null))

export const getRemoveOrganizationDialogName = createSelector(getStore, (state) =>
    _.get(state, 'removeOrganizationDialogName', null))
