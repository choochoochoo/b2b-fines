import { createSelector } from 'reselect'
import _ from 'lodash'

export const getStore = createSelector((state) => _.get(state, 'branches'), (state) => state)

export const getIsOpenCreateBranchDialog = createSelector(getStore, (state) =>
    _.get(state, 'isOpenCreateBranchDialog', false))

export const getIsOpenEditBranchDialog = createSelector(getStore, (state) =>
    _.get(state, 'isOpenEditBranchDialog', false))

export const getEditBranchDialogId = createSelector(getStore, (state) =>
    _.get(state, 'editBranchDialogId', false))

export const getIsOpenRemoveBranchDialog = createSelector(getStore, (state) =>
    _.get(state, 'isOpenRemoveBranchDialog', false))

export const getRemoveBranchDialogId = createSelector(getStore, (state) =>
    _.get(state, 'removeBranchDialogId', false))

export const getRemoveBranchDialogName = createSelector(getStore, (state) =>
    _.get(state, 'removeBranchDialogName', false))

export const getRemoveBranchDialogOrganizationId = createSelector(getStore, (state) =>
    _.get(state, 'removeBranchDialogOrganizationId', false))
