import React from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'
import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux'
import i18next from 'i18next'

import { selectors, actions } from '../../../__data__'
import { Dialog } from '../../general'
import { HISTORY } from '../../../index'

import { RemoveGroupForm } from './remove-group-form'
import style from './remove-group-dialog.css'

export const RemoveGroupDialogPure = (
    {
        removeGroup,
        opened,
        closeRemoveGroupDialog,
        groupId,
        groupName,
    }) => {

    function handleSubmit () {
        removeGroup({
            groupId,
            onSuccessHandler: () => {
                HISTORY.push('/vehicles')
            }
        })
    }

    return (
        <Dialog opened={opened} closeClick={closeRemoveGroupDialog} width="544px">
            <div className={style.title}>
                {i18next.t('groups.remove.group.dialog.title', { GROUP_NAME: groupName })}
            </div>
            <div className={style.message}>
                {i18next.t('groups.remove.group.dialog.message')}
            </div>
            <RemoveGroupForm
                onSubmit={handleSubmit}
                cancelClick={closeRemoveGroupDialog}
            />
        </Dialog>
    )
}

RemoveGroupDialogPure.propTypes = {
    removeGroup: PropTypes.func,
    opened: PropTypes.bool,
    closeRemoveGroupDialog: PropTypes.func,
    groupId: PropTypes.string,
    groupName: PropTypes.string,
}

RemoveGroupDialogPure.defaultProps = {
    removeGroup: _.noop,
    opened: false,
    closeRemoveGroupDialog: _.noop,
    groupId: '',
    groupName: '',
}

const mapStateToProps = () => createStructuredSelector({
    opened: selectors.group.getIsOpenRemoveGroupDialog,
})

const mapDispatchToProps = (dispatch) => ({
    removeGroup: (param) => dispatch(actions.group.removeGroup(param)),
    closeRemoveGroupDialog: () => dispatch(actions.group.closeRemoveGroupDialog()),
})

export const RemoveGroupDialog = connect(mapStateToProps, mapDispatchToProps)(RemoveGroupDialogPure)
