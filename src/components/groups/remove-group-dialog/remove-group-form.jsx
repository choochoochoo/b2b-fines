import React from 'react'
import { reduxForm } from 'redux-form'
import i18next from 'i18next'
import PropTypes from 'prop-types'
import _ from 'lodash'

import { Button } from '../../general'

import style from './remove-group-form.css'

const RemoveGroupFormPure = (props) => {
    const { handleSubmit, cancelClick } = props

    return (
        <form onSubmit={handleSubmit} >

            <Button
                colorScheme="dialog-pink"
                title={i18next.t('groups.remove.group.dialog.button.remove')}
                type="submit"
                className={style.buttonRemove}
            />

            <Button
                colorScheme="dialog-green"
                title={i18next.t('groups.remove.group.dialog.button.cancel')}
                type="button"
                onClick={cancelClick}
            />
        </form>
    )
}

RemoveGroupFormPure.propTypes = {
    cancelClick: PropTypes.func,
    handleSubmit: PropTypes.func,
}

RemoveGroupFormPure.defaultProps = {
    cancelClick: _.noop,
    handleSubmit: _.noop,
}

export const RemoveGroupForm = reduxForm({
    form: 'RemoveGroupForm',
    validate: null,
})(RemoveGroupFormPure)

