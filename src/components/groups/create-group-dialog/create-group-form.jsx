import React from 'react'
import { reduxForm } from 'redux-form'
import i18next from 'i18next'
import PropTypes from 'prop-types'
import _ from 'lodash'

import { InputField, Button } from '../../general'

import style from './create-group-form.css'

export const GROUP_NAME_REGEXP = /^[A-Za-zА-Яа-я0-9!"№:,.;()_+<>?\s]{0,}$/
export const FORM_NAME = 'CreateGroupForm'

export const validate = (values) => {
    const errors = {}

    if (!values.groupName) {
        errors.groupName = i18next.t('input.field.required')
    } else if (!GROUP_NAME_REGEXP.test(values.groupName)) {
        errors.groupName = i18next.t('groups.create.group.dialog.validation.regexp.name.error')
    }

    return errors
}

const CreateGroupFormPure = (props) => {
    const { handleSubmit, valid } = props

    return (
        <form onSubmit={handleSubmit} >
            <InputField
                name="groupName"
                required
                type="text"
                placeholder={i18next.t('groups.create.group.dialog.input.name.label')}
                label={i18next.t('groups.create.group.dialog.input.name.label')}
            />

            <div className={style.comment}>
                <p className={style.commentUpper}>{i18next.t('groups.create.group.dialog.description1')}</p>
                <p>{i18next.t('groups.create.group.dialog.description2')}</p>
            </div>

            <Button
                colorScheme="big-green"
                title={i18next.t('groups.create.group.dialog.button.create')}
                disabled={!valid}
                type="submit"
            />
        </form>
    )
}

CreateGroupFormPure.propTypes = {
    valid: PropTypes.bool,
    handleSubmit: PropTypes.func,
}

CreateGroupFormPure.defaultProps = {
    valid: false,
    handleSubmit: _.noop,
}

export const CreateGroupForm = reduxForm({
    form: FORM_NAME,
    validate
})(CreateGroupFormPure)

