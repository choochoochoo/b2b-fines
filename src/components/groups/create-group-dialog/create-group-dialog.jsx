import React from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'
import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux'
import i18next from 'i18next'
import { reset } from 'redux-form'

import { selectors, actions } from '../../../__data__'
import { Dialog, useEscapeClick, useOutsideClick } from '../../general'

import { CreateGroupForm, FORM_NAME } from './create-group-form'
import style from './create-group-dialog.css'

const dialogRef = React.createRef()
const modalRef = React.createRef()

export const CreateGroupDialogPure = (
    {
        createGroup,
        opened,
        closeCreateGroupDialog,
        resetForm,
    }) => {

    function handleSubmit (values) {
        createGroup({
            name: _.trim(values.groupName),
            onSuccessHandler: () => resetForm(FORM_NAME),
        })
    }

    useEscapeClick(modalRef, closeCreateGroupDialog)
    useOutsideClick(dialogRef, closeCreateGroupDialog)

    return (
        <Dialog
            opened={opened}
            closeClick={closeCreateGroupDialog}
            width="624px"
            modalRef={modalRef}
            dialogRef={dialogRef}
        >
            <div className={style.title}>{i18next.t('groups.create.group.dialog.title')}</div>
            <CreateGroupForm
                onSubmit={handleSubmit}
            />
        </Dialog>
    )
}

CreateGroupDialogPure.propTypes = {
    createGroup: PropTypes.func,
    opened: PropTypes.bool,
    closeCreateGroupDialog: PropTypes.func,
    resetForm: PropTypes.func,
}

CreateGroupDialogPure.defaultProps = {
    createGroup: _.noop,
    opened: false,
    closeCreateGroupDialog: _.noop,
    resetForm: _.noop,
}

const mapStateToProps = () => createStructuredSelector({
    opened: selectors.vehicles.getIsOpenCreateGroupDialog,
})

const mapDispatchToProps = (dispatch) => ({
    createGroup: (param) => dispatch(actions.group.createGroup(param)),
    closeCreateGroupDialog: () => dispatch(actions.group.closeCreateGroupDialog()),
    resetForm: (formName) => dispatch(reset(formName)),
})

export const CreateGroupDialog = connect(mapStateToProps, mapDispatchToProps)(CreateGroupDialogPure)
