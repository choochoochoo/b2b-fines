import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import _ from 'lodash'

import * as actions from '../../__data__/actions'
import * as selectors from '../../__data__/selectors'
import { Template, Navbar, getCurrentPage, DialogError, Loader, } from '../general'
import { EditGroupDialog, RemoveGroupDialog, MoveVehiclesDialog, MoveCreateVehiclesDialog } from '../groups'
import { Table } from '../vehicles/table'
import { VehiclesSelector } from '../vehicles/vehicles-selector'
import { hasOwnGroups } from '../vehicles/vehicles'
import { HISTORY } from '../../'

import { Header } from './header'

export const GroupPure = (
    {
        location,
        match,
        group,
        isLoading,
        hasMore,
        getGroup,
        vehicles,
        openEditGroupDialog,
        openRemoveGroupDialog,
        quantity,
        selectGroupVehicle,
        deselectGroupVehicle,
        selectAllGroupVehicles,
        deselectAllGroupVehicles,
        groups,
        openMoveVehiclesDialog,
        openMoveCreateVehiclesDialog,
        selectedVehicles,
        originalGroups,
        groupId,
    }) => {
    useEffect(() => {
        getGroup({
            groupId: match.params.id,
            page: getCurrentPage(location)
        })
    }, [location])

    function handleAllGroupVehicles (event) {
        if (event.target.checked) {
            selectAllGroupVehicles()
        } else {
            deselectAllGroupVehicles()
        }
    }

    function successMove () {
        HISTORY.push(`/groups/${groupId}`)
    }

    return (
        <Template>
            <Navbar />
            {
                !isLoading && (
                    <React.Fragment>
                        <Header
                            groupId={group.id}
                            groupName={group.name}
                            groupQuantity={group.quantity}
                            openEditGroupDialog={openEditGroupDialog}
                            openRemoveGroupDialog={openRemoveGroupDialog}
                        />
                        <Table
                            hasMore={hasMore}
                            vehicles={vehicles}
                            location={location}
                            handleAllVehicles={handleAllGroupVehicles}
                            selectVehicle={selectGroupVehicle}
                            deselectVehicle={deselectGroupVehicle}
                        />
                    </React.Fragment>
                )
            }
            <DialogError />
            <Loader />
            <MoveVehiclesDialog
                groups={groups}
                quantity={quantity}
                selectedVehicles={selectedVehicles}
                onSuccessExecute={successMove}
            />
            <MoveCreateVehiclesDialog
                quantity={quantity}
                selectedVehicles={selectedVehicles}
                onSuccessExecute={successMove}
            />
            <VehiclesSelector
                quantity={quantity}
                openDialog={hasOwnGroups(originalGroups) ? openMoveVehiclesDialog : openMoveCreateVehiclesDialog}
            />
            <EditGroupDialog
                groupId={groupId}
                groupName={group.name}
            />
            <RemoveGroupDialog
                groupId={group.id}
                groupName={group.name}
            />
        </Template>
    )
}

GroupPure.propTypes = {
    location: PropTypes.object,
    match: PropTypes.object,
    group: PropTypes.object,
    isLoading: PropTypes.bool,
    hasMore: PropTypes.bool,
    getGroup: PropTypes.func,
    vehicles: PropTypes.array,
    openEditGroupDialog: PropTypes.func,
    openRemoveGroupDialog: PropTypes.func,
    quantity: PropTypes.number,
    selectGroupVehicle: PropTypes.func,
    deselectGroupVehicle: PropTypes.func,
    selectAllGroupVehicles: PropTypes.func,
    deselectAllGroupVehicles: PropTypes.func,
    groups: PropTypes.array,
    openMoveVehiclesDialog: PropTypes.func,
    openMoveCreateVehiclesDialog: PropTypes.func,
    selectedVehicles: PropTypes.array,
    originalGroups: PropTypes.array,
    groupId: PropTypes.number,
}

GroupPure.defaultProps = {
    location: {},
    match: {},
    group: {},
    isLoading: false,
    hasMore: false,
    getGroup: _.noop,
    vehicles: [],
    openEditGroupDialog: _.noop,
    openRemoveGroupDialog: _.noop,
    quantity: 0,
    selectGroupVehicle: _.noop,
    deselectGroupVehicle: _.noop,
    selectAllGroupVehicles: _.noop,
    deselectAllGroupVehicles: _.noop,
    groups: [],
    openMoveVehiclesDialog: _.noop,
    openMoveCreateVehiclesDialog: _.noop,
    selectedVehicles: [],
    originalGroups: [],
    groupId: 0,
}

const mapStateToProps = () => createStructuredSelector({
    vehicles: selectors.group.getVehicles,
    group: selectors.group.getGroup,
    isLoading: selectors.group.getIsLoading,
    hasMore: selectors.group.getHasMore,
    quantity: selectors.group.getCheckedVehiclesQuantity,
    groups: selectors.group.getGroupsWithoutCurrent,
    originalGroups: selectors.group.getOriginalGroups,
    selectedVehicles: selectors.group.getCheckedVehiclesArray,
    groupId: selectors.group.getGroupId,
})

const mapDispatchToProps = (dispatch) => ({
    getGroup: (param) => dispatch(actions.group.getGroup(param)),
    openEditGroupDialog: () => dispatch(actions.group.openEditGroupDialog()),
    openRemoveGroupDialog: () => dispatch(actions.group.openRemoveGroupDialog()),
    selectGroupVehicle: (id) => dispatch(actions.group.selectGroupVehicle(id)),
    deselectGroupVehicle: (id) => dispatch(actions.group.deselectGroupVehicle(id)),
    selectAllGroupVehicles: () => dispatch(actions.group.selectAllGroupVehicles()),
    deselectAllGroupVehicles: () => dispatch(actions.group.deselectAllGroupVehicles()),
    openMoveVehiclesDialog: () => dispatch(actions.group.openMoveVehiclesDialog()),
    openMoveCreateVehiclesDialog: () => dispatch(actions.group.openMoveCreateVehiclesDialog()),
})

export const Group = connect(mapStateToProps, mapDispatchToProps)(GroupPure)
