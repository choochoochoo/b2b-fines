import React from 'react'
import { reduxForm } from 'redux-form'
import i18next from 'i18next'
import PropTypes from 'prop-types'
import _ from 'lodash'

import { InputField, Button } from '../../general'
import { validate } from '../create-group-dialog'

import styles from './move-create-vehicles-dialog-form.css'

export const FORM_NAME = 'MoveCreateVehiclesForm'

const MoveCreateVehiclesFormPure = (props) => {
    const { handleSubmit, valid } = props

    return (
        <form onSubmit={handleSubmit}>
            <InputField
                name="groupName"
                required
                type="text"
                placeholder={i18next.t('groups.create.group.dialog.input.name.label')}
                label={i18next.t('groups.create.group.dialog.input.name.label')}
            />

            <Button
                colorScheme="big-green"
                title={i18next.t('groups.move.vehicles.dialog.select.group.button.move')}
                disabled={!valid}
                type="submit"
                className={styles.button}
            />
        </form>
    )
}

MoveCreateVehiclesFormPure.propTypes = {
    valid: PropTypes.bool,
    handleSubmit: PropTypes.func,
}

MoveCreateVehiclesFormPure.defaultProps = {
    valid: false,
    handleSubmit: _.noop,
}

export const MoveCreateVehiclesForm = reduxForm({
    form: FORM_NAME,
    validate
})(MoveCreateVehiclesFormPure)

