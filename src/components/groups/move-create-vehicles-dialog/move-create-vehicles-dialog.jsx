import React from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'
import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux'
import i18next from 'i18next'
import { reset } from 'redux-form'

import { selectors, actions } from '../../../__data__'
import { Dialog } from '../../general'

import { MoveCreateVehiclesForm, FORM_NAME } from './move-create-vehicles-dialog-form'
import style from './move-create-vehicles-dialog.css'

export const MoveCreateVehiclesDialogPure = (
    {
        createGroup,
        opened,
        closeMoveCreateVehiclesDialog,
        quantity,
        selectedVehicles,
        resetForm,
        onSuccessExecute,
    }) => {

    function handleSubmit (values) {
        createGroup({
            name: _.trim(values.groupName),
            documentsIds: selectedVehicles,

            onSuccessHandler: () => {
                resetForm(FORM_NAME)

                if (onSuccessExecute) {
                    onSuccessExecute()
                }
            },
        })
    }

    return (
        <Dialog opened={opened} closeClick={closeMoveCreateVehiclesDialog} width="464px">
            <div className={style.title}>
                {i18next.t('groups.move.vehicles.dialog.new.group.title')}
            </div>
            <div className={style.description}>
                {i18next.t('groups.move.vehicles.dialog.select.group.description', { QUANTITY: quantity })}
            </div>
            <MoveCreateVehiclesForm
                onSubmit={handleSubmit}
            />
        </Dialog>
    )
}

MoveCreateVehiclesDialogPure.propTypes = {
    createGroup: PropTypes.func,
    opened: PropTypes.bool,
    closeMoveCreateVehiclesDialog: PropTypes.func,
    quantity: PropTypes.number,
    selectedVehicles: PropTypes.array,
    resetForm: PropTypes.func,
    onSuccessExecute: PropTypes.func,
}

MoveCreateVehiclesDialogPure.defaultProps = {
    createGroup: _.noop,
    opened: false,
    closeMoveCreateVehiclesDialog: _.noop,
    quantity: 0,
    selectedVehicles: [],
    resetForm: _.noop,
    onSuccessExecute: _.noop,
}

const mapStateToProps = () => createStructuredSelector({
    opened: selectors.vehicles.getIsOpenMoveCreateVehiclesDialog,
})

const mapDispatchToProps = (dispatch) => ({
    createGroup: (param) => dispatch(actions.group.createGroup(param)),
    closeMoveCreateVehiclesDialog: () => dispatch(actions.group.closeMoveCreateVehiclesDialog()),
    resetForm: (formName) => dispatch(reset(formName)),
})

export const MoveCreateVehiclesDialog = connect(mapStateToProps, mapDispatchToProps)(MoveCreateVehiclesDialogPure)
