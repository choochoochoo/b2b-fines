import React from 'react'
import { reduxForm } from 'redux-form'
import i18next from 'i18next'
import PropTypes from 'prop-types'
import _ from 'lodash'
import { connect } from 'react-redux'

import { RadioField, Button } from '../../general'
import { GROUP_NAME_REGEXP } from '../create-group-dialog'

import { RadioInputField } from './radio-input-field'
import style from './move-vehicles-form.css'

export const RADIO_GROUP_NAME = 'selectGroup'
export const NEW_GROUP_FIELD = 'newGroup'
export const NEW_GROUP_INPUT_FIELD = 'newGroupNameInput'
export const FORM_NAME = 'MoveVehiclesForm'

const newGroupTextInputRef = React.createRef()

const validate = (values) => {
    const errors = {}

    if (values[RADIO_GROUP_NAME] !== NEW_GROUP_FIELD) {
        return {}
    }

    if (!values[NEW_GROUP_INPUT_FIELD]) {
        errors[NEW_GROUP_INPUT_FIELD] = i18next.t('input.field.required')
    } else if (!GROUP_NAME_REGEXP.test(values[NEW_GROUP_INPUT_FIELD])) {
        errors[NEW_GROUP_INPUT_FIELD] = i18next.t('groups.create.group.dialog.validation.regexp.name.error')
    }

    return errors
}

const MoveVehiclesFormPure = (props) => {
    const {
        handleSubmit,
        valid,
        groups,
        change,
        focusForm,
        setShowNewButton,
        showNewButton,
    } = props

    function visibleNewButton () {
        setShowNewButton(true)

        change(RADIO_GROUP_NAME, NEW_GROUP_FIELD)
        focusForm(FORM_NAME, NEW_GROUP_INPUT_FIELD)

        setTimeout(() => {
            newGroupTextInputRef.current.focus()
        }, 10)
    }

    return (
        <form onSubmit={handleSubmit}>
            {
                groups.map((item) => (
                    <RadioField
                        key={item.id.toString()}
                        name={RADIO_GROUP_NAME}
                        label={item.name}
                        value={item.id.toString()}
                    />
                ))
            }
            {
                !showNewButton &&
                <button type="button" className={style.buttonNew} onClick={visibleNewButton}>
                    <div className={style.buttonNewText}>
                        {i18next.t('groups.move.vehicles.dialog.select.group.button.new.group')}
                    </div>
                </button>
            }
            {
                showNewButton &&
                <RadioInputField
                    radioName={RADIO_GROUP_NAME}
                    radioValue={NEW_GROUP_FIELD}
                    newGroupTextInputRef={newGroupTextInputRef}
                    newGroupTextFieldName={NEW_GROUP_INPUT_FIELD}
                />
            }
            <Button
                colorScheme="big-green"
                title={i18next.t('groups.move.vehicles.dialog.select.group.button.move')}
                disabled={!valid}
                type="submit"
                className={style.button}
            />
        </form>
    )
}

MoveVehiclesFormPure.propTypes = {
    valid: PropTypes.bool,
    handleSubmit: PropTypes.func,
    groups: PropTypes.array,
    change: PropTypes.func,
    focusForm: PropTypes.func,
    setShowNewButton: PropTypes.func,
    showNewButton: PropTypes.bool,
}

MoveVehiclesFormPure.defaultProps = {
    valid: false,
    handleSubmit: _.noop,
    groups: [],
    change: _.noop,
    focusForm: _.noop,
    setShowNewButton: _.noop,
    showNewButton: false,
}

export const MoveVehiclesFormRF = reduxForm({
    form: FORM_NAME,
    validate,
    enableReinitialize: true,
})(MoveVehiclesFormPure)


export const MoveVehiclesForm = connect(
    (state, ownParam) => {
        const initialValues = {}

        initialValues[RADIO_GROUP_NAME] = _.get(ownParam, 'groups[0].id', '').toString()

        return {
            initialValues: { ...initialValues }
        }
    }, null, null, { forwardRef: true }
)(MoveVehiclesFormRF)

