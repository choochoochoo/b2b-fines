import React from 'react'
import PropTypes from 'prop-types'
import { Field } from 'redux-form'

import styles from './radio-input-field.css'

const renderField = (
    {
        input,
        type,
    }) =>
    (
        <input
            {...input}
            className={styles.radioInput}
            type={type}
        />
    )

renderField.propTypes = {
    input: PropTypes.object,
    type: PropTypes.string,
}

renderField.defaultProps = {
    input: {},
    type: '',
}

const renderField2 = (
    {
        input,
        meta: { error, active },
        label,
        type,
        newGroupTextInputRef,
        radioName,
        radioValue,
    }) =>
    (
        <div>
            <div className={styles.root}>
                <Field
                    name={radioName}
                    type="radio"
                    component={renderField}
                    label={label}
                    value={radioValue}
                />
                <div>
                    <input
                        {...input}
                        className={styles.textInput}
                        type={type}
                        ref={newGroupTextInputRef}
                    />
                </div>
            </div>
            {
                !active &&
                <div className={styles.error}>
                    {error}
                </div>
            }
        </div>
    )

renderField2.propTypes = {
    input: PropTypes.object,
    meta: PropTypes.object,
    type: PropTypes.string,
    label: PropTypes.string,
    newGroupTextInputRef: PropTypes.object,
    radioName: PropTypes.string,
    radioValue: PropTypes.string,
}

renderField2.defaultProps = {
    input: {},
    meta: {},
    type: '',
    label: '',
    newGroupTextInputRef: React.createRef(),
    radioName: '',
    radioValue: '',
}

export const RadioInputField = ({ radioName, radioValue, newGroupTextInputRef, newGroupTextFieldName }) =>
    (
        <Field
            name={newGroupTextFieldName}
            type="text"
            component={renderField2}
            radioValue={radioValue}
            newGroupTextInputRef={newGroupTextInputRef}
            radioName={radioName}
        />
    )

RadioInputField.propTypes = {
    radioName: PropTypes.string,
    radioValue: PropTypes.string,
    newGroupTextInputRef: PropTypes.object,
    newGroupTextFieldName: PropTypes.string,
}

RadioInputField.defaultProps = {
    radioName: '',
    radioValue: '',
    newGroupTextInputRef: React.createRef(),
    newGroupTextFieldName: '',
}
