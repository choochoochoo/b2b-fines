import React, { useState } from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'
import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux'
import i18next from 'i18next'
import { focus, reset } from 'redux-form'

import { selectors, actions } from '../../../__data__'
import { Dialog } from '../../general'

import {
    MoveVehiclesForm,
    RADIO_GROUP_NAME,
    NEW_GROUP_FIELD,
    NEW_GROUP_INPUT_FIELD,
    FORM_NAME,
} from './move-vehicles-form'
import style from './move-vehicles-dialog.css'

export const MoveVehiclesDialogPure = (
    {
        createGroup,
        editGroupAddingVehicles,
        opened,
        closeMoveVehiclesDialog,
        quantity,
        groups,
        focusForm,
        selectedVehicles,
        resetForm,
        onSuccessExecute,
    }) => {

    const [showNewButton, setShowNewButton] = useState(false)

    function close () {
        closeMoveVehiclesDialog()
        resetForm(FORM_NAME)
        setShowNewButton(false)
    }

    function handleSubmit (values) {
        if (values[RADIO_GROUP_NAME] === NEW_GROUP_FIELD) {
            createGroup({
                name: values[NEW_GROUP_INPUT_FIELD],
                documentsIds: selectedVehicles,
                onSuccessHandler: () => {
                    close()
                    if (onSuccessExecute) {
                        onSuccessExecute()
                    }
                },
                onErrorHandler: () => close,
            })
        } else {
            editGroupAddingVehicles({
                groupId: values[RADIO_GROUP_NAME],
                documentsIds: selectedVehicles,
                onSuccessHandler: () => {
                    close()
                    if (onSuccessExecute) {
                        onSuccessExecute()
                    }
                },
                onErrorHandler: () => close,
            })
        }
    }

    return (
        <Dialog opened={opened} closeClick={close} width="464px">
            <div className={style.title}>
                {i18next.t('groups.move.vehicles.dialog.select.group.title')}
            </div>
            <div className={style.description}>
                {i18next.t('groups.move.vehicles.dialog.select.group.description', { QUANTITY: quantity })}
            </div>
            <MoveVehiclesForm
                onSubmit={handleSubmit}
                groups={groups}
                focusForm={focusForm}
                showNewButton={showNewButton}
                setShowNewButton={setShowNewButton}
            />
        </Dialog>
    )
}

MoveVehiclesDialogPure.propTypes = {
    createGroup: PropTypes.func,
    editGroupAddingVehicles: PropTypes.func,
    opened: PropTypes.bool,
    closeMoveVehiclesDialog: PropTypes.func,
    quantity: PropTypes.number,
    groups: PropTypes.array,
    focusForm: PropTypes.func,
    selectedVehicles: PropTypes.array,
    resetForm: PropTypes.func,
    onSuccessExecute: PropTypes.func,
}

MoveVehiclesDialogPure.defaultProps = {
    createGroup: _.noop,
    editGroupAddingVehicles: _.noop,
    opened: false,
    closeMoveVehiclesDialog: _.noop,
    quantity: 0,
    groups: [],
    focusForm: _.noop,
    selectedVehicles: [],
    resetForm: _.noop,
    onSuccessExecute: _.noop,
}

const mapStateToProps = () => createStructuredSelector({
    opened: selectors.vehicles.getIsOpenMoveVehiclesDialog,
})

const mapDispatchToProps = (dispatch) => ({
    createGroup: (param) => dispatch(actions.group.createGroup(param)),
    editGroupAddingVehicles: (param) => dispatch(actions.group.editGroupAddingVehicles(param)),
    closeMoveVehiclesDialog: () => dispatch(actions.group.closeMoveVehiclesDialog()),
    focusForm: (formName, fieldName) => dispatch(focus(formName, fieldName)),
    resetForm: (formName) => dispatch(reset(formName)),
})

export const MoveVehiclesDialog = connect(mapStateToProps, mapDispatchToProps)(MoveVehiclesDialogPure)
