import React from 'react'
import { reduxForm } from 'redux-form'
import i18next from 'i18next'
import PropTypes from 'prop-types'
import _ from 'lodash'
import { connect } from 'react-redux'

import { InputField, Button } from '../../general'
import { validate } from '../create-group-dialog'

import style from './edit-group-form.css'

export const FORM_NAME = 'EditGroupForm'

const EditGroupFormPure = (props) => {
    const { handleSubmit, valid } = props

    return (
        <form onSubmit={handleSubmit} >
            <InputField
                name="groupName"
                required
                type="text"
                placeholder={i18next.t('groups.create.group.dialog.input.name.label')}
                label={i18next.t('groups.create.group.dialog.input.name.label')}
            />

            <Button
                colorScheme="big-green"
                title={i18next.t('groups.edit.group.dialog.button.title')}
                disabled={!valid}
                type="submit"
                className={style.button}
            />
        </form>
    )
}

EditGroupFormPure.propTypes = {
    valid: PropTypes.bool,
    handleSubmit: PropTypes.func,
}

EditGroupFormPure.defaultProps = {
    valid: false,
    handleSubmit: _.noop,
}

export const EditGroupFormRF = reduxForm({
    form: FORM_NAME,
    validate,
    enableReinitialize: true,
})(EditGroupFormPure)

export const EditGroupForm = connect(
    (state, ownParam) => {
        const initialValues = {}

        initialValues.groupName = _.get(ownParam, 'groupName', '')

        return {
            initialValues: { ...initialValues }
        }
    }, null, null, { forwardRef: true }
)(EditGroupFormRF)
