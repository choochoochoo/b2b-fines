import React from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'
import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux'
import i18next from 'i18next'
import { reset } from 'redux-form'

import { selectors, actions } from '../../../__data__'
import { Dialog } from '../../general'

import { EditGroupForm, FORM_NAME } from './edit-group-form'
import style from './edit-group-dialog.css'

export const EditGroupDialogPure = (
    {
        editGroup,
        opened,
        closeEditGroupDialog,
        groupId,
        groupName,
        resetForm,
    }) => {

    function close () {
        closeEditGroupDialog()
        resetForm(FORM_NAME)
    }

    function handleSubmit (values) {
        editGroup({
            groupId,
            name: _.trim(values.groupName),
            onSuccessHandler: () => close(),
            onErrorHandler: () => close(),
        })
    }

    return (
        <Dialog opened={opened} closeClick={close} width="464px">
            <div className={style.title}>
                {i18next.t('groups.edit.group.dialog.title')}
            </div>
            <EditGroupForm
                onSubmit={handleSubmit}
                groupName={groupName}
            />
        </Dialog>
    )
}

EditGroupDialogPure.propTypes = {
    editGroup: PropTypes.func,
    opened: PropTypes.bool,
    closeEditGroupDialog: PropTypes.func,
    groupId: PropTypes.number,
    groupName: PropTypes.string,
    resetForm: PropTypes.func,
}

EditGroupDialogPure.defaultProps = {
    editGroup: _.noop,
    opened: false,
    closeEditGroupDialog: _.noop,
    groupId: 0,
    groupName: '',
    resetForm: _.noop,
}

const mapStateToProps = () => createStructuredSelector({
    opened: selectors.group.getIsOpenEditGroupDialog,
})

const mapDispatchToProps = (dispatch) => ({
    editGroup: (param) => dispatch(actions.group.editGroup(param)),
    closeEditGroupDialog: () => dispatch(actions.group.closeEditGroupDialog()),
    resetForm: (formName) => dispatch(reset(formName)),
})

export const EditGroupDialog = connect(mapStateToProps, mapDispatchToProps)(EditGroupDialogPure)
