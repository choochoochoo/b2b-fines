import React from 'react'
import i18next from 'i18next'
import PropTypes from 'prop-types'
import _ from 'lodash'
import { Link } from 'react-router-dom'

import { SvgIcon, Button } from '../general'

import styles from './header.css'

const isNotDefaultGroup = (groupId) => groupId !== 0

export const Header = (
    {
        groupId,
        groupName,
        groupQuantity,
        openEditGroupDialog,
        openRemoveGroupDialog,
    }) =>
    (
        <div className={styles.root}>
            <div className={styles.bread}>
                <Link
                    to="/vehicles"
                    className={styles.link}
                >
                    {i18next.t('group.header.title')}
                </Link>
                <div className={styles.breadArrowIcon}>
                    <SvgIcon
                        icon="breadArrow"
                        width="16"
                        height="16"
                    />
                </div>
                <div className={styles.breadText}>
                    {groupName}
                </div>
            </div>
            <div className={styles.titleRow}>
                <div className={styles.title}>
                    {groupName}
                </div>
                {
                    isNotDefaultGroup(groupId) &&
                    <button
                        className={styles.editIcon}
                        title={i18next.t('group.header.button.edit')}
                        onClick={openEditGroupDialog}
                    >
                        <SvgIcon
                            icon="pencil"
                        />
                    </button>
                }
                <div>
                    {
                        isNotDefaultGroup(groupId) &&
                        <Button
                            title={i18next.t('group.header.button.remove')}
                            colorScheme="second-pink"
                            className={styles.removeButton}
                            onClick={openRemoveGroupDialog}
                            type="button"
                        />
                    }
                </div>
            </div>
            <div className={styles.quantity}>
                {i18next.t('group.header.documents.quantity', { count: groupQuantity, QUANTITY: groupQuantity })}
            </div>
        </div>
    )

Header.propTypes = {
    groupId: PropTypes.number,
    groupName: PropTypes.string,
    groupQuantity: PropTypes.number,
    openEditGroupDialog: PropTypes.func,
    openRemoveGroupDialog: PropTypes.func,
}

Header.defaultProps = {
    groupId: 0,
    groupName: '',
    groupQuantity: 0,
    openEditGroupDialog: _.noop,
    openRemoveGroupDialog: _.noop,
}
