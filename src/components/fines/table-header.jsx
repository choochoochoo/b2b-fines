import React from 'react'
import PropTypes from 'prop-types'
import i18next from 'i18next'

import {
    formatMoney,
    formatDate,
    Filter,
} from '../general'

import style from './table-header.css'

export const TableHeader = (
    {
        quantity,
        lastCheckingFinesDate,
        totalAmount,
        filters,
        location,
    }) =>
    (
        <div className={style.root}>
            <div className={style.header}>
                <div>
                    <div className={style.title}>
                        {i18next.t('fines.title')} {quantity}
                    </div>
                    <div className={style.description}>
                        {i18next.t('fines.description')} {formatDate(lastCheckingFinesDate)}
                    </div>
                </div>
                <div>
                    <div className={style.totalAmount}>
                        {i18next.t('fines.total.amount', { TOTAL_AMOUNT: formatMoney(totalAmount) })}
                    </div>
                </div>
            </div>
            {
                filters.length > 0 && <Filter items={filters} location={location} />
            }
        </div>
    )

TableHeader.propTypes = {
    quantity: PropTypes.number,
    lastCheckingFinesDate: PropTypes.string,
    totalAmount: PropTypes.number,
    filters: PropTypes.array,
    location: PropTypes.object,
}

TableHeader.defaultProps = {
    quantity: 0,
    lastCheckingFinesDate: '',
    totalAmount: 0,
    filters: [],
    location: {},
}
