import React from 'react'
import PropTypes from 'prop-types'
import i18next from 'i18next'
import { Link } from 'react-router-dom'

import { formatDate } from '../general'

import styles from './file-not-uploaded.css'

export const NoCheckedFines = ({ nextCheckingFinesDate }) =>
    (
        <div className={styles.root}>
            <div className={styles.content}>
                <div className={styles.title}>
                    {i18next.t('no.checked.fines.title')}
                </div>
                <div className={styles.description}>
                    {i18next.t('no.checked.fines.description', { DATE: formatDate(nextCheckingFinesDate) })}
                </div>
                <Link
                    to="/vehicles"
                    className={styles.button}
                >
                    {i18next.t('no.checked.fines.button')}
                </Link>
            </div>
        </div>
    )

NoCheckedFines.propTypes = {
    nextCheckingFinesDate: PropTypes.string,
}

NoCheckedFines.defaultProps = {
    nextCheckingFinesDate: '',
}
