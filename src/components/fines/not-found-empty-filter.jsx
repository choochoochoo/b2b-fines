import React from 'react'
import PropTypes from 'prop-types'
import i18next from 'i18next'
import _ from 'lodash'

import styles from './file-not-uploaded.css'

export const NotFoundEmptyFilter = ({ click }) =>
    (
        <div className={styles.root}>
            <div className={styles.content}>
                <div className={styles.title}>
                    {i18next.t('fines.not.found.empty.filter.title')}
                </div>
                <div className={styles.description}>
                    {i18next.t('fines.not.found.empty.filter.description')}
                </div>

                <button
                    className={styles.button}
                    title={i18next.t('fines.not.found.empty.filter.button.text')}
                    onClick={click}
                >
                    <div className={styles.buttonText}>
                        {i18next.t('fines.not.found.empty.filter.button.text')}
                    </div>
                </button>
            </div>
        </div>
    )

NotFoundEmptyFilter.propTypes = {
    click: PropTypes.func,
}

NotFoundEmptyFilter.defaultProps = {
    click: _.noop,
}
