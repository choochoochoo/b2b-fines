import React from 'react'
import PropTypes from 'prop-types'
import i18next from 'i18next'
import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux'
import _ from 'lodash'

import * as selectors from '../../__data__/selectors'
import * as actions from '../../__data__/actions'
import { RowSelector } from '../general'

import style from './fines-selector.css'

export const FinesSelectorPure = ({ quantity, downloadSelectedFines, finesArray }) => {
    function clickHandle () {
        downloadSelectedFines({ data: finesArray })
    }

    return (
        <RowSelector>
            <div className={style.title}>
                {i18next.t('fines.row.selector.title', { QUANTITY: quantity })}
            </div>
            <div className={style.rowCol}>
                <div className={style.description}>
                    {i18next.t('fines.row.selector.description')}
                </div>
                <div>
                    <button className={style.button} onClick={clickHandle} disabled={quantity === 0}>
                        <div className={style.buttonText}>
                            {i18next.t('fines.row.selector.download.button')}
                        </div>
                    </button>
                </div>
            </div>
        </RowSelector>
    )
}

FinesSelectorPure.propTypes = {
    quantity: PropTypes.number,
    finesArray: PropTypes.array,
    downloadSelectedFines: PropTypes.func,
}

FinesSelectorPure.defaultProps = {
    quantity: 0,
    finesArray: [],
    downloadSelectedFines: _.noop,
}

const mapStateToProps = () => createStructuredSelector({
    quantity: selectors.fines.getCheckedFinesQuantity,
    finesArray: selectors.fines.getCheckedFinesArray,
})

const mapDispatchToProps = (dispatch) => ({
    downloadSelectedFines: (param) => dispatch(actions.downloadSelectedFines(param)),
})

export const FinesSelector = connect(mapStateToProps, mapDispatchToProps)(FinesSelectorPure)
