import React from 'react'
import PropTypes from 'prop-types'
import i18next from 'i18next'

import { formatDate } from '../general'

import styles from './file-not-uploaded.css'

export const NotFound = ({ lastCheckingFinesDate }) =>
    (
        <div className={styles.root}>
            <div className={styles.content}>
                <div className={styles.title}>
                    {i18next.t('fines.not.found.title')}
                </div>
                <div className={styles.description}>
                    {i18next.t('fines.not.found.description', { DATE: formatDate(lastCheckingFinesDate) })}
                </div>
            </div>
        </div>
    )

NotFound.propTypes = {
    lastCheckingFinesDate: PropTypes.string,
}

NotFound.defaultProps = {
    lastCheckingFinesDate: '',
}
