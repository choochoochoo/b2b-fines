import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import _ from 'lodash'

import * as actions from '../../__data__/actions'
import * as selectors from '../../__data__/selectors'
import { Template, Navbar, getCurrentPage, Loader, DialogError } from '../general'
import { formToFilter } from '../../__data__/reducers/filter-utils'
import { FineDetailsDialog } from '../fine-details'
import { HISTORY } from '../../index'

import { Table } from './table'
import { FileNotUploaded } from './file-not-uploaded'
import { NoCheckedFines } from './no-checked-fines'
import { NotFound } from './not-found'
import { NotFoundEmptyFilter } from './not-found-empty-filter'
import { getSort } from './sort-button'
import { TableHeader } from './table-header'
import { FinesSelector } from './fines-selector'

export const Content = (
    {
        isLoading,
        uploadVehiclesFile,
        fines,
        lastCheckingFinesDate,
        nextCheckingFinesDate,
        quantity,
        totalAmount,
        handleAllFines,
        selectFine,
        deselectFine,
        hasMore,
        location,
        filters,
        openFineDetailsDialog,
        hasAppliedFilter,
        clearAllFilters,
    }) => {

    if (isLoading) {
        return <div />
    }

    function clickButtonFilter () {
        clearAllFilters()
        HISTORY.push(`/fines?sort=${getSort(location)}`)
    }

    if (fines.length === 0 && hasAppliedFilter) {
        return (
            <React.Fragment>
                <TableHeader
                    quantity={quantity}
                    lastCheckingFinesDate={lastCheckingFinesDate}
                    totalAmount={totalAmount}
                    filters={filters}
                />
                <NotFoundEmptyFilter click={clickButtonFilter} />
            </React.Fragment>
        )
    }

    if (fines.length === 0 && lastCheckingFinesDate) {
        return (
            <React.Fragment>
                <TableHeader
                    quantity={quantity}
                    lastCheckingFinesDate={lastCheckingFinesDate}
                    totalAmount={totalAmount}
                    filters={filters}
                />
                <NotFound lastCheckingFinesDate={lastCheckingFinesDate} />
            </React.Fragment>
        )
    }

    if (!uploadVehiclesFile) {
        return <FileNotUploaded />
    }

    if (!lastCheckingFinesDate) {
        return <NoCheckedFines nextCheckingFinesDate={nextCheckingFinesDate} />
    }

    return (
        <div>
            <TableHeader
                quantity={quantity}
                lastCheckingFinesDate={lastCheckingFinesDate}
                totalAmount={totalAmount}
                filters={filters}
                location={location}
            />
            <Table
                handleAllFines={handleAllFines}
                fines={fines}
                selectFine={selectFine}
                deselectFine={deselectFine}
                hasMore={hasMore}
                location={location}
                openFineDetailsDialog={openFineDetailsDialog}
            />
            <FinesSelector />
        </div>
    )
}

Content.propTypes = {
    fines: PropTypes.array,
    quantity: PropTypes.number,
    totalAmount: PropTypes.number,
    lastCheckingFinesDate: PropTypes.string,
    nextCheckingFinesDate: PropTypes.string,
    hasMore: PropTypes.bool,
    location: PropTypes.object,
    uploadVehiclesFile: PropTypes.bool,
    isLoading: PropTypes.bool,
    selectFine: PropTypes.func,
    deselectFine: PropTypes.func,
    handleAllFines: PropTypes.func,
    filters: PropTypes.array,
    openFineDetailsDialog: PropTypes.func,
    hasAppliedFilter: PropTypes.bool,
    clearAllFilters: PropTypes.func,
}

Content.defaultProps = {
    fines: [],
    quantity: 0,
    totalAmount: 0,
    lastCheckingFinesDate: '',
    nextCheckingFinesDate: '',
    hasMore: false,
    location: {},
    uploadVehiclesFile: false,
    isLoading: false,
    selectFine: _.noop,
    deselectFine: _.noop,
    handleAllFines: _.noop,
    filters: [],
    openFineDetailsDialog: _.noop,
    hasAppliedFilter: false,
    clearAllFilters: _.noop,
}

export const FinesPure = (
    {
        getFines,
        openFineDetailsDialog,
        fines,
        quantity,
        totalAmount,
        lastCheckingFinesDate,
        nextCheckingFinesDate,
        hasMore,
        location,
        selectFine,
        deselectFine,
        selectAllFines,
        deselectAllFines,
        uploadVehiclesFile,
        isLoading,
        filters,
        form,
        prevParams,
        hasAppliedFilter,
        clearAllFilters,
        organizationFilter,
    }) => {
    useEffect(() => {

        if (organizationFilter) {
            getFines({
                page: getCurrentPage(location),
                sort: getSort(location),
                filters: [
                    {
                        key: 'organization',
                        set: [
                            {
                                key: organizationFilter,
                                selected: true
                            }
                        ]
                    }
                ]
            })
        } else {
            const newParams = {
                page: getCurrentPage(location),
                sort: getSort(location),
                filters: formToFilter(form, filters),
            }

            if (!_.isEqual(prevParams, newParams)) {
                getFines(newParams)
            }
        }

    }, [location])

    function handleAllFines (event) {
        if (event.target.checked) {
            selectAllFines()
        } else {
            deselectAllFines()
        }
    }

    return (
        <Template>
            <Navbar />
            <Content
                isLoading={isLoading}
                uploadVehiclesFile={uploadVehiclesFile}
                fines={fines}
                lastCheckingFinesDate={lastCheckingFinesDate}
                nextCheckingFinesDate={nextCheckingFinesDate}
                quantity={quantity}
                totalAmount={totalAmount}
                handleAllFines={handleAllFines}
                selectFine={selectFine}
                deselectFine={deselectFine}
                hasMore={hasMore}
                location={location}
                filters={filters}
                openFineDetailsDialog={openFineDetailsDialog}
                hasAppliedFilter={hasAppliedFilter}
                clearAllFilters={clearAllFilters}
            />
            <DialogError />
            <Loader />
            <FineDetailsDialog />
        </Template>
    )
}

FinesPure.propTypes = {
    fines: PropTypes.array,
    quantity: PropTypes.number,
    totalAmount: PropTypes.number,
    lastCheckingFinesDate: PropTypes.string,
    nextCheckingFinesDate: PropTypes.string,
    hasMore: PropTypes.bool,
    location: PropTypes.object,
    uploadVehiclesFile: PropTypes.bool,
    isLoading: PropTypes.bool,
    getFines: PropTypes.func,
    openFineDetailsDialog: PropTypes.func,
    selectFine: PropTypes.func,
    deselectFine: PropTypes.func,
    selectAllFines: PropTypes.func,
    deselectAllFines: PropTypes.func,
    filters: PropTypes.array,
    form: PropTypes.object,
    prevParams: PropTypes.object,
    hasAppliedFilter: PropTypes.bool,
    clearAllFilters: PropTypes.func,
    organizationFilter: PropTypes.string,
}

FinesPure.defaultProps = {
    fines: [],
    quantity: 0,
    totalAmount: 0,
    lastCheckingFinesDate: '',
    nextCheckingFinesDate: '',
    hasMore: false,
    location: {},
    uploadVehiclesFile: false,
    isLoading: false,
    getFines: _.noop,
    openFineDetailsDialog: _.noop,
    selectFine: _.noop,
    deselectFine: _.noop,
    selectAllFines: _.noop,
    deselectAllFines: _.noop,
    filters: [],
    form: {},
    prevParams: {},
    hasAppliedFilter: false,
    clearAllFilters: _.noop,
    organizationFilter: '',
}

const mapStateToProps = () => createStructuredSelector({
    fines: selectors.fines.getFines,
    quantity: selectors.fines.getFinesQuantity,
    totalAmount: selectors.fines.getFinesTotalAmount,
    lastCheckingFinesDate: selectors.global.getLastCheckingFinesDate,
    nextCheckingFinesDate: selectors.global.getNextCheckingFinesDate,
    hasMore: selectors.fines.getHasMore,
    uploadVehiclesFile: selectors.global.getIsUploadVehiclesFile,
    isLoading: selectors.fines.getIsLoading,
    filters: selectors.fines.getFilters,
    form: selectors.filters.getForm,
    prevParams: selectors.fines.getPrevParams,
    hasAppliedFilter: selectors.filters.getHasAppliedFilter,
    organizationFilter: selectors.fines.getOrganizationFilter,
})

const mapDispatchToProps = (dispatch) => ({
    getFines: (param) => dispatch(actions.getFines(param)),
    openFineDetailsDialog: (uin) => dispatch(actions.openFineDetailsDialog(uin)),
    selectFine: (uin) => dispatch(actions.global.selectFine(uin)),
    deselectFine: (uin) => dispatch(actions.global.deselectFine(uin)),
    selectAllFines: () => dispatch(actions.global.selectAllFines()),
    deselectAllFines: () => dispatch(actions.global.deselectAllFines()),
    clearAllFilters: () => dispatch(actions.clearAllFilters()),
})

export const Fines = connect(mapStateToProps, mapDispatchToProps)(FinesPure)
