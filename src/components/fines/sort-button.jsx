import React from 'react'
import 'url-search-params-polyfill'
import { Link } from 'react-router-dom'
import classnames from 'classnames'
import PropTypes from 'prop-types'

import { SvgIcon } from '../general'
import { DEFAULT_SORT } from '../../__data__/actions/get-fines'

import style from './sort-button.css'

export const getSort = (location) => {
    const urlParams = new URLSearchParams(location.search)
    return urlParams.get('sort') || DEFAULT_SORT
}

export const SortButton = ({ text, type, currentSort }) => {
    const direction = currentSort.includes('asc') ? 'desc' : 'asc'
    const isSelected = currentSort.includes(type)

    return (
        <Link
            to={`?sort=${type}_${direction}`}
            className={classnames(style.root, style[direction], {
                [style.selected]: isSelected
            })}
            title={text}
        >
            {text}
            {
                isSelected &&
                <div className={style.icon}>
                    <SvgIcon
                        icon="sortArrow"
                        width="6px"
                        height="6px"
                    />
                </div>
            }
        </Link>
    )
}

SortButton.propTypes = {
    text: PropTypes.string,
    type: PropTypes.string,
    currentSort: PropTypes.string,
}

SortButton.defaultProps = {
    text: '',
    type: '',
    currentSort: '',
}
