import React from 'react'
import PropTypes from 'prop-types'
import i18next from 'i18next'
import classnames from 'classnames'
import _ from 'lodash'

import {
    getCurrentPage,
    Paging,
    formatMoney,
    Tooltip,
    SvgIcon,
    formatSts,
    formatDate,
    insertText,
    getStatus,
    getStatusIcon,
    getStatusMainText,
    getStatusTooltip,
} from '../general'

import style from './fines.css'
import { getSort, SortButton } from './sort-button'

export const Table = (
    {
        handleAllFines,
        fines,
        selectFine,
        deselectFine,
        hasMore,
        location,
        openFineDetailsDialog,
    }) =>
    (
        <div className={style.root}>
            <table cellPadding="0" cellSpacing="0">
                <thead>
                    <tr>
                        <th className={classnames(style.titleCell, style.titleCellCheckbox)}>
                            <input type="checkbox" onChange={handleAllFines} />
                        </th>
                        <th className={classnames(style.titleCell, style.titleCellStatus)}>
                            {i18next.t('fines.table.col.status')}
                        </th>
                        <th className={classnames(style.titleCell, style.titleCellUin)}>
                            {i18next.t('fines.table.col.uin')}
                        </th>
                        <th className={classnames(style.titleCell, style.titleCellViolation)}>
                            {i18next.t('fines.table.col.violation')}
                        </th>
                        <th className={classnames(style.titleCell, style.titleCellDateTime)}>
                            <SortButton
                                text={i18next.t('fines.table.col.datetime')}
                                type="actDate"
                                currentSort={getSort(location)}
                            />
                        </th>
                        <th className={classnames(style.titleCell, style.titleCellRegSign)}>
                            {i18next.t('fines.table.col.reg.sign')}
                        </th>
                        <th className={classnames(style.titleCell, style.titleCellAmount)}>
                            {i18next.t('fines.table.col.amount')}
                        </th>
                        <th className={classnames(style.titleCell, style.titleCellDetails)} />
                    </tr>
                </thead>
                <tbody>
                    {
                        fines.map((item) => {
                            function detailsButtonClick () {
                                openFineDetailsDialog(item.uin)
                            }

                            function handleSelectFine (event) {
                                if (event.target.checked) {
                                    selectFine(item.uin)
                                } else {
                                    deselectFine(item.uin)
                                }
                            }

                            const statusRes = getStatus(item.discountDate, item.actDate)

                            return (
                                <tr
                                    key={item.uin}
                                    className={style.row}
                                >
                                    <td className={classnames(style.cell, style.cellCheckbox)}>
                                        <input
                                            type="checkbox"
                                            checked={item.checked}
                                            onClick={handleSelectFine}
                                        />
                                    </td>
                                    <td className={classnames(style.cell, style.cellStatus)}>
                                        <Tooltip title={getStatusTooltip(statusRes, item.discountDate, item.actDate)}>
                                            <div className={style.statusWrapper}>
                                                <div className={style.statusIcon}>
                                                    <SvgIcon
                                                        icon={getStatusIcon(statusRes.status)}
                                                    />
                                                </div>
                                                {getStatusMainText(statusRes)}
                                            </div>
                                        </Tooltip>
                                    </td>
                                    <td className={classnames(style.cell, style.cellUin)}>
                                        {insertText(item.uin)}
                                    </td>
                                    <td className={classnames(style.cell, style.cellViolation)}>
                                        <Tooltip title={item.violationText || i18next.t('fines.violation.text.tooltip.default')}>
                                            <div className={style.violationTypeIcon}>
                                                <SvgIcon
                                                    icon={`violation_${item.violationType}`}
                                                />
                                            </div>
                                        </Tooltip>
                                        <Tooltip title={item.violationAddress} className={style.cellViolationAddress}>
                                            {insertText(item.violationAddress)}
                                        </Tooltip>
                                    </td>
                                    <td className={classnames(style.cell, style.cellDateTime)}>
                                        <Tooltip title={i18next.t('fines.table.col.tooltip.act.data', { DATE: formatDate(item.actDate) })}>
                                            {insertText(formatDate(item.violationDate))}
                                        </Tooltip>
                                    </td>
                                    <td className={classnames(style.cell, style.cellRegSign)}>
                                        <Tooltip title={item.regSign === null ? i18next.t('fines.reg.sing.tooltip.not.found.sts') : i18next.t('fines.table.col.tooltip.sts', { STS: formatSts(item.sts) })}>
                                            {item.regSign === null ? <span className={style.regSignIcon}><SvgIcon icon="unknownAuto" /></span> : insertText(item.regSign)}
                                        </Tooltip>
                                    </td>
                                    <td className={classnames(style.cell, style.cellAmount)}>
                                        {formatMoney(item.amount)}
                                    </td>
                                    <td className={classnames(style.cell, style.cellDetails)}>
                                        <button className={style.detailsButton} onClick={detailsButtonClick}>
                                            <div className={style.detailsButtonText}>
                                                {i18next.t('fines.table.col.details.button')}
                                            </div>
                                        </button>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
            {
                !(getCurrentPage(location) === 1 && !hasMore) &&
                <Paging
                    currentPage={getCurrentPage(location)}
                    hasMore={hasMore}
                    sort={getSort(location)}
                />
            }
        </div>
    )

Table.propTypes = {
    fines: PropTypes.array,
    hasMore: PropTypes.bool,
    location: PropTypes.object,
    handleAllFines: PropTypes.func,
    selectFine: PropTypes.func,
    deselectFine: PropTypes.func,
    openFineDetailsDialog: PropTypes.func,
}

Table.defaultProps = {
    fines: [],
    hasMore: false,
    location: {},
    handleAllFines: _.noop,
    selectFine: _.noop,
    deselectFine: _.noop,
    openFineDetailsDialog: _.noop,
}
