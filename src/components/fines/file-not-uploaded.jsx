import React from 'react'
import i18next from 'i18next'
import { Link } from 'react-router-dom'

import styles from './file-not-uploaded.css'

export const FileNotUploaded = () =>
    (
        <div className={styles.root}>
            <div className={styles.content}>
                <div className={styles.title}>
                    {i18next.t('fines.file.not.uploaded.title')}
                </div>
                <Link
                    to="/vehicles"
                    className={styles.button}
                >
                    {i18next.t('fines.file.not.uploaded.button')}
                </Link>
            </div>
        </div>
    )

