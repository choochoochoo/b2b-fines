import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'

import { SvgIcon } from '../general/svgicon'

import style from './accordion.css'

export class Accordion extends React.PureComponent {

    constructor (props) {
        super(props)

        this.state = {
            collapsed: props.collapsed,
        }
    }

    handleClick = () => {
        const {
            collapsed,
        } = this.state

        this.setState({ collapsed: !collapsed })
    }

    render () {
        const {
            children,
            title,
        } = this.props

        const {
            collapsed,
        } = this.state


        return (
            <div
                className={classnames({
                    [style.collapsed]: collapsed
                })}
            >
                <div>
                    <button
                        onClick={this.handleClick}
                        className={style.button}
                        title={title}
                        type="button"
                    >
                        <div className={style.buttonContent}>
                            <div className={style.buttonIcon}>
                                <SvgIcon
                                    icon={collapsed ? 'faqPlus' : 'faqMinus'}
                                    width="16"
                                    height="16"
                                />
                            </div>
                            <div className={style.buttonTitle}>
                                {title}
                            </div>
                        </div>
                    </button>
                    <div className={style.body}>
                        {children}
                    </div>
                </div>
            </div>
        )
    }
}

Accordion.propTypes = {
    collapsed: PropTypes.bool,
    children: PropTypes.node,
    title: PropTypes.string,
}

Accordion.defaultProps = {
    collapsed: false,
    children: null,
    title: '',
}
