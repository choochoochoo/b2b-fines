import React from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'

import { Accordion } from './accordion'

export const getId = (idx) => idx

export const Questions = ({ children }) => (
    <div>
        {children.map((item, idx) => {
            const title = _.get(item, 'props.children[0].props.children[0]')

            if (title) {
                return (
                    title && <Accordion
                        collapsed
                        title={title}
                        key={getId(idx)}
                    >
                        {item.props.children}
                    </Accordion>
                )
            }

            return <div key={getId(idx)} />
        }
        )}
    </div>
)

Questions.propTypes = {
    children: PropTypes.node,
}

Questions.defaultProps = {
    children: null,
}
