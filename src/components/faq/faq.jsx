import React from 'react'
import Markdown from 'markdown-to-jsx'

import { Template, Navbar } from '../general'

import style from './faq.css'
import { Questions } from './questions'
import md from './faq.md'

export const Faq = () =>
    (
        <Template>
            <Navbar />
            <div className={style.root}>
                <Markdown
                    options={{
                        overrides: {
                            ul: {
                                component: Questions,
                            },
                        },
                    }}
                >
                    {md}
                </Markdown>

            </div>
        </Template>
    )
