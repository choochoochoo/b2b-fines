import React from 'react'
import { reduxForm } from 'redux-form'
import i18next from 'i18next'
import PropTypes from 'prop-types'
import _ from 'lodash'

import styles from './login.css'
import { InputField } from './input-field'

const validate = (values) => {
    const errors = {}

    if (!values.login) {
        errors.login = i18next.t('input.field.required')
    }

    if (!values.password) {
        errors.password = i18next.t('input.field.required')
    }

    return errors
}


const LoginFormPure = (props) => {
    const { handleSubmit, valid, loginError } = props

    return (
        <form onSubmit={handleSubmit} className={styles.formWrapper}>
            <InputField
                name="login"
                type="text"
                label={i18next.t('login.label.login')}
                placeholder={i18next.t('login.placeholder.login')}
            />

            <div className={styles.emailWrapper}>
                <InputField
                    name="password"
                    type="password"
                    label={i18next.t('login.label.password')}
                    placeholder={i18next.t('login.placeholder.password')}
                />
            </div>

            <div className={styles.buttonWrapper}>
                {
                    loginError &&
                    <div className={styles.error}>
                        {loginError}
                    </div>
                }
                <button
                    title={i18next.t('login_button')}
                    className={styles.button}
                    disabled={!valid}
                >
                    <div className={styles.buttonText}>
                        {i18next.t('login.button.enter')}
                    </div>
                </button>
            </div>
        </form>
    )
}

LoginFormPure.propTypes = {
    valid: PropTypes.bool,
    loginError: PropTypes.string,
    handleSubmit: PropTypes.func,
}

LoginFormPure.defaultProps = {
    valid: false,
    loginError: '',
    handleSubmit: _.noop,
}

export const LoginForm = reduxForm({
    form: 'LoginForm',
    validate
})(LoginFormPure)

