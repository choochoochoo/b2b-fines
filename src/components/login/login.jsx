import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import _ from 'lodash'
import i18next from 'i18next'
import { createStructuredSelector } from 'reselect'

import * as actions from '../../__data__/actions'
import config from '../../../config.json'
import * as selectors from '../../__data__/selectors'
import { Loader } from '../general'
import { HISTORY } from '../../'

import { LoginForm } from './login-form'
import styles from './login.css'


export const LoginPure = ({ history, login, error, getProfile }) => {

    useEffect(() => {
        getProfile({
            onSuccessHandler: () => {
                HISTORY.push('/fines')
            },
        })
    }, [])

    function handleSubmit (values) {
        login({
            login: _.trim(values.login.toLowerCase()),
            password: _.trim(values.password),
            onSuccessHandler: () => {
                history.push('/vehicles')
                getProfile({})
            },
        })
    }

    return (
        <div className={styles.root}>
            <div className={styles.content}>
                <div className={styles.title}>
                    {i18next.t('login.title')}
                </div>
                <LoginForm
                    onSubmit={handleSubmit}
                    loginError={error}
                />
                <div className={styles.contacts}>
                    <div className={styles.contactsLabel}>
                        {i18next.t('login.contacts')}
                    </div>
                    <div className={styles.email}>
                        <a href={`mailto:${config.contactEmail}`} className={styles.linkEmail}>
                            {config.contactEmail}
                        </a>
                    </div>
                </div>
            </div>
            <Loader />
        </div>
    )
}

LoginPure.propTypes = {
    history: PropTypes.object,
    error: PropTypes.string,
    login: PropTypes.func,
    getProfile: PropTypes.func,
}

LoginPure.defaultProps = {
    history: {},
    error: '',
    login: _.noop,
    getProfile: _.noop,
}

const mapDispatchToProps = (dispatch) => ({
    login: (param) => dispatch(actions.login(param)),
    getProfile: (param) => dispatch(actions.getProfile(param)),
})

const mapStateToProps = () => createStructuredSelector({
    error: selectors.login.getError,
})

export const Login = connect(mapStateToProps, mapDispatchToProps)(LoginPure)
