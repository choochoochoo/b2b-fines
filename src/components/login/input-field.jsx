import React from 'react'
import PropTypes from 'prop-types'
import { Field } from 'redux-form'
import i18next from 'i18next'

import styles from './input-field.css'

const renderField = (
    {
        input,
        placeholder,
        type,
        meta: { touched, error },
        label,
        required,
    }) => {

    const styleErrorText = touched && error ? {
        color: '#f57123'
    } : {}

    const labelText = touched && error ? `${label} — ${error}` : label

    let req = null

    if (required) {
        req = <span className={styles.required}>{i18next.t('dialog.error.icon.close')}</span>
    }

    return (
        <div>
            <label>
                <div className={styles.label} style={styleErrorText}>
                    {labelText}
                    {req}
                </div>
                <div>
                    <input
                        {...input}
                        placeholder={placeholder}
                        type={type}
                        className={styles.input}
                    />
                </div>
            </label>
        </div>
    )
}

renderField.propTypes = {
    input: PropTypes.object,
    meta: PropTypes.object,
    type: PropTypes.string,
    placeholder: PropTypes.string,
    label: PropTypes.string,
    required: PropTypes.bool,
}

renderField.defaultProps = {
    input: {},
    meta: {},
    type: '',
    placeholder: '',
    label: '',
    required: false,
}

export const InputField = ({ name, type, placeholder, label, required }) =>
    (
        <Field
            name={name}
            type={type}
            placeholder={placeholder}
            component={renderField}
            label={label}
            required={required}
        />
    )

InputField.propTypes = {
    name: PropTypes.string,
    type: PropTypes.string,
    placeholder: PropTypes.string,
    label: PropTypes.string,
    required: PropTypes.bool,
}

InputField.defaultProps = {
    name: '',
    type: '',
    placeholder: '',
    label: '',
    required: false,
}
