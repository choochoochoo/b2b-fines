import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import _ from 'lodash'

import * as actions from '../../__data__/actions'
import * as selectors from '../../__data__/selectors'
import { Template, Navbar, getCurrentPage, DialogError, Loader, FileLoader, } from '../general'
import { CreateGroupDialog, MoveVehiclesDialog, MoveCreateVehiclesDialog } from '../groups'

import { Table } from './table'
import { Script } from './script'
import { Header } from './header'
import { VehiclesSelector } from './vehicles-selector'
import styles from './vehicles.css'

export const hasOwnGroups = (groups) => groups.length > 1

export const VehiclesPure = (
    {
        getVehicles,
        getProfile,
        vehicles,
        vehiclesQuantity,
        location,
        hasMore,
        uploadVehiclesFile,
        uploadVehicles,
        isLoading,
        groups,
        openGroupDialog,
        selectAllVehicles,
        deselectAllVehicles,
        selectVehicle,
        deselectVehicle,
        quantity,
        openMoveVehiclesDialog,
        openMoveCreateVehiclesDialog,
        selectedVehicles,
    }) => {
    useEffect(() => {
        getVehicles({
            page: getCurrentPage(location)
        })
    }, [location])

    function handleAllVehicles (event) {
        if (event.target.checked) {
            selectAllVehicles()
        } else {
            deselectAllVehicles()
        }
    }

    return (
        <Template>
            <Navbar />
            {
                !isLoading && uploadVehiclesFile && (
                    <React.Fragment>
                        <Header
                            groups={groups}
                            vehiclesQuantity={vehiclesQuantity}
                            openGroupDialog={openGroupDialog}
                        />
                        <div className={styles.table}>
                            <Table
                                hasMore={hasMore}
                                vehicles={vehicles}
                                location={location}
                                handleAllVehicles={handleAllVehicles}
                                selectVehicle={selectVehicle}
                                deselectVehicle={deselectVehicle}
                            />
                            <FileLoader />
                        </div>
                    </React.Fragment>
                )
            }
            {
                !isLoading && !uploadVehiclesFile &&
                <Script uploadVehicles={uploadVehicles} getProfile={getProfile} />
            }
            <DialogError />
            <Loader />
            <CreateGroupDialog />
            <MoveVehiclesDialog
                groups={groups}
                quantity={quantity}
                selectedVehicles={selectedVehicles}
            />
            <MoveCreateVehiclesDialog
                quantity={quantity}
                selectedVehicles={selectedVehicles}
            />
            <VehiclesSelector
                quantity={quantity}
                openDialog={hasOwnGroups(groups) ? openMoveVehiclesDialog : openMoveCreateVehiclesDialog}
            />
        </Template>
    )
}

VehiclesPure.propTypes = {
    location: PropTypes.object,
    vehicles: PropTypes.array,
    groups: PropTypes.array,
    vehiclesQuantity: PropTypes.number,
    hasMore: PropTypes.bool,
    uploadVehiclesFile: PropTypes.bool,
    isLoading: PropTypes.bool,
    uploadVehicles: PropTypes.func,
    getVehicles: PropTypes.func,
    getProfile: PropTypes.func,
    openGroupDialog: PropTypes.func,
    selectAllVehicles: PropTypes.func,
    deselectAllVehicles: PropTypes.func,
    selectVehicle: PropTypes.func,
    deselectVehicle: PropTypes.func,
    quantity: PropTypes.number,
    openMoveVehiclesDialog: PropTypes.func,
    openMoveCreateVehiclesDialog: PropTypes.func,
    selectedVehicles: PropTypes.array,
}

VehiclesPure.defaultProps = {
    location: {},
    vehicles: [],
    groups: [],
    vehiclesQuantity: 0,
    hasMore: false,
    uploadVehiclesFile: false,
    isLoading: false,
    uploadVehicles: _.noop,
    getVehicles: _.noop,
    getProfile: _.noop,
    openGroupDialog: _.noop,
    selectAllVehicles: _.noop,
    deselectAllVehicles: _.noop,
    selectVehicle: _.noop,
    deselectVehicle: _.noop,
    quantity: 0,
    openMoveVehiclesDialog: _.noop,
    openMoveCreateVehiclesDialog: _.noop,
    selectedVehicles: [],
}

const mapStateToProps = () => createStructuredSelector({
    vehicles: selectors.vehicles.getVehicles,
    groups: selectors.vehicles.getGroups,
    vehiclesQuantity: selectors.vehicles.getVehiclesQuantity,
    hasMore: selectors.vehicles.getHasMore,
    uploadVehiclesFile: selectors.global.getIsUploadVehiclesFile,
    isLoading: selectors.vehicles.getIsLoading,
    quantity: selectors.vehicles.getCheckedVehiclesQuantity,
    selectedVehicles: selectors.vehicles.getCheckedVehiclesArray,
})

const mapDispatchToProps = (dispatch) => ({
    getVehicles: (param) => dispatch(actions.vehicles.getVehicles(param)),
    uploadVehicles: (param) => dispatch(actions.uploadVehicles(param)),
    getProfile: (param) => dispatch(actions.getProfile(param)),
    openGroupDialog: () => dispatch(actions.group.openCreateGroupDialog()),
    selectAllVehicles: () => dispatch(actions.vehicles.selectAllVehicles()),
    deselectAllVehicles: () => dispatch(actions.vehicles.deselectAllVehicles()),
    selectVehicle: (id) => dispatch(actions.vehicles.selectVehicle(id)),
    deselectVehicle: (id) => dispatch(actions.vehicles.deselectVehicle(id)),
    openMoveVehiclesDialog: () => dispatch(actions.group.openMoveVehiclesDialog()),
    openMoveCreateVehiclesDialog: () => dispatch(actions.group.openMoveCreateVehiclesDialog()),
})

export const Vehicles = connect(mapStateToProps, mapDispatchToProps)(VehiclesPure)
