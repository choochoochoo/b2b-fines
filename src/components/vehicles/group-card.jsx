import React from 'react'
import i18next from 'i18next'
import PropTypes from 'prop-types'

import { LinkButton } from '../general'

import style from './group-card.css'

export const GroupCard = ({ id, title, description, href }) => (
    <div className={style.root} key={id}>
        <div className={style.title}>
            {title}
        </div>
        <div className={style.description}>
            {description}
        </div>
        <hr className={style.line} />

        <div className={style.buttonPanel}>
            <LinkButton
                title={i18next.t('vehicles.header.group.card.button')}
                href={href}
            />
        </div>
    </div>
)

GroupCard.propTypes = {
    id: PropTypes.number,
    title: PropTypes.string,
    description: PropTypes.string,
    href: PropTypes.string,
}

GroupCard.defaultProps = {
    id: 0,
    title: '',
    description: '',
    href: '',
}
