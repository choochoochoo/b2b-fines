import React from 'react'
import i18next from 'i18next'
import PropTypes from 'prop-types'
import _ from 'lodash'

import { Button } from '../general'

import style from './header.css'
import { GroupCard } from './group-card'

export const Header = ({ groups, vehiclesQuantity, openGroupDialog }) => (
    <div className={style.root}>
        <div className={style.header}>
            <div className={style.leftCol}>
                <div className={style.title}>{i18next.t('vehicles.title')}</div>
                <div className={style.description}>
                    {
                        i18next.t(
                            'vehicles.description.document',
                            { count: vehiclesQuantity, QUANTITY: vehiclesQuantity }
                        )
                    }
                </div>
            </div>
            <div>
                <Button
                    title={i18next.t('vehicles.header.button.add.group')}
                    colorScheme="green"
                    onClick={openGroupDialog}
                />
            </div>
        </div>
        <div className={style.cardPanel}>
            {
                groups.map((item) => {

                    if (item.id === 0 && item.quantity === 0) {
                        return <div />
                    }

                    return (
                        <GroupCard
                            key={item.id}
                            title={item.name}
                            description={i18next.t('vehicles.header.group.card.description', {
                                count: item.quantity,
                                QUANTITY: item.quantity
                            })}
                            id={item.id}
                            href={`/groups/${item.id}`}
                        />
                    )
                })
            }
        </div>
    </div>
)

Header.propTypes = {
    groups: PropTypes.array,
    vehiclesQuantity: PropTypes.bool,
    openGroupDialog: PropTypes.func,
}

Header.defaultProps = {
    groups: [],
    vehiclesQuantity: 0,
    openGroupDialog: _.noop,
}
