import React from 'react'
import PropTypes from 'prop-types'
import i18next from 'i18next'
import _ from 'lodash'

import { RowSelector, Button } from '../general'

import style from './vehicles-selector.css'

export const VehiclesSelector = ({ quantity, openDialog, }) =>
    (
        <RowSelector>
            <div className={style.title}>
                {i18next.t('vehicles.row.selector.title', { QUANTITY: quantity })}
            </div>
            <div className={style.rowCol}>
                <Button
                    title={i18next.t('vehicles.row.selector.move.button.title')}
                    onClick={openDialog}
                    colorScheme="green"
                    className={style.buttonMove}
                    disabled={quantity === 0}
                />
            </div>
        </RowSelector>
    )

VehiclesSelector.propTypes = {
    quantity: PropTypes.number,
    openDialog: PropTypes.func,
}

VehiclesSelector.defaultProps = {
    quantity: 0,
    openDialog: _.noop,
}
