import React from 'react'
import PropTypes from 'prop-types'

import style from './stepper.css'

export const Stepper = ({ children }) => (
    <div>
        {children.map((item, index) => (
            <Step
                title={item.props.title}
                number={index}
                key={item.props.title}
                length={children.length}
            >
                {item.props.children}
            </Step>)
        )}
    </div>
)

Stepper.propTypes = {
    children: PropTypes.node,
}

Stepper.defaultProps = {
    children: null,
}

export const Step = ({ number, title, length, children }) => (
    <div className={style.stepRoot}>
        <div className={style.stepNumber}>
            <div className={style.stepNumberIcon}>
                <div className={style.stepNumberText}>
                    {number + 1}
                </div>
            </div>
        </div>
        <div>
            <div className={style.stepTitle}>
                {title}
            </div>
            <div>
                {children}
            </div>
        </div>
        { length !== number + 1 && <div className={style.stepLine} /> }
    </div>
)


Step.propTypes = {
    children: PropTypes.node,
    number: PropTypes.number,
    length: PropTypes.number,
    title: PropTypes.string,
}

Step.defaultProps = {
    children: null,
    number: 0,
    length: 0,
    title: '',
}
