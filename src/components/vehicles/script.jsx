import React from 'react'
import i18next from 'i18next'
import PropTypes from 'prop-types'
import _ from 'lodash'

import { FileLoaderBase, SvgIcon } from '../general'
import { HISTORY } from '../../'

import style from './script.css'
import { Stepper, Step } from './stepper'

export const Warning = () => (
    <div className={style.warning}>
        <div className={style.warningIcon}>
            <SvgIcon icon="warning" />
        </div>
        <div className={style.warningText}>
            {i18next.t('vehicles.script.block.warning.file')}
        </div>
    </div>
)

export const Script = ({ uploadVehicles, getProfile, }) => {
    const url = '/api/profiles/files/templates'

    function handleUploadVehicles (files) {
        uploadVehicles({
            file: files.file,
            onSuccessHandler: () => {
                getProfile({})
                HISTORY.push('/fines')
            },
        })
    }

    return (
        <div className={style.root}>
            <div className={style.title}>{i18next.t('Как проверять штрафы')}</div>

            <div className={style.stepperPanel}>
                <div>
                    <Stepper>
                        <Step title={i18next.t('vehicles.script.block.step1')}>
                            <a
                                className={style.downloadButton}
                                href={url}
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                <div className={style.downloadButtonText}>
                                    {i18next.t('vehicles.script.button.download.template')}
                                </div>
                            </a>
                        </Step>
                        <Step title={i18next.t('vehicles.script.block.step2')} />
                        <Step title={i18next.t('vehicles.script.block.step3')} />
                        <Step title={i18next.t('vehicles.script.block.step4')} />
                        <Step title={i18next.t('vehicles.script.block.step5')}>
                            <div>
                                <FileLoaderBase uploadFile={handleUploadVehicles}>
                                    <button
                                        className={style.uploadButton}
                                        title={i18next.t('vehicles.script.button.upload.template')}
                                    >
                                        <div className={style.uploadButtonText}>
                                            {i18next.t('vehicles.script.button.upload.template')}
                                        </div>
                                    </button>
                                </FileLoaderBase>
                                <Warning />
                            </div>
                        </Step>
                    </Stepper>
                </div>
            </div>
        </div>
    )
}

Script.propTypes = {
    uploadVehicles: PropTypes.func,
    getProfile: PropTypes.func,
}

Script.defaultProps = {
    uploadVehicles: _.noop,
    getProfile: _.noop,
}
