import React from 'react'
import PropTypes from 'prop-types'
import i18next from 'i18next'
import classnames from 'classnames'
import _ from 'lodash'

import { getCurrentPage, Paging, formatSts } from '../general'

import style from './table.css'

export const Table = (
    {
        hasMore,
        vehicles,
        location,
        handleAllVehicles,
        selectVehicle,
        deselectVehicle,
    }) =>
    (
        <div className={style.root}>
            <table cellPadding="0" cellSpacing="0">
                <thead>
                    <tr>
                        <th className={classnames(style.titleCell, style.titleCellCheckbox)}>
                            <input type="checkbox" onChange={handleAllVehicles} />
                        </th>
                        <th className={classnames(style.titleCell, style.titleCellSts)}>
                            {i18next.t('vehicles.table.col.sts')}
                        </th>
                        <th className={classnames(style.titleCell, style.titleCellRegSign)}>
                            {i18next.t('vehicles.table.col.reg.sign')}
                        </th>
                        <th className={classnames(style.titleCell, style.titleCellComment)}>
                            {i18next.t('vehicles.table.col.comment')}
                        </th>
                        <th className={classnames(style.titleCell, style.titleCellGroup)}>
                            {i18next.t('vehicles.table.col.group')}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {
                        vehicles.map((item) => {

                            function handleSelectVehicle (event) {
                                if (event.target.checked) {
                                    selectVehicle(item.id)
                                } else {
                                    deselectVehicle(item.id)
                                }
                            }

                            return (
                                <tr key={item.id} className={style.row}>
                                    <td className={classnames(style.cell, style.cellCheckbox)}>
                                        <input
                                            type="checkbox"
                                            checked={item.checked}
                                            onClick={handleSelectVehicle}
                                        />
                                    </td>
                                    <td className={classnames(style.cell, style.cellSts)}>
                                        {formatSts(item.sts)}
                                    </td>
                                    <td className={classnames(style.cell, style.cellRegSign)}>
                                        {item.regSign}
                                    </td>
                                    <td className={classnames(style.cell, style.cellComment)}>
                                        {item.comment}
                                    </td>
                                    <td className={classnames(style.cell, style.cellGroup)}>
                                        {_.get(item, 'groupsIdentifiers[0]')}
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
            <div className={style.paging}>
                {
                    !(getCurrentPage(location) === 1 && !hasMore) &&
                    <Paging
                        currentPage={getCurrentPage(location)}
                        hasMore={hasMore}
                    />
                }
            </div>
        </div>
    )

Table.propTypes = {
    location: PropTypes.object,
    vehicles: PropTypes.array,
    hasMore: PropTypes.bool,
    handleAllVehicles: PropTypes.func,
    selectVehicle: PropTypes.func,
    deselectVehicle: PropTypes.func,
}

Table.defaultProps = {
    location: {},
    vehicles: [],
    hasMore: false,
    handleAllVehicles: _.noop,
    selectVehicle: _.noop,
    deselectVehicle: _.noop,
}
