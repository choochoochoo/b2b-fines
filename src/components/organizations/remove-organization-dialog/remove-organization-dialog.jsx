import React from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'
import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux'
import i18next from 'i18next'
import { withRouter } from 'react-router'

import { selectors, actions } from '../../../__data__'
import { Dialog } from '../../general'
import { HISTORY } from '../../../'

import { RemoveOrganizationForm } from './remove-organization-form'
import style from './remove-organization-dialog.css'

export const RemoveOrganizationDialogPure = (
    {
        removeOrganization,
        opened,
        closeRemoveOrganizationDialog,
        organizationId,
        organizationName,
        organizations,
    }) => {

    function handleSubmit () {
        removeOrganization({
            organizationId,
            onSuccessHandler: () => {
                const organizationsClone = _.cloneDeep(organizations)
                _.remove(organizationsClone, (item) => item.id === organizationId)
                const firstOrganization = _.get(organizationsClone, '[0]', {})

                if (firstOrganization.id) {
                    HISTORY.push(`/organizations/${firstOrganization.id}`)
                } else {
                    HISTORY.push('/organizations/new')
                }
            }
        })
    }

    return (
        <Dialog opened={opened} closeClick={closeRemoveOrganizationDialog} width="544px">
            <div className={style.title}>
                {i18next.t('organizations.organization.dialog.remove.title')}
                <br />
                {`${organizationName} ?`}
            </div>
            <div className={style.message}>
                {i18next.t('organizations.organization.dialog.remove.description')}
            </div>
            <RemoveOrganizationForm
                onSubmit={handleSubmit}
                cancelClick={closeRemoveOrganizationDialog}
            />
        </Dialog>
    )
}

RemoveOrganizationDialogPure.propTypes = {
    removeOrganization: PropTypes.func,
    opened: PropTypes.bool,
    closeRemoveOrganizationDialog: PropTypes.func,
    organizationId: PropTypes.string,
    organizationName: PropTypes.string,
    organizations: PropTypes.array,
}

RemoveOrganizationDialogPure.defaultProps = {
    removeOrganization: _.noop,
    opened: false,
    closeRemoveOrganizationDialog: _.noop,
    organizationId: '',
    organizationName: '',
    organizations: [],
}

const mapStateToProps = () => createStructuredSelector({
    opened: selectors.organizations.getIsOpenRemoveOrganizationDialog,
    organizationId: selectors.organizations.getRemoveOrganizationDialogId,
    organizationName: selectors.organizations.getRemoveOrganizationDialogName,
})

const mapDispatchToProps = (dispatch) => ({
    removeOrganization: (param) => dispatch(actions.organization.removeOrganization(param)),
    closeRemoveOrganizationDialog: () => dispatch(actions.organization.closeRemoveOrganizationDialog()),
})

export const RemoveOrganizationDialog = connect(mapStateToProps, mapDispatchToProps)(withRouter(RemoveOrganizationDialogPure))
