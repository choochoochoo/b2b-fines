import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import _ from 'lodash'
import { reset } from 'redux-form'

import { actions, selectors } from '../../__data__'
import { Template, Navbar, Loader, DialogError } from '../general'
import { HISTORY } from '../../'
import { CreateBranchDialog, EditBranchDialog, RemoveBranchDialog } from '../branches'

import styles from './organizations.css'
import { OrganizationsLeftCol } from './organizations-left-col'
import { OrganizationCreate } from './organization-create'
import { OrganizationDetails } from './organization-details'
import { newOrganizationSelected } from './organizations-left-col/new-organization-button'
import { EditOrganizationDialog } from './edit-organization-dialog'
import { RemoveOrganizationDialog } from './remove-organization-dialog'

export function redirectToPage (match, firstOrganization) {
    const organizationId = _.get(match, 'params.id')

    if (organizationId) {
        HISTORY.push(`/organizations/${organizationId}`)
    } else if (firstOrganization && firstOrganization.id) {
        HISTORY.push(`/organizations/${firstOrganization.id}`)
    } else {
        HISTORY.push('/organizations/new')
    }
}

export const OrganizationsPure = (
    {
        getOrganizations,
        organizations,
        createOrganization,
        resetForm,
        match,
        firstOrganization,
    }) => {
    useEffect(() => {
        if (organizations.length > 0) {
            redirectToPage(match, firstOrganization)
        } else {
            getOrganizations({})
        }
    }, [])

    return (
        <Template>
            <Navbar />
            <div className={styles.root}>
                <div className={styles.leftCol}>
                    <OrganizationsLeftCol list={organizations} />
                </div>
                <div className={styles.rightCol}>
                    {
                        newOrganizationSelected() &&
                        <OrganizationCreate
                            createOrganization={createOrganization}
                            resetForm={resetForm}
                        />
                    }
                    {
                        !newOrganizationSelected() &&
                        <OrganizationDetails
                            organizations={organizations}
                        />
                    }
                </div>
            </div>
            <DialogError />
            <Loader />
            <EditOrganizationDialog organizations={organizations} />
            <RemoveOrganizationDialog organizations={organizations} />
            <CreateBranchDialog />
            <EditBranchDialog organizations={organizations} />
            <RemoveBranchDialog />
        </Template>
    )
}

OrganizationsPure.propTypes = {
    organizations: PropTypes.array,
    getOrganizations: PropTypes.func,
    createOrganization: PropTypes.func,
    resetForm: PropTypes.func,
    match: PropTypes.object,
    firstOrganization: PropTypes.object,
}

OrganizationsPure.defaultProps = {
    organizations: [],
    getOrganizations: _.noop,
    createOrganization: _.noop,
    resetForm: _.noop,
    match: {},
    firstOrganization: {},
}

const mapStateToProps = () => createStructuredSelector({
    organizations: selectors.organizations.getOrganizations,
    firstOrganization: selectors.organizations.getFirstOrganization,
})

const mapDispatchToProps = (dispatch) => ({
    getOrganizations: (param) => dispatch(actions.organization.getOrganizations(param)),
    createOrganization: (param) => dispatch(actions.organization.createOrganization(param)),
    resetForm: (formName) => dispatch(reset(formName)),
})

export const Organizations = connect(mapStateToProps, mapDispatchToProps)(OrganizationsPure)
