import React from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'
import i18next from 'i18next'

import { Button } from '../../general/button'

import styles from './branch-item.css'
import { OrganizationDetailsAttr } from './organization-details-attr'

export const BranchItem = (
    {
        id,
        name,
        address,
        kpp,
        openEditBranchDialog,
    }) => {

    function opedEdit () {
        openEditBranchDialog(id)
    }

    return (
        <div className={styles.root}>
            <div className={styles.header}>
                <div className={styles.leftCol}>
                    <div className={styles.title}>
                        {name}
                    </div>
                    <div className={styles.description}>
                        {address}
                    </div>
                </div>
                <div className={styles.rightCol}>
                    <Button
                        colorScheme="outline-green"
                        onClick={opedEdit}
                        title={i18next.t('organizations.organization.details.branches.item.button.edit')}
                    />
                </div>
            </div>

            <div>
                <OrganizationDetailsAttr
                    label={i18next.t('organizations.organization.details.branches.item.kpp')}
                    value={kpp}
                    className={styles.attr}
                />
            </div>
        </div>
    )
}

BranchItem.propTypes = {
    id: PropTypes.number,
    name: PropTypes.string,
    address: PropTypes.string,
    kpp: PropTypes.string,
    openEditBranchDialog: PropTypes.func,
}

BranchItem.defaultProps = {
    id: 0,
    name: '',
    address: '',
    kpp: '',
    openEditBranchDialog: _.noop,
}

