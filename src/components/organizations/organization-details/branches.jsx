import React from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'
import i18next from 'i18next'

import { Button } from '../../general'

import styles from './branches.css'
import { BranchItem } from './branch-item'

export const Branches = (
    {
        list,
        openCreateBranchDialog,
        openEditBranchDialog,
    }) =>
    (
        <div className={styles.root}>
            {
                list.length > 0 &&
                <div className={styles.title}>
                    {i18next.t('organizations.organization.details.branches.title')}
                </div>
            }
            {
                list.map((item) => (
                    <BranchItem
                        key={item.id}
                        id={item.id}
                        name={item.name}
                        address={item.address}
                        kpp={item.kpp}
                        openEditBranchDialog={openEditBranchDialog}
                    />
                ))
            }
            {
                list.length < 10 &&
                <Button
                    onClick={openCreateBranchDialog}
                    colorScheme="big-second"
                    title={i18next.t('organizations.organization.details.branches.button.add.branch')}
                    className={styles.addBranchButton}
                />
            }
        </div>
    )

Branches.propTypes = {
    list: PropTypes.array,
    openCreateBranchDialog: PropTypes.func,
    openEditBranchDialog: PropTypes.func,
}

Branches.defaultProps = {
    list: [],
    openCreateBranchDialog: _.noop,
    openEditBranchDialog: _.noop,
}
