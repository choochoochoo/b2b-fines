import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import _ from 'lodash'
import i18next from 'i18next'
import { withRouter } from 'react-router'

import { actions, selectors } from '../../../__data__'
import { Button } from '../../general'
import { redirectToPage } from '../organizations'
import { HISTORY } from '../../../'

import styles from './organization-details.css'
import { OrganizationDetailsAttr } from './organization-details-attr'
import { Branches } from './branches'

export const getOrganization = (organizations, id) => organizations.find((item) => item.id.toString() === id.toString()) || {}
export const getFirstOrganizationPure = (response) => _.get(response, 'body.organizations[0]', null)

export const OrganizationDetailsPure = (
    {
        organizations,
        getOrganizations,
        match,
        openEditOrganizationDialog,
        openCreateBranchDialog,
        openEditBranchDialog,
        setOrganizationFilter,
    }) => {

    useEffect(() => {
        if (organizations.length === 0) {
            getOrganizations({
                onSuccessHandler: (response) => {
                    redirectToPage(match, getFirstOrganizationPure(response))
                }
            })
        }
    }, [])

    const organizationId = _.get(match, 'params.id', 0)
    const organization = getOrganization(organizations, organizationId)
    const branches = _.get(organization, 'branches', [])

    function openDialog () {
        openEditOrganizationDialog(organizationId)
    }

    function viewFines () {

        setOrganizationFilter({
            organizationId,
        })

        HISTORY.push('/fines')
    }

    return (
        <div>
            <div className={styles.root}>
                <div className={styles.header}>
                    <div>
                        <div className={styles.title}>
                            {organization.name}
                        </div>
                        <div className={styles.description}>
                            {organization.address}
                        </div>
                    </div>
                    <div>
                        <Button
                            colorScheme="outline-green"
                            onClick={openDialog}
                            title={i18next.t('organizations.organization.details.button.edit')}
                        />
                    </div>
                </div>

                <div className={styles.attrs}>
                    <OrganizationDetailsAttr
                        label={i18next.t('organizations.organization.details.inn')}
                        value={organization.inn}
                    />
                    <OrganizationDetailsAttr
                        label={i18next.t('organizations.organization.details.ogrn')}
                        value={organization.ogrn}
                    />
                    <OrganizationDetailsAttr
                        label={i18next.t('organizations.organization.details.kpp')}
                        value={organization.kpp}
                    />
                </div>

                <Button
                    onClick={viewFines}
                    colorScheme="big-green"
                    title={i18next.t('organizations.organization.details.button.fines.view')}
                />
            </div>
            <Branches
                list={branches}
                openCreateBranchDialog={openCreateBranchDialog}
                openEditBranchDialog={openEditBranchDialog}
            />
        </div>
    )
}

OrganizationDetailsPure.propTypes = {
    organizations: PropTypes.array,
    getOrganizations: PropTypes.func,
    match: PropTypes.object,
    openEditOrganizationDialog: PropTypes.func,
    openCreateBranchDialog: PropTypes.func,
    openEditBranchDialog: PropTypes.func,
    setOrganizationFilter: PropTypes.func,
}

OrganizationDetailsPure.defaultProps = {
    organizations: [],
    getOrganizations: _.noop,
    match: {},
    openEditOrganizationDialog: _.noop,
    openCreateBranchDialog: _.noop,
    openEditBranchDialog: _.noop,
    setOrganizationFilter: _.noop,
}

const mapStateToProps = () => createStructuredSelector({
    organizations: selectors.organizations.getOrganizations,
})

const mapDispatchToProps = (dispatch) => ({
    getOrganizations: (param) => dispatch(actions.organization.getOrganizations(param)),
    openEditOrganizationDialog: (id) => dispatch(actions.organization.openEditOrganizationDialog(id)),
    openCreateBranchDialog: () => dispatch(actions.branches.openCreateBranchDialog()),
    openEditBranchDialog: (id) => dispatch(actions.branches.openEditBranchDialog(id)),
    setOrganizationFilter: (param) => dispatch(actions.setOrganizationFilter(param)),
})

export const OrganizationDetails = connect(mapStateToProps, mapDispatchToProps)(withRouter(OrganizationDetailsPure))
