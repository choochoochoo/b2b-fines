import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'

import styles from './organization-details-attr.css'

export const OrganizationDetailsAttr = ({ label, value, className }) => (
    <div
        className={classnames(styles.root, className)}
    >
        <div className={styles.label}>{label}</div>
        <div className={styles.value}>{value}</div>
        <hr className={styles.line} />
    </div>
)

OrganizationDetailsAttr.propTypes = {
    label: PropTypes.string,
    value: PropTypes.string,
    className: PropTypes.string,
}

OrganizationDetailsAttr.defaultProps = {
    label: '',
    value: '',
    className: '',
}
