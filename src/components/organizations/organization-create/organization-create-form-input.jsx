import React from 'react'
import PropTypes from 'prop-types'
import { Field } from 'redux-form'
import classnames from 'classnames'

import styles from './organization-create-form-input.css'

const renderField = (
    {
        input,
        type,
        meta: { touched, error, active },
        label,
    }) =>
    (
        <label
            className={classnames(styles.label, {
                [styles.hasError]: touched && error,
            })}
        >
            <div className={styles.labelText}>
                {label}
            </div>
            <div>
                <input
                    {...input}
                    type={type}
                    className={classnames(styles.input, {
                        [styles.active]: active
                    })}
                />
            </div>
            {
                touched && error && <div className={styles.error}>
                    {error}
                </div>
            }
        </label>
    )

renderField.propTypes = {
    input: PropTypes.object,
    meta: PropTypes.object,
    type: PropTypes.string,
    label: PropTypes.string,
}

renderField.defaultProps = {
    input: {},
    meta: {},
    type: '',
    label: '',
}

export const OrganizationCreateFormInput = ({ name, type, label }) =>
    (
        <Field
            name={name}
            type={type}
            component={renderField}
            label={label}
        />
    )

OrganizationCreateFormInput.propTypes = {
    name: PropTypes.string,
    type: PropTypes.string,
    label: PropTypes.string,
}

OrganizationCreateFormInput.defaultProps = {
    name: '',
    type: '',
    label: '',
}
