import React from 'react'
import { reduxForm } from 'redux-form'
import i18next from 'i18next'
import PropTypes from 'prop-types'
import _ from 'lodash'

import { Button } from '../../general'

import styles from './organization-create-form.css'
import { OrganizationCreateFormInput } from './organization-create-form-input'

export const INN_REGEXP = /^[0-9\s]{10,12}$/
export const FORM_NAME = 'OrganizationCreateForm'

export const validate = (values) => {
    const errors = {}

    if (!values.organizationInn) {
        errors.organizationInn = i18next.t('input.field.required')
    } else if (!INN_REGEXP.test(values.organizationInn)) {
        errors.organizationInn = i18next.t('organizations.organization.create.input.error')
    }

    return errors
}

const OrganizationCreateFormPure = (props) => {
    const { handleSubmit, valid } = props

    return (
        <form onSubmit={handleSubmit} className={styles.root}>
            <OrganizationCreateFormInput
                name="organizationInn"
                required
                type="text"
                label={i18next.t('organizations.organization.create.input.label')}
            />

            <div>
                <Button
                    colorScheme="big-green"
                    title={i18next.t('organizations.organization.create.input.button')}
                    disabled={!valid}
                    type="submit"
                />
            </div>
        </form>
    )
}

OrganizationCreateFormPure.propTypes = {
    valid: PropTypes.bool,
    handleSubmit: PropTypes.func,
}

OrganizationCreateFormPure.defaultProps = {
    valid: false,
    handleSubmit: _.noop,
}

export const OrganizationCreateForm = reduxForm({
    form: FORM_NAME,
    validate
})(OrganizationCreateFormPure)

