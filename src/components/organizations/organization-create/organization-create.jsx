import React from 'react'
import i18next from 'i18next'
import _ from 'lodash'
import PropTypes from 'prop-types'

import { HISTORY } from '../../../'

import styles from './organization-create.css'
import { OrganizationCreateForm, FORM_NAME } from './organization-create-form'

export const OrganizationCreate = ({ createOrganization, resetForm }) => {

    function handleSubmit (values) {
        createOrganization({
            inn: _.trim(values.organizationInn),
            onSuccessHandler: (response) => {
                const organizationId = _.get(response, 'body.id')
                resetForm(FORM_NAME)
                HISTORY.push(`/organizations/${organizationId}`)
            }
        })
    }

    return (
        <div className={styles.root}>
            <div className={styles.title}>
                {i18next.t('organizations.organization.create.title')}
            </div>
            <div className={styles.description}>
                {i18next.t('organizations.organization.create.description')}
            </div>

            <OrganizationCreateForm
                onSubmit={handleSubmit}
            />
        </div>
    )
}

OrganizationCreate.propTypes = {
    createOrganization: PropTypes.func,
    resetForm: PropTypes.func,
}

OrganizationCreate.defaultProps = {
    createOrganization: _.noop,
    resetForm: _.noop,
}
