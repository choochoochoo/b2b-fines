import React from 'react'
import PropTypes from 'prop-types'
import i18next from 'i18next'

import styles from './organizations-left-col.css'
import { NewOrganizationButton } from './new-organization-button'
import { OrganizationItem } from './organization-item'

export const OrganizationsLeftCol = ({ list }) =>
    (
        <div className={styles.root}>
            <div className={styles.title}>
                {i18next.t('organizations.left.col.title')}
            </div>
            {
                list.map((item) =>
                    (<OrganizationItem
                        key={item.id}
                        id={item.id}
                        name={item.name}
                        inn={item.inn}
                    />)
                )
            }
            <NewOrganizationButton />
        </div>
    )

OrganizationsLeftCol.propTypes = {
    list: PropTypes.array,
}

OrganizationsLeftCol.defaultProps = {
    list: [],
}

