import React from 'react'
import classnames from 'classnames'
import { Link } from 'react-router-dom'
import i18next from 'i18next'

import { SvgIcon } from '../../general'

import styles from './new-organization-button.css'

export const newOrganizationSelected = () => /new/.test(window.location.pathname)

export const NewOrganizationButton = () => (
    <Link
        to="/organizations/new"
        className={classnames(styles.addOrganizationButton, {
            [styles.addOrganizationButtonSelected]: newOrganizationSelected()
        })}
    >
        <div className={styles.addOrganizationButtonIcon}>
            <SvgIcon icon="plus" />
        </div>
        <div className={styles.addOrganizationButtonText}>
            {i18next.t('organizations.left.col.add.organization.button')}
        </div>
    </Link>
)

