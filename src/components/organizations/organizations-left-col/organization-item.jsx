import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import { Link } from 'react-router-dom'

import styles from './organization-item.css'

export const newOrganizationSelected = (id) => new RegExp(`/organizations/${id}`).test(window.location.pathname)

export const OrganizationItem = ({ id, name, inn }) => (
    <Link
        to={`/organizations/${id}`}
        className={classnames(styles.link, {
            [styles.selected]: newOrganizationSelected(id)
        })}
    >
        <div className={styles.title}>
            {name}
        </div>
        <div className={styles.description}>
            {inn}
        </div>
    </Link>
)

OrganizationItem.propTypes = {
    id: PropTypes.number,
    name: PropTypes.string,
    inn: PropTypes.string,
}

OrganizationItem.defaultProps = {
    id: 0,
    name: '',
    inn: '',
}
