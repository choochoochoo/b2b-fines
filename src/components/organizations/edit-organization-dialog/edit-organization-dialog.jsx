import React from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'
import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux'
import i18next from 'i18next'
import { reset } from 'redux-form'

import { selectors, actions } from '../../../__data__'
import { Dialog, Button } from '../../general'
import { getOrganization } from '../organization-details/organization-details'

import { EditOrganizationForm, FORM_NAME } from './edit-organization-form'
import style from './edit-organization-dialog.css'

export const EditOrganizationDialogPure = (
    {
        editOrganization,
        opened,
        closeEditOrganizationDialog,
        openRemoveOrganizationDialog,
        resetForm,
        organizationId,
        organizations,
    }) => {

    const organization = organizationId ? getOrganization(organizations, organizationId) : {}

    function close () {
        closeEditOrganizationDialog()
        resetForm(FORM_NAME)
    }

    function handleSubmit (values) {
        editOrganization({
            organizationId,
            name: _.trim(values.name),
            address: _.trim(values.address),
            kpp: _.trim(values.kpp),

            onSuccessHandler: () => close(),
            onErrorHandler: () => close(),
        })
    }

    function openRemove () {
        closeEditOrganizationDialog()
        openRemoveOrganizationDialog(organization)
    }

    return (
        <Dialog opened={opened} width="624px" closeClick={close} hideCloseButton>
            <div className={style.header}>
                <div className={style.title}>
                    {i18next.t('organizations.organization.dialog.edit.title')}
                </div>
                <div>
                    <Button
                        onClick={openRemove}
                        colorScheme="second-pink"
                        title={i18next.t('organizations.organization.dialog.edit.delete')}
                    />
                </div>
            </div>

            <EditOrganizationForm
                onSubmit={handleSubmit}
                organization={organization}
                closeEditOrganizationDialog={closeEditOrganizationDialog}
            />
        </Dialog>
    )
}

EditOrganizationDialogPure.propTypes = {
    editOrganization: PropTypes.func,
    opened: PropTypes.bool,
    closeEditOrganizationDialog: PropTypes.func,
    openRemoveOrganizationDialog: PropTypes.func,
    resetForm: PropTypes.func,
    organizationId: PropTypes.number,
    organizations: PropTypes.array,
}

EditOrganizationDialogPure.defaultProps = {
    editOrganization: _.noop,
    opened: false,
    closeEditOrganizationDialog: _.noop,
    openRemoveOrganizationDialog: _.noop,
    resetForm: _.noop,
    organizationId: 0,
    organizations: [],
}

const mapStateToProps = () => createStructuredSelector({
    opened: selectors.organizations.getIsOpenEditOrganizationDialog,
    organizationId: selectors.organizations.getEditOrganizationDialogId,
})

const mapDispatchToProps = (dispatch) => ({
    editOrganization: (param) => dispatch(actions.organization.editOrganization(param)),
    closeEditOrganizationDialog: () => dispatch(actions.organization.closeEditOrganizationDialog()),
    openRemoveOrganizationDialog: (organization) => dispatch(actions.organization.openRemoveOrganizationDialog(organization)),
    resetForm: (formName) => dispatch(reset(formName)),
})

export const EditOrganizationDialog = connect(mapStateToProps, mapDispatchToProps)(EditOrganizationDialogPure)
