import React from 'react'
import { reduxForm } from 'redux-form'
import i18next from 'i18next'
import PropTypes from 'prop-types'
import _ from 'lodash'
import { connect } from 'react-redux'

import { InputField, Button } from '../../general'

import style from './edit-organization-form.css'

export const FORM_NAME = 'EditOrganizationForm'
export const KPP_REGEXP = /^[0-9\s]{9}$/

export const validate = (values) => {
    const errors = {}

    if (!values.name) {
        errors.name = i18next.t('input.field.required')
    }

    if (!values.address) {
        errors.address = i18next.t('input.field.required')
    }

    if (!values.kpp) {
        errors.kpp = i18next.t('input.field.required')
    } else if (!KPP_REGEXP.test(values.kpp)) {
        errors.kpp = i18next.t('organizations.organization.dialog.edit.input.kpp.error')
    }

    return errors
}

const EditOrganizationFormPure = (props) => {
    const { handleSubmit, valid, closeEditOrganizationDialog } = props

    return (
        <form onSubmit={handleSubmit} >
            <InputField
                name="name"
                required
                type="text"
                placeholder={i18next.t('organizations.organization.dialog.edit.input.name')}
                label={i18next.t('organizations.organization.dialog.edit.input.name')}
            />

            <InputField
                name="address"
                required
                type="text"
                placeholder={i18next.t('organizations.organization.dialog.edit.input.address')}
                label={i18next.t('organizations.organization.dialog.edit.input.address')}
            />

            <InputField
                name="kpp"
                required
                type="text"
                placeholder={i18next.t('organizations.organization.dialog.edit.input.kpp')}
                label={i18next.t('organizations.organization.dialog.edit.input.kpp')}
            />

            <Button
                colorScheme="big-green"
                title={i18next.t('organizations.organization.dialog.edit.button.save')}
                disabled={!valid}
                type="submit"
                className={style.saveButton}
            />

            <Button
                onClick={closeEditOrganizationDialog}
                colorScheme="big-second"
                title={i18next.t('organizations.organization.dialog.edit.button.cancel')}
            />
        </form>
    )
}

EditOrganizationFormPure.propTypes = {
    valid: PropTypes.bool,
    handleSubmit: PropTypes.func,
    closeEditOrganizationDialog: PropTypes.func,
}

EditOrganizationFormPure.defaultProps = {
    valid: false,
    handleSubmit: _.noop,
    closeEditOrganizationDialog: _.noop,
}

export const EditOrganizationFormRF = reduxForm({
    form: FORM_NAME,
    validate,
    enableReinitialize: true,
})(EditOrganizationFormPure)

export const EditOrganizationForm = connect(
    (state, ownParam) => {
        const initialValues = {}

        initialValues.name = _.get(ownParam, 'organization.name', '')
        initialValues.address = _.get(ownParam, 'organization.address', '')
        initialValues.kpp = _.get(ownParam, 'organization.kpp', '')

        return {
            initialValues: { ...initialValues }
        }
    }, null, null, { forwardRef: true }
)(EditOrganizationFormRF)
