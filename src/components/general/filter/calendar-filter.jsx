import React, { useState, useRef } from 'react'
import PropTypes from 'prop-types'
import i18next from 'i18next'
import _ from 'lodash'
import classnames from 'classnames'
import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux'
import dayjs from 'dayjs'

import { SvgIcon } from '../svgicon'
import * as selectors from '../../../__data__/selectors'
import * as actions from '../../../__data__/actions'
import { HISTORY } from '../../../'
import { getSort } from '../../fines/sort-button'

import style from './calendar-filter.css'
import { useOutsideClick } from './side-effect'
import { DateField } from './date-field'

export const CalendarFilterPure = (
    {
        filterKey,
        title,
        startDate,
        endDate,
        form,
        originalFilters,
        setFilter,
        clearFilter,
        setDefaultFilter,
        setErrorFilter,
        filterError,
    }) => {

    const [isOpen, setOpen] = useState(false)

    function hide () {
        if (isOpen) {
            setOpen(false)
            setDefaultFilter(originalFilters)
        }
    }

    function open () {
        if (isOpen) {
            hide()
        } else {
            setOpen(true)
        }
    }

    const wrapperRef = useRef(null)
    useOutsideClick(wrapperRef, hide)

    const hasFilter = startDate && endDate

    const startDateKey = `${filterKey}__startDate`
    const endDateKey = `${filterKey}__endDate`
    const starDateValue = form[startDateKey]
    const endDateValue = form[endDateKey]

    function clearFilterClick (event) {
        clearFilter({
            list: [
                { key: startDateKey, value: null },
                { key: endDateKey, value: null },
            ]
        })

        HISTORY.push(`/fines?sort=${getSort(location)}`)

        event.stopPropagation()
    }

    function apply () {

        if (!starDateValue || !endDateValue) {
            setErrorFilter({
                key: filterKey,
                error: i18next.t('general.filter.calendar.empty.fields')
            })

            return
        }

        const parsedStartDate = dayjs(starDateValue, 'DD.MM.YYYY')
        const parsedEndDate = dayjs(endDateValue, 'DD.MM.YYYY')

        if (!parsedStartDate.isValid() || !parsedEndDate.isValid()) {

            setErrorFilter({
                key: filterKey,
                error: i18next.t('general.filter.calendar.empty.format')
            })

            return
        }

        if (parsedStartDate.isAfter(parsedEndDate) && !parsedStartDate.isSame(parsedEndDate)) {
            setErrorFilter({
                key: filterKey,
                error: i18next.t('general.filter.calendar.empty.start.date.earlier')
            })

            return
        }

        HISTORY.push(`/fines?sort=${getSort(location)}`)
    }

    return (
        <div className={classnames(style.root, { [style.hasFilter]: hasFilter })} ref={wrapperRef}>
            <button className={style.dropdownRoot} onClick={open}>
                <div className={style.title}>
                    { hasFilter ? `${title}` : title }
                </div>
                <div className={style.icon}>
                    {
                        hasFilter ? (
                            <button
                                className={style.buttonClearFilter}
                                title={i18next.t('general.filter.button.clear')}
                                onClick={clearFilterClick}
                            >
                                <SvgIcon
                                    icon="dropdownRemove"
                                    width="16"
                                    height="16"
                                />
                            </button>) :
                            <SvgIcon
                                icon="dropdownArrow"
                                width="16"
                                height="16"
                            />
                    }
                </div>
            </button>
            {
                isOpen && (
                    <div className={style.popup} >
                        <div className={style.list}>
                            <DateField
                                label={i18next.t('general.filter.calendar.start.date')}
                                inputKey={startDateKey}
                                value={starDateValue}
                                setFilter={setFilter}
                            />
                            <DateField
                                label={i18next.t('general.filter.calendar.end.date')}
                                inputKey={endDateKey}
                                value={endDateValue}
                                setFilter={setFilter}
                            />
                        </div>
                        {
                            filterError.key === filterKey && (
                                <div className={style.error}>
                                    {filterError.error}
                                </div>
                            )
                        }

                        <div className={style.buttonPanel}>
                            <button className={style.button} onClick={apply}>
                                {i18next.t('general.filter.button.apply')}
                            </button>
                        </div>
                    </div>
                )
            }
        </div>
    )
}

CalendarFilterPure.propTypes = {
    filterKey: PropTypes.string,
    title: PropTypes.string,
    startDate: PropTypes.string,
    endDate: PropTypes.string,
    originalFilters: PropTypes.array,
    form: PropTypes.object,
    setFilter: PropTypes.func,
    clearFilter: PropTypes.func,
    setDefaultFilter: PropTypes.func,
    setErrorFilter: PropTypes.func,
    filterError: PropTypes.object,
}

CalendarFilterPure.defaultProps = {
    title: '',
    startDate: '',
    endDate: '',
    originalFilters: [],
    setFilter: _.noop,
    setDefaultFilter: _.noop,
    setErrorFilter: _.noop,
    clearFilter: _.noop,
    filterKey: '',
    form: {},
    filterError: {},
}

const mapStateToProps = () => createStructuredSelector({
    form: selectors.filters.getForm,
    filterError: selectors.filters.getFilterError,
    originalFilters: selectors.fines.getFilters,
})

const mapDispatchToProps = (dispatch) => ({
    setFilter: (param) => dispatch(actions.setFilter(param)),
    clearFilter: (param) => dispatch(actions.clearFilter(param)),
    setErrorFilter: (filterError) => dispatch(actions.setErrorFilter(filterError)),
    setDefaultFilter: (filters) => dispatch(actions.setDefaultFilter(filters)),
})

export const CalendarFilter = connect(mapStateToProps, mapDispatchToProps)(CalendarFilterPure)
