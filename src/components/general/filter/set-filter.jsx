import React, { useState, useRef } from 'react'
import PropTypes from 'prop-types'
import i18next from 'i18next'
import _ from 'lodash'
import classnames from 'classnames'
import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux'

import { SvgIcon } from '../svgicon'
import * as selectors from '../../../__data__/selectors'
import * as actions from '../../../__data__/actions'
import { HISTORY } from '../../../'
import { getSort } from '../../fines/sort-button'

import style from './set-filter.css'
import { useOutsideClick } from './side-effect'

export const SetFilterPure = (
    {
        filterKey,
        title,
        list,
        form,
        originalFilters,
        setFilter,
        clearFilter,
        setDefaultFilter,
    }) => {

    const [isOpen, setOpen] = useState(false)

    function hide () {
        if (isOpen) {
            setOpen(false)
            setDefaultFilter(originalFilters)
        }
    }

    function open () {
        if (isOpen) {
            hide()
        } else {
            setOpen(true)
        }
    }

    function apply () {
        HISTORY.push(`/fines?sort=${getSort(location)}`)
    }

    const wrapperRef = useRef(null)
    useOutsideClick(wrapperRef, hide)

    function clickInput (event) {
        setFilter({
            key: event.target.id,
            value: event.target.checked
        })
    }

    const selectedFilters = list.filter((item) => item.selected)
    const hasFilter = selectedFilters.length > 0

    function clearFilterClick (event) {
        clearFilter({
            list: list.map((item) => ({
                key: `${filterKey}__${item.key}`,
                value: false,
            }))
        })

        HISTORY.push(`/fines?sort=${getSort(location)}`)

        event.stopPropagation()
    }

    return (
        <div className={classnames(style.root, { [style.hasFilter]: hasFilter })} ref={wrapperRef}>
            <button className={style.dropdownRoot} onClick={open}>
                <div className={style.title}>
                    { hasFilter ? `${title}: ${selectedFilters.length}` : title }
                </div>
                <div className={style.icon}>
                    {
                        hasFilter ? (
                            <button
                                className={style.buttonClearFilter}
                                title={i18next.t('general.filter.button.clear')}
                                onClick={clearFilterClick}
                            >
                                <SvgIcon
                                    icon="dropdownRemove"
                                    width="16"
                                    height="16"
                                />
                            </button>) :
                            <SvgIcon
                                icon="dropdownArrow"
                                width="16"
                                height="16"
                            />
                    }

                </div>
            </button>
            {
                isOpen && (
                    <div className={style.popup} >
                        <div className={style.list}>
                            {
                                list.map((item) => {
                                    const id = `${filterKey}__${item.key}`
                                    return (
                                        <div className={style.option} key={id}>
                                            <input
                                                id={id}
                                                name={id}
                                                type="checkbox"
                                                className={style.optionCheckbox}
                                                onClick={clickInput}
                                                checked={form[id]}
                                            />
                                            <label htmlFor={id} className={style.optionText}>
                                                {item.description}
                                            </label>
                                        </div>
                                    )
                                })
                            }
                        </div>
                        <div className={style.buttonPanel}>
                            <button className={style.button} onClick={apply}>
                                {i18next.t('general.filter.button.apply')}
                            </button>
                        </div>
                    </div>
                )
            }
        </div>
    )
}

SetFilterPure.propTypes = {
    filterKey: PropTypes.string,
    title: PropTypes.string,
    list: PropTypes.array,
    originalFilters: PropTypes.array,
    form: PropTypes.object,
    setFilter: PropTypes.func,
    clearFilter: PropTypes.func,
    setDefaultFilter: PropTypes.func,
}

SetFilterPure.defaultProps = {
    title: '',
    list: [],
    originalFilters: [],
    setFilter: _.noop,
    clearFilter: _.noop,
    setDefaultFilter: _.noop,
    filterKey: '',
    form: {},
}

const mapStateToProps = () => createStructuredSelector({
    form: selectors.filters.getForm,
    originalFilters: selectors.fines.getFilters,
})

const mapDispatchToProps = (dispatch) => ({
    setFilter: (param) => dispatch(actions.setFilter(param)),
    clearFilter: (param) => dispatch(actions.clearFilter(param)),
    setDefaultFilter: (filters) => dispatch(actions.setDefaultFilter(filters)),
})

export const SetFilter = connect(mapStateToProps, mapDispatchToProps)(SetFilterPure)
