import React from 'react'
import PropTypes from 'prop-types'
import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux'
import _ from 'lodash'

import * as selectors from '../../../__data__/selectors'
import * as actions from '../../../__data__/actions'
import { HISTORY } from '../../../'
import { getSort } from '../../fines/sort-button'

import style from './checkbox-filter.css'

export const CheckboxFilterPure = (
    {
        filterKey,
        title,
        form,
        setFilter,
        location,
    }) => {

    function clickInput (event) {

        const fieldKey = event.target.id
        const fieldValue = event.target.checked

        setFilter({
            key: fieldKey,
            value: fieldValue
        })

        HISTORY.push(`/fines?sort=${getSort(location)}`)
    }

    return (
        <div className={style.root}>
            <input
                id={filterKey}
                name={filterKey}
                type="checkbox"
                className={style.checkbox}
                onClick={clickInput}
                checked={form[filterKey]}
            />
            <label htmlFor={filterKey} className={style.label}>
                {title}
            </label>
        </div>
    )
}

CheckboxFilterPure.propTypes = {
    filterKey: PropTypes.string,
    title: PropTypes.string,
    form: PropTypes.object,
    setFilter: PropTypes.func,
    location: PropTypes.object,
}

CheckboxFilterPure.defaultProps = {
    filterKey: '',
    title: '',
    setFilter: _.noop,
    form: {},
    location: {},
}

const mapStateToProps = () => createStructuredSelector({
    form: selectors.filters.getForm,
})

const mapDispatchToProps = (dispatch) => ({
    setFilter: (param) => dispatch(actions.setFilter(param)),
    getFines: (param) => dispatch(actions.getFines(param)),
})

export const CheckboxFilter = connect(mapStateToProps, mapDispatchToProps)(CheckboxFilterPure)
