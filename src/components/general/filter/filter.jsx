import React from 'react'
import PropTypes from 'prop-types'

import style from './filter.css'
import { CalendarFilter } from './calendar-filter'
import { SetFilter } from './set-filter'
import { CheckboxFilter } from './checkbox-filter'

const build = (items, location) => items.map((item) => {

    let answer = null

    switch (item.type) {
        case 'calendar':
            answer = (
                <CalendarFilter
                    key={item.key}
                    filterKey={item.key}
                    title={item.description}
                    startDate={item.startDate}
                    endDate={item.endDate}
                    location={location}
                />
            )

            break
        case 'checkbox':
            answer = (
                <CheckboxFilter
                    key={item.key}
                    filterKey={item.key}
                    title={item.description}
                    selected={item.selected}
                    location={location}
                />
            )

            break
        default:
            answer = (
                <SetFilter
                    key={item.key}
                    filterKey={item.key}
                    title={item.description}
                    list={item.set}
                    location={location}
                />
            )
            break
    }

    return answer
})

export const Filter = ({ items, location }) => (
    <div className={style.root}>
        {build(items, location)}
    </div>
)

Filter.propTypes = {
    items: PropTypes.array,
    location: PropTypes.object,
}

Filter.defaultProps = {
    items: [],
    location: {},
}
