import React, { useRef, useState } from 'react'
import PropTypes from 'prop-types'
import MaskedTextInput from 'react-text-mask'
import 'react-datepicker/dist/react-datepicker-cssmodules.css'
import DatePicker, { registerLocale } from 'react-datepicker'
import ruLocale from 'date-fns/locale/ru'
import dayjs from 'dayjs'
import ruLocaleDayJs from 'dayjs/locale/ru'
import customParseFormat from 'dayjs/plugin/customParseFormat'
import _ from 'lodash'

import { SvgIcon } from '../svgicon'

import style from './date-field.css'
import { useOutsideClick } from './side-effect'

registerLocale('ru', ruLocale)
dayjs.extend(customParseFormat)
dayjs.locale(ruLocaleDayJs)

export const DateField = ({ inputKey, label, value, setFilter }) => {

    const [isOpen, setOpen] = useState(false)

    function hide () {
        if (isOpen) {
            setOpen(false)
        }
    }

    const wrapperRef = useRef(null)
    useOutsideClick(wrapperRef, hide)

    function open () {
        if (isOpen) {
            setOpen(false)
        } else {
            setOpen(true)
        }
    }

    function calendarChange (selectedValue) {
        setFilter({
            key: inputKey,
            value: dayjs(selectedValue).format('DD.MM.YYYY')
        })

        hide()
    }

    function changeInput (event) {
        setFilter({
            key: inputKey,
            value: event.target.value
        })
    }

    const dateForCalendar = dayjs(value, 'DD.MM.YYYY')

    return (
        <div className={style.root} ref={wrapperRef}>
            <label className={style.dateLabel} htmlFor={inputKey}>
                {label}
            </label>
            <MaskedTextInput
                id={inputKey}
                className={style.dateInput}
                value={value}
                type="text"
                mask={[/\d/, /\d/, '.', /\d/, /\d/, '.', /\d/, /\d/, /\d/, /\d/]}
                onChange={changeInput}
            />
            <button className={style.icon} onClick={open}>
                <SvgIcon
                    icon="calendar"
                    width="24"
                    height="24"
                />
            </button>
            {
                isOpen &&
                <div className={style.calendar}>
                    <DatePicker
                        selected={dateForCalendar.isValid() ? dateForCalendar.toDate() : new Date()}
                        onChange={calendarChange}
                        dateFormat="dd.MM.yyyy"
                        locale="ru"
                        maxDate={new Date()}
                        inline
                    />
                </div>
            }
        </div>
    )
}

DateField.propTypes = {
    inputKey: PropTypes.string,
    label: PropTypes.string,
    value: PropTypes.string,
    setFilter: PropTypes.func,
}

DateField.defaultProps = {
    inputKey: '',
    label: '',
    value: '',
    setFilter: _.noop,
}
