import React from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'
import classnames from 'classnames'

import styles from './button.css'

export const Button = ({ title, onClick, colorScheme, disabled, type, className }) =>
    (
        <button
            className={classnames(styles.root, styles[colorScheme], className)}
            title={title}
            onClick={onClick}
            type={type || 'button'}
            disabled={disabled}
        >
            <div className={styles.buttonText}>
                {title}
            </div>
        </button>
    )

Button.propTypes = {
    title: PropTypes.string,
    colorScheme: PropTypes.string,
    onClick: PropTypes.func,
    disabled: PropTypes.bool,
    type: PropTypes.string,
    className: PropTypes.string,
}

Button.defaultProps = {
    title: '',
    colorScheme: '',
    onClick: _.noop,
    disabled: false,
    type: '',
    className: '',
}
