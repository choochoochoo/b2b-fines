import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

import styles from './link-button.css'

export const LinkButton = ({ title, href }) =>
    (
        <Link
            to={href}
            className={styles.root}
        >
            {title}
        </Link>
    )

LinkButton.propTypes = {
    title: PropTypes.string,
    href: PropTypes.string,
}

LinkButton.defaultProps = {
    title: '',
    href: '',
}
