import React from 'react'
import PropTypes from 'prop-types'
import i18next from 'i18next'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import classnames from 'classnames'
import _ from 'lodash'

import { HISTORY } from '../../../'
import * as actions from '../../../__data__/actions'

import style from './navbar.css'

export const isFinesTab = () => /fines/.test(window.location.pathname)

export const isVehiclesTab = () => /vehicles/.test(window.location.pathname)

export const isOrganizationsTab = () => /organizations/.test(window.location.pathname)

export const isFaqTab = () => /faq/.test(window.location.pathname)

export const NavbarPure = ({ logout }) => {

    function logoutHandler () {
        logout({
            onSuccessHandler: () => {
                HISTORY.push('/')
            },
        })
    }

    return (
        <nav className={style.root}>
            <ul className={style.tabs}>
                <li
                    className={classnames({
                        [style.selectedTab]: isFinesTab()
                    })}
                >
                    <Link
                        to="/fines"
                        className={style.tabLink}
                    >
                        {i18next.t('nav.tab.fines')}
                    </Link>
                </li>
                <li
                    className={classnames({
                        [style.selectedTab]: isVehiclesTab()
                    })}
                >
                    <Link
                        to="/vehicles"
                        className={style.tabLink}
                    >
                        {i18next.t('nav.tab.vehicles')}
                    </Link>
                </li>
                <li
                    className={classnames({
                        [style.selectedTab]: isOrganizationsTab()
                    })}
                >
                    <Link
                        to="/organizations"
                        className={style.tabLink}
                    >
                        {i18next.t('nav.tab.organizations')}
                    </Link>
                </li>
                <li
                    className={classnames({
                        [style.selectedTab]: isFaqTab()
                    })}
                >
                    <Link
                        to="/faq"
                        className={style.tabLink}
                    >
                        {i18next.t('nav.tab.faq')}
                    </Link>
                </li>
            </ul>
            <div className={style.rightCol}>
                <button
                    title={i18next.t('nav.button.exit')}
                    className={style.button}
                    onClick={logoutHandler}
                >
                    {i18next.t('nav.button.exit')}
                </button>
            </div>
        </nav>
    )
}

NavbarPure.propTypes = {
    logout: PropTypes.func,
}

NavbarPure.defaultProps = {
    logout: _.noop,
}

const mapDispatchToProps = (dispatch) => ({
    logout: (param) => dispatch(actions.logout(param)),
})

export const Navbar = connect(null, mapDispatchToProps)(NavbarPure)
