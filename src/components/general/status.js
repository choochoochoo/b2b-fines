import dayjs from 'dayjs'
import i18next from 'i18next'

import config from '../../../config.json'

import { formatDate } from './utils'

export const STATUS_NONE = 'STATUS_NONE'

export const STATUS_HAS_DISCOUNT = 'STATUS_HAS_DISCOUNT'

export const STATUS_WILL_SEND_FSSP = 'STATUS_WILL_SEND_FSSP'

export const STATUS_SENT_FSSP = 'STATUS_SENT_FSSP'

export const getStatus = (discountDate, actDate) => {

    const currenDate = dayjs()
        .set('hour', 0)
        .set('minute', 0)
        .set('second', 0)
        .set('millisecond', 0)

    if (discountDate) {
        const discountDateDayJs = dayjs(discountDate)

        if (discountDateDayJs.isAfter(currenDate) || discountDateDayJs.isSame(currenDate)) {
            return {
                status: STATUS_HAS_DISCOUNT,
                remain: discountDateDayJs.diff(currenDate, 'day') + 1
            }
        }
    }

    if (actDate) {
        const actDateDayJs = dayjs(actDate)

        const remain = actDateDayJs.diff(currenDate, 'day') - 1 + config.fsspSendDays

        if (remain < 0) {
            return {
                status: STATUS_SENT_FSSP,
                remain: 0,
            }
        }

        return {
            status: STATUS_WILL_SEND_FSSP,
            remain,
        }
    }

    return {
        status: STATUS_NONE,
        remain: 0,
    }
}

export const getStatusIcon = (status) => {
    switch (status) {
        case STATUS_HAS_DISCOUNT:
            return 'statusDiscount'
        case STATUS_WILL_SEND_FSSP:
            return 'statusWillSendFssp'
        case STATUS_SENT_FSSP:
            return 'statusSentFssp'
        default:
            return 'statusNone'
    }
}

export const getStatusTooltip = (status, discountDate, actDate) => {
    switch (status.status) {
        case STATUS_HAS_DISCOUNT:
            return i18next.t('fines.details.status.has.discount.tooltip', { DISCOUNT_DATE: formatDate(discountDate) })
        case STATUS_WILL_SEND_FSSP:
            return i18next.t('fines.details.status.will.send.fssp.tooltip', { ACT_DATE: formatDate(actDate) })
        case STATUS_SENT_FSSP:
            return i18next.t('fines.details.status.sent.fssp.tooltip')
        default:
            return ''
    }
}

export const getStatusMainText = (status) => {
    switch (status.status) {
        case STATUS_HAS_DISCOUNT:
            return i18next.t('fines.details.status.has.discount.main.text', { count: status.remain, REMAIN: status.remain })
        case STATUS_WILL_SEND_FSSP:
            return i18next.t('fines.details.status.will.send.fssp.main.text', { count: status.remain, REMAIN: status.remain })
        case STATUS_SENT_FSSP:
            return i18next.t('fines.details.status.sent.fssp.main.text')
        default:
            return ''
    }
}

export const getFullStatus = (status, discountDate, actDate) => {
    switch (status.status) {
        case STATUS_HAS_DISCOUNT: {
            return `${getStatusTooltip(status, discountDate, actDate)} (${getStatusMainText(status)})`
        }
        case STATUS_WILL_SEND_FSSP: {
            return `${getStatusTooltip(status, discountDate, actDate)} (${getStatusMainText(status)})`
        }
        case STATUS_SENT_FSSP:
            return i18next.t('fines.details.status.sent.fssp.tooltip')
        default:
            return ''
    }
}
