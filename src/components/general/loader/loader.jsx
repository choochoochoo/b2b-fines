import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'

import * as selectors from '../../../__data__/selectors'

import styles from './loader.css'

const markRef = React.createRef()

export const LoaderPure = ({ opened }) => {
    useEffect(() => {
        if (markRef.current) {
            markRef.current.addEventListener('click', (event) => event.preventDefault())
        }
    }, [])

    return (
        <div
            className={styles.modal}
            style={{
                opacity: opened ? 1 : 0,
                visibility: opened ? 'visible' : 'hidden'
            }}
            ref={markRef}
        >
            <div className={styles.ldsRing}>
                <div />
                <div />
                <div />
                <div />
            </div>
        </div>
    )
}
LoaderPure.propTypes = {
    opened: PropTypes.bool,
}

LoaderPure.defaultProps = {
    opened: '',
}

const mapStateToProps = () => createStructuredSelector({
    opened: selectors.global.getLoaderOpened
})

export const Loader = connect(mapStateToProps, null)(LoaderPure)
