import dayjs from 'dayjs'
import i18next from 'i18next'

import { maskString } from './mask'

export const formatMoney = (amount) => {
    if (amount) {
        return new Intl.NumberFormat('ru-RU', { style: 'currency', currency: 'RUB' })
            .format(amount).replace(',00', '')
    }

    return null
}


export const formatSts = (sts) => maskString(sts, 'SS SS SSSSSS', { S: /[A-Za-z0-9а-яА-Я]/ })

export const formatDate = (date) => {

    if (!date) {
        return null
    }

    const parsedDate = dayjs(date)

    if (parsedDate.hour() === 0 && parsedDate.minute() === 0) {
        return parsedDate.format('DD.MM.YYYY')
    }

    return parsedDate.format('DD.MM.YYYY, HH:mm')
}

export const insertText = (text) => text || i18next.t('fines.common.text.default')
