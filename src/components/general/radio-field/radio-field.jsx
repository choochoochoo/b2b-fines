import React from 'react'
import PropTypes from 'prop-types'
import { Field } from 'redux-form'

import styles from './radio-field.css'

const renderField = (
    {
        input,
        label,
        type,
    }) =>
    (
        <label className={styles.label}>
            <div>
                <input
                    {...input}
                    className={styles.input}
                    type={type}
                />
            </div>
            <div className={styles.labelText}>
                {label}
            </div>
        </label>
    )

renderField.propTypes = {
    input: PropTypes.object,
    type: PropTypes.string,
    label: PropTypes.string,
}

renderField.defaultProps = {
    input: {},
    type: '',
    label: '',
}

export const RadioField = ({ name, label, value }) =>
    (
        <Field
            name={name}
            type="radio"
            component={renderField}
            label={label}
            value={value}
        />
    )

RadioField.propTypes = {
    name: PropTypes.string,
    label: PropTypes.string,
    value: PropTypes.string,
}

RadioField.defaultProps = {
    name: '',
    label: '',
    value: '',
}
