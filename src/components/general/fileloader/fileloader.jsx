import React from 'react'
import i18next from 'i18next'
import PropTypes from 'prop-types'
import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux'
import _ from 'lodash'

import * as selectors from '../../../__data__/selectors'
import * as actions from '../../../__data__/actions'
import { HISTORY } from '../../../'

import { FileLoaderBase } from './fileloaderbase'
import style from './fileloader.css'

const url = '/api/profiles/files/vehicles'

const MAX_FILE_SIZE = 1048576
const EMPTY_FILE_SIZE = 8

export const FileLoaderPure = ({ uploadVehiclesFileName, uploadVehicles, getProfile, openDialog }) => {
    function handleUploadVehicles (files) {

        if (files.file.size > MAX_FILE_SIZE) {
            openDialog(
                i18next.t('fileloader.error.big.file.title'),
                i18next.t('fileloader.error.big.file.message')
            )

            return
        }

        if (files.file.size < EMPTY_FILE_SIZE) {
            openDialog(
                i18next.t('fileloader.error.empty.file.title'),
                i18next.t('fileloader.error.empty.file.message')
            )

            return
        }

        uploadVehicles({
            file: files.file,
            onSuccessHandler: () => {
                getProfile({})
                HISTORY.push('/vehicles')
            },
        })
    }

    return (
        <div className={style.root}>
            <div className={style.title}>
                {i18next.t('fileloader.title')}
            </div>
            <div className={style.fileName}>
                {uploadVehiclesFileName}
            </div>
            <div className={style.description1}>
                {i18next.t('fileloader.description1')}
            </div>
            <div className={style.description1}>
                {i18next.t('fileloader.description2')}
            </div>
            <a
                className={style.downloadButton}
                href={url}
                target="_blank"
                rel="noopener noreferrer"
            >
                {i18next.t('fileloader.button.download')}
            </a>
            <FileLoaderBase uploadFile={handleUploadVehicles}>
                <button
                    className={style.newButton}
                    title={i18next.t('fileloader.button.new')}
                >
                    <div className={style.newButtonText}>
                        {i18next.t('fileloader.button.new')}
                    </div>
                </button>
            </FileLoaderBase>
        </div>
    )
}

FileLoaderPure.propTypes = {
    uploadVehiclesFileName: PropTypes.string,
    uploadVehicles: PropTypes.func,
    getProfile: PropTypes.func,
    openDialog: PropTypes.func,
}

FileLoaderPure.defaultProps = {
    uploadVehiclesFileName: '',
    uploadVehicles: _.noop,
    getProfile: _.noop,
    openDialog: _.noop,
}

const mapStateToProps = () => createStructuredSelector({
    uploadVehiclesFileName: selectors.global.getUploadVehiclesFileName,
})

const mapDispatchToProps = (dispatch) => ({
    uploadVehicles: (param) => dispatch(actions.uploadVehicles(param)),
    getProfile: (param) => dispatch(actions.getProfile(param)),
    openDialog: (title, message) => dispatch(actions.global.openDialog(title, message)),
})

export const FileLoader = connect(mapStateToProps, mapDispatchToProps)(FileLoaderPure)
