import React from 'react'
import PropTypes from 'prop-types'
import Dropzone from 'react-dropzone'
import _ from 'lodash'

export const FileLoaderBase = ({ children, uploadFile }) => {

    function handleDrop (acceptedFiles) {
        if (acceptedFiles.length > 0) {
            uploadFile({
                file: acceptedFiles[0]
            })
        }
    }

    return (
        <Dropzone onDrop={handleDrop}>
            {({ getRootProps, getInputProps }) => (
                <section>
                    <div {...getRootProps()}>
                        <input {...getInputProps()} />
                        {children}
                    </div>
                </section>
            )}
        </Dropzone>
    )
}

FileLoaderBase.propTypes = {
    children: PropTypes.node,
    uploadFile: PropTypes.func,
}

FileLoaderBase.defaultProps = {
    children: null,
    uploadFile: _.noop,
}
