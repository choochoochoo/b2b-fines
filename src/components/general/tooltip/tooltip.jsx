import React from 'react'
import PropTypes from 'prop-types'
import i18next from 'i18next'

import styles from './tooltip.css'

export const Tooltip = ({ title, children, className }) =>
    (
        <div className={styles.root}>
            <div className={className}>{children}</div>
            <div className={styles.tooltipWrapper}>
                <div className={styles.tooltipBlock}>
                    <div className={styles.tooltipText}>
                        {title || i18next.t('fines.common.tooltip.default')}
                    </div>
                </div>
            </div>
        </div>
    )

Tooltip.propTypes = {
    title: PropTypes.string,
    className: PropTypes.string,
    children: PropTypes.node,
}

Tooltip.defaultProps = {
    title: '',
    className: '',
    children: null,
}
