import React from 'react'
import i18next from 'i18next'
import 'url-search-params-polyfill'
import classnames from 'classnames'
import PropTypes from 'prop-types'

import { SvgIcon } from '../svgicon'

import { Button } from './button'
import style from './paging.css'

export const getCurrentPage = (location) => {
    const urlParams = new URLSearchParams(location.search)
    const page = urlParams.get('page')
    return page ? parseInt(page, 10) : 1
}

export const Paging = ({ currentPage, hasMore, sort }) => (
    <div className={style.root}>
        {
            currentPage !== 1 &&
            <Button
                page={currentPage - 1}
                sort={sort}
                title={i18next.t('paging.button.prev.page')}
            >

                <div className={classnames(style.arrowBlock, style.prev)}>
                    <div className={style.arrow}>
                        <SvgIcon icon="arrow" />
                    </div>
                    <div className={style.arrowText}>
                        {i18next.t('paging.prev')}
                    </div>
                </div>
            </Button>
        }
        {
            <div className={style.pageButton}>
                <div className={style.pageButtonText}>
                    {currentPage.toString()}
                </div>
            </div>
        }
        {
            hasMore &&
            <Button
                page={currentPage + 1}
                sort={sort}
                title={i18next.t('paging.button.next.page')}
            >
                <div className={classnames(style.arrowBlock, style.next)}>
                    <div>
                        {i18next.t('paging.next')}
                    </div>
                    <div className={style.arrow}>
                        <SvgIcon icon="arrow" />
                    </div>
                </div>
            </Button>
        }
    </div>
)

Paging.propTypes = {
    sort: PropTypes.string,
    currentPage: PropTypes.number,
    hasMore: PropTypes.bool,
}

Paging.defaultProps = {
    sort: null,
    currentPage: 0,
    hasMore: false,
}
