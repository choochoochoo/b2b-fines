import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import classnames from 'classnames'

import styles from './button.css'

export const Button = ({ page, active, children, className, title, sort }) => {

    const style = classnames(styles.button, className, {
        [styles.active]: active
    })

    const content = (
        <div className={styles.wrapper}>
            <div className={styles.text}>
                {children}
            </div>
        </div>
    )

    if (active) {
        return (
            <span className={style}>{content}</span>
        )
    }

    const sortParam = sort ? `&sort=${sort}` : ''

    return (
        <Link
            to={`?page=${page}${sortParam}`}
            className={style}
            title={title}
        >
            {content}
        </Link>
    )
}

Button.propTypes = {
    page: PropTypes.number,
    active: PropTypes.bool,
    children: PropTypes.node,
    className: PropTypes.string,
    title: PropTypes.string,
    sort: PropTypes.string,
}

Button.defaultProps = {
    page: 1,
    active: false,
    children: null,
    className: '',
    title: '',
    sort: null,
}
