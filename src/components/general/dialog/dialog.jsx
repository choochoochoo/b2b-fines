import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'

import { SvgIcon } from '../svgicon'

import styles from './dialog.css'
import { useEscapeClick } from './use-escape-click'
import { useOutsideClick } from './use-outside-click'

export const Dialog = ({ children, opened, closeClick, width, hideCloseButton, dialogClass }) => {

    const dialogRef = React.createRef()
    const modalRef = React.createRef()

    function close () {
        if (opened) {
            closeClick()
        }
    }

    useEscapeClick(modalRef, close)
    useOutsideClick(dialogRef, close)

    return (
        <div
            className={styles.modal}
            style={{
                opacity: opened ? 1 : 0,
                visibility: opened ? 'visible' : 'hidden',
            }}
            ref={modalRef}
        >
            <div
                className={classnames(styles.root, dialogClass)}
                style={{
                    width
                }}
                ref={dialogRef}
            >
                {children}
                {
                    !hideCloseButton &&
                    <button className={styles.buttonClose} onClick={closeClick}>
                        <SvgIcon
                            icon="closeDialog"
                            width="20"
                            height="20"
                        />
                    </button>
                }
            </div>
        </div>
    )
}

Dialog.propTypes = {
    children: PropTypes.node,
    opened: PropTypes.bool,
    closeClick: PropTypes.func,
    width: PropTypes.string,
    hideCloseButton: PropTypes.bool,
    dialogClass: PropTypes.string,
}

Dialog.defaultProps = {
    children: null,
    opened: false,
    closeClick: null,
    width: '0',
    hideCloseButton: false,
    dialogClass: '',
}

