import { useEffect } from 'react'

const ESCAPE_KEY_CODE = 27

export const useEscapeClick = (ref, callback) => {
    const handleClickOutside = (event) => {
        if (ESCAPE_KEY_CODE === event.keyCode) {
            // eslint-disable-next-line callback-return
            callback()
        }
    }

    useEffect(() => {
        document.addEventListener('keydown', handleClickOutside)
        return () => {
            document.removeEventListener('keydown', handleClickOutside)
        }
    })
}
