export const maskString = (value = '', mask = '', maskPatterns = {}) => {
    let maskedValue = ''
    const valueParts = value.split('')
    const maskParts = mask.split('')

    while (valueParts.length > 0) {
        let unmaskedChar = valueParts.shift()

        while (unmaskedChar !== null) {
            const maskChar = maskParts.shift()

            // eslint-disable-next-line no-undefined
            if (maskChar !== undefined) {
                const maskPattern = maskPatterns[maskChar.toUpperCase()]

                // eslint-disable-next-line no-undefined
                if (maskPattern !== undefined) {
                    let check = false

                    if (typeof maskPattern === 'function') {
                        check = maskPattern(unmaskedChar)
                    } else if (maskPattern instanceof RegExp) {
                        check = maskPattern.test(unmaskedChar)
                    }

                    if (check) {
                        maskedValue += unmaskedChar
                    } else {
                        maskParts.unshift(maskChar)
                    }

                    unmaskedChar = null
                } else {
                    maskedValue += maskChar
                }
            } else {
                unmaskedChar = null
            }
        }
    }

    return maskedValue
}
