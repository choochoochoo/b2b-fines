import React from 'react'
import PropTypes from 'prop-types'

import style from './row-selector.css'

export const RowSelector = ({ children }) => (
    <section className={style.root} >{children}</section>
)


RowSelector.propTypes = {
    children: PropTypes.node,
}

RowSelector.defaultProps = {
    children: null,
}
