import React from 'react'
import PropTypes from 'prop-types'
import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux'
import i18next from 'i18next'
import _ from 'lodash'

import * as selectors from '../../../__data__/selectors'
import * as actions from '../../../__data__/actions'
import { Dialog, Button } from '../'

import styles from './dialog-error.css'

export const DialogErrorPure = ({ title, message, opened, closeDialog }) =>
    (
        <Dialog
            opened={opened}
            width="544px"
            closeClick={closeDialog}
        >
            <div className={styles.title}>
                {title || i18next.t('dialog.error.title')}
            </div>
            <hr className={styles.line} />
            <div className={styles.body}>
                {message || i18next.t('dialog.error.common.message')}
            </div>
            <Button
                colorScheme="green"
                onClick={closeDialog}
                title={i18next.t('dialog.error.button.close')}
            />
        </Dialog>
    )

DialogErrorPure.propTypes = {
    title: PropTypes.string,
    message: PropTypes.string,
    opened: PropTypes.bool,
    closeDialog: PropTypes.func,
}

DialogErrorPure.defaultProps = {
    title: '',
    message: '',
    opened: false,
    closeDialog: _.noop,
}

const mapStateToProps = () => createStructuredSelector({
    opened: selectors.global.getIsDialogOpen,
    message: selectors.global.getIsDialogMessage,
    title: selectors.global.getIsDialogTitle,
})

const mapDispatchToProps = (dispatch) => ({
    closeDialog: () => dispatch(actions.global.closeDialog()),
})

export const DialogError = connect(mapStateToProps, mapDispatchToProps)(DialogErrorPure)
