import React from 'react'
import PropTypes from 'prop-types'

import style from './template.css'

export const Template = ({ children }) => (
    <div className={style.root}>
        <div className={style.content}>{children}</div>
    </div>
)

Template.propTypes = {
    children: PropTypes.node,
}

Template.defaultProps = {
    children: null,
}
