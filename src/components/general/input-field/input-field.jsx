import React from 'react'
import PropTypes from 'prop-types'
import { Field } from 'redux-form'
import classnames from 'classnames'

import styles from './input-field.css'

const renderField = (
    {
        input,
        placeholder,
        type,
        meta: { touched, error, active },
        label,
    }) =>
    (
        <label
            className={classnames(styles.label, {
                [styles.visible]: active || input.value,
                [styles.hasError]: touched && error,
            })}
        >
            <div className={styles.labelText}>
                {label}
            </div>
            <div>
                <input
                    {...input}
                    placeholder={placeholder}
                    type={type}
                    className={classnames(styles.input, {
                        [styles.active]: active
                    })}
                />
            </div>
            {
                <div
                    className={classnames(styles.error, {
                        [styles.errorVisible]: touched && error
                    })}
                >
                    {error}
                </div>
            }
        </label>
    )

renderField.propTypes = {
    input: PropTypes.object,
    meta: PropTypes.object,
    type: PropTypes.string,
    placeholder: PropTypes.string,
    label: PropTypes.string,
}

renderField.defaultProps = {
    input: {},
    meta: {},
    type: '',
    placeholder: '',
    label: '',
}

export const InputField = ({ name, type, placeholder, label }) =>
    (
        <Field
            name={name}
            type={type}
            placeholder={placeholder}
            component={renderField}
            label={label}
        />
    )

InputField.propTypes = {
    name: PropTypes.string,
    type: PropTypes.string,
    placeholder: PropTypes.string,
    label: PropTypes.string,
}

InputField.defaultProps = {
    name: '',
    type: '',
    placeholder: '',
    label: '',
}
