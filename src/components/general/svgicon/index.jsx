/* eslint-disable */

import React from 'react'
import PropTypes from 'prop-types'

import {
    photo,
    arrow,
    dropdownArrow,
    dropdownRemove,
    calendar,
    sortArrow,
    violation20to40,
    violation40to60,
    violationOther,
    violationParking,
    violationNone,
    warning,
    unknownAuto,
    accordionArrow,
    closeDialog,
    statusDiscount,
    statusSentFssp,
    statusWillSendFssp,
    statusNone,
    breadArrow,
    pencil,
    plus,
    faqPlus,
    faqMinus,
} from './icons'

const icons = {
    photo,
    arrow,
    sortArrow,
    dropdownArrow,
    dropdownRemove,
    calendar,
    warning,
    violation_20_40: violation20to40,
    violation_40_60: violation40to60,
    violation_other: violationOther,
    violation_parking: violationParking,
    violation_none: violationNone,
    unknownAuto,
    accordionArrow,
    closeDialog,
    statusDiscount,
    statusSentFssp,
    statusWillSendFssp,
    statusNone,
    breadArrow,
    pencil,
    plus,
    faqPlus,
    faqMinus,
}

export const SvgIcon = ({ icon, width, height, className }) => {
    const iconStyle = {
        width: `${width}px`,
        height: `${height}px`,
    }

    return (
        <span
            dangerouslySetInnerHTML={{ __html: icons[icon] }}
            style={iconStyle}
            className={className}
        />
    )
}

SvgIcon.propTypes = {
    icon: PropTypes.string,
    width: PropTypes.number,
    height: PropTypes.number,
    className: PropTypes.string,
}

SvgIcon.defaultProps = {
    icon: '',
    width: 0,
    height: 0,
    className: '',
}
