import React from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'
import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux'
import i18next from 'i18next'
import { reset } from 'redux-form'
import { withRouter } from 'react-router'

import { selectors, actions } from '../../../__data__'
import { Button, Dialog } from '../../general'
import { getOrganization } from '../../organizations/organization-details/organization-details'

import { EditBranchForm, FORM_NAME } from './edit-branch-form'
import style from './edit-branch-dialog.css'

export const getBranch = (branches, id) => branches.find((item) => item.id.toString() === id.toString()) || {}

export const EditBranchDialogPure = (
    {
        editBranch,
        opened,
        closeEditBranchDialog,
        resetForm,
        match,
        organizations,
        branchId,
        openRemoveBranchDialog,
    }) => {

    function close () {
        closeEditBranchDialog()
        resetForm(FORM_NAME)
    }

    function handleSubmit (values) {
        editBranch({
            branchId,
            name: _.trim(values.name),
            address: _.trim(values.address),
            kpp: _.trim(values.kpp),

            onSuccessHandler: () => close(),
            onErrorHandler: () => close(),
        })
    }

    const organizationId = _.get(match, 'params.id')
    const organization = organizationId ? getOrganization(organizations, organizationId) : {}
    const branch = branchId && getBranch(organization.branches, branchId)

    function openRemove () {
        openRemoveBranchDialog({ ...branch, organizationId })
        close()
    }

    return (
        <Dialog opened={opened} width="624px" closeClick={close} hideCloseButton>
            <div className={style.header}>
                <div className={style.title}>
                    {i18next.t('organizations.branch.dialog.edit.title')}
                </div>
                <div>
                    <Button
                        onClick={openRemove}
                        colorScheme="second-pink"
                        title={i18next.t('organizations.branch.dialog.edit.button.remove')}
                    />
                </div>
            </div>

            <EditBranchForm
                onSubmit={handleSubmit}
                cancelClick={closeEditBranchDialog}
                branch={branch}
                okButtonText={i18next.t('organizations.branch.dialog.edit.button.add')}
            />
        </Dialog>
    )
}

EditBranchDialogPure.propTypes = {
    editBranch: PropTypes.func,
    opened: PropTypes.bool,
    closeEditBranchDialog: PropTypes.func,
    resetForm: PropTypes.func,
    match: PropTypes.object,
    organizations: PropTypes.array,
    branchId: PropTypes.number,
    openRemoveBranchDialog: PropTypes.func,
}

EditBranchDialogPure.defaultProps = {
    editBranch: _.noop,
    opened: false,
    closeEditBranchDialog: _.noop,
    resetForm: _.noop,
    match: {},
    organizations: [],
    branchId: 0,
    openRemoveBranchDialog: _.noop,
}

const mapStateToProps = () => createStructuredSelector({
    opened: selectors.branches.getIsOpenEditBranchDialog,
    branchId: selectors.branches.getEditBranchDialogId,
})

const mapDispatchToProps = (dispatch) => ({
    editBranch: (param) => dispatch(actions.branches.editBranch(param)),
    closeEditBranchDialog: () => dispatch(actions.branches.closeEditBranchDialog()),
    openRemoveBranchDialog: (branch) => dispatch(actions.branches.openRemoveBranchDialog(branch)),
    resetForm: (formName) => dispatch(reset(formName)),
})

export const EditBranchDialog = connect(mapStateToProps, mapDispatchToProps)(withRouter(EditBranchDialogPure))
