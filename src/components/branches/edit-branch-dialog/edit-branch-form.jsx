import { reduxForm } from 'redux-form'
import _ from 'lodash'
import { connect } from 'react-redux'

import { CreateBranchFormPure, validate } from '../create-branch-dialog'

export const FORM_NAME = 'EditBranchForm'

export const EditBranchFormRF = reduxForm({
    form: FORM_NAME,
    validate,
    enableReinitialize: true,
})(CreateBranchFormPure)

export const EditBranchForm = connect(
    (state, ownParam) => {
        const initialValues = {}

        initialValues.name = _.get(ownParam, 'branch.name', '')
        initialValues.address = _.get(ownParam, 'branch.address', '')
        initialValues.kpp = _.get(ownParam, 'branch.kpp', '')

        return {
            initialValues: { ...initialValues }
        }
    }, null, null, { forwardRef: true }
)(EditBranchFormRF)
