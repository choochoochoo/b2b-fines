import React from 'react'
import { reduxForm } from 'redux-form'
import i18next from 'i18next'
import PropTypes from 'prop-types'
import _ from 'lodash'

import { InputField, Button } from '../../general'
import { KPP_REGEXP } from '../../organizations/edit-organization-dialog'

import style from './create-branch-form.css'

export const FORM_NAME = 'CreateBranchForm'

export const validate = (values) => {
    const errors = {}

    if (!values.name) {
        errors.name = i18next.t('input.field.required')
    }

    if (!values.address) {
        errors.address = i18next.t('input.field.required')
    }

    if (!values.kpp) {
        errors.kpp = i18next.t('input.field.required')
    } else if (!KPP_REGEXP.test(values.kpp)) {
        errors.kpp = i18next.t('organizations.organization.dialog.edit.input.kpp.error')
    }

    return errors
}

export const CreateBranchFormPure = (props) => {
    const { handleSubmit, valid, cancelClick, okButtonText } = props

    return (
        <form onSubmit={handleSubmit} >
            <InputField
                name="name"
                required
                type="text"
                placeholder={i18next.t('organizations.branch.dialog.create.input.name')}
                label={i18next.t('organizations.branch.dialog.create.input.name')}
            />

            <InputField
                name="address"
                required
                type="text"
                placeholder={i18next.t('organizations.branch.dialog.create.input.address')}
                label={i18next.t('organizations.branch.dialog.create.input.address')}
            />

            <InputField
                name="kpp"
                required
                type="text"
                placeholder={i18next.t('organizations.branch.dialog.create.input.kpp')}
                label={i18next.t('organizations.branch.dialog.create.input.kpp')}
            />

            <Button
                colorScheme="big-green"
                title={okButtonText}
                disabled={!valid}
                type="submit"
                className={style.saveButton}
            />

            <Button
                onClick={cancelClick}
                colorScheme="big-second"
                title={i18next.t('organizations.branch.dialog.create.button.cancel')}
                type="submit"
            />
        </form>
    )
}

CreateBranchFormPure.propTypes = {
    valid: PropTypes.bool,
    handleSubmit: PropTypes.func,
    cancelClick: PropTypes.func,
    okButtonText: PropTypes.string,
}

CreateBranchFormPure.defaultProps = {
    valid: false,
    handleSubmit: _.noop,
    cancelClick: _.noop,
    okButtonText: '',
}

export const CreateBranchForm = reduxForm({
    form: FORM_NAME,
    validate,
    enableReinitialize: true,
})(CreateBranchFormPure)

