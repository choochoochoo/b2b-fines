import React from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'
import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux'
import i18next from 'i18next'
import { reset } from 'redux-form'
import { withRouter } from 'react-router'

import { selectors, actions } from '../../../__data__'
import { Dialog } from '../../general'

import { CreateBranchForm, FORM_NAME } from './create-branch-form'
import style from './create-branch-dialog.css'


export const CreateBranchDialogPure = (
    {
        createBranch,
        opened,
        closeCreateBranchDialog,
        resetForm,
        match,
    }) => {

    function close () {
        closeCreateBranchDialog()
        resetForm(FORM_NAME)
    }

    function handleSubmit (values) {
        createBranch({
            organizationId: _.get(match, 'params.id'),
            name: _.trim(values.name),
            address: _.trim(values.address),
            kpp: _.trim(values.kpp),

            onSuccessHandler: () => close(),
            onErrorHandler: () => close(),
        })
    }

    return (
        <Dialog opened={opened} width="624px" closeClick={close} hideCloseButton>
            <div className={style.header}>
                <div className={style.title}>
                    {i18next.t('organizations.branch.dialog.create.title')}
                </div>
            </div>

            <CreateBranchForm
                onSubmit={handleSubmit}
                cancelClick={closeCreateBranchDialog}
                okButtonText={i18next.t('organizations.branch.dialog.create.button.add')}
            />
        </Dialog>
    )
}

CreateBranchDialogPure.propTypes = {
    createBranch: PropTypes.func,
    opened: PropTypes.bool,
    closeCreateBranchDialog: PropTypes.func,
    resetForm: PropTypes.func,
    match: PropTypes.object,
}

CreateBranchDialogPure.defaultProps = {
    createBranch: _.noop,
    opened: false,
    closeCreateBranchDialog: _.noop,
    resetForm: _.noop,
    match: {},
}

const mapStateToProps = () => createStructuredSelector({
    opened: selectors.branches.getIsOpenCreateBranchDialog,
})

const mapDispatchToProps = (dispatch) => ({
    createBranch: (param) => dispatch(actions.branches.createBranch(param)),
    closeCreateBranchDialog: () => dispatch(actions.branches.closeCreateBranchDialog()),
    resetForm: (formName) => dispatch(reset(formName)),
})

export const CreateBranchDialog = connect(mapStateToProps, mapDispatchToProps)(withRouter(CreateBranchDialogPure))
