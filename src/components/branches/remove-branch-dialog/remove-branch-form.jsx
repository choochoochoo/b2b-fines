import React from 'react'
import { reduxForm } from 'redux-form'
import i18next from 'i18next'
import PropTypes from 'prop-types'
import _ from 'lodash'

import { Button } from '../../general'

import style from './remove-branch-form.css'

const RemoveBranchFormPure = (props) => {
    const { handleSubmit, cancelClick } = props

    return (
        <form onSubmit={handleSubmit}>
            <Button
                colorScheme="dialog-pink"
                title={i18next.t('organizations.branch.dialog.remove.button.remove')}
                type="submit"
                className={style.buttonRemove}
            />

            <Button
                colorScheme="dialog-green"
                title={i18next.t('organizations.branch.dialog.remove.button.cancel')}
                type="button"
                onClick={cancelClick}
            />
        </form>
    )
}

RemoveBranchFormPure.propTypes = {
    cancelClick: PropTypes.func,
    handleSubmit: PropTypes.func,
}

RemoveBranchFormPure.defaultProps = {
    cancelClick: _.noop,
    handleSubmit: _.noop,
}

export const RemoveBranchForm = reduxForm({
    form: 'RemoveBranchForm',
    validate: null,
})(RemoveBranchFormPure)

