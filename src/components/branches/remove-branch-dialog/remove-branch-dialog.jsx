import React from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'
import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux'
import i18next from 'i18next'

import { selectors, actions } from '../../../__data__'
import { Dialog } from '../../general'

import { RemoveBranchForm } from './remove-branch-form'
import style from './remove-branch-dialog.css'

export const RemoveBranchDialogPure = (
    {
        removeBranch,
        opened,
        closeRemoveBranchDialog,
        branchId,
        branchName,
        organizationId,
    }) => {

    function handleSubmit () {
        removeBranch({
            branchId,
            organizationId,
        })
    }

    return (
        <Dialog opened={opened} closeClick={closeRemoveBranchDialog} width="544px">
            <div className={style.title}>
                {i18next.t('organizations.organization.dialog.remove.title')}
                <br />
                {`${branchName} ?`}
            </div>
            <div className={style.message}>
                {i18next.t('organizations.organization.dialog.remove.description')}
            </div>
            <RemoveBranchForm
                onSubmit={handleSubmit}
                cancelClick={closeRemoveBranchDialog}
            />
        </Dialog>
    )
}

RemoveBranchDialogPure.propTypes = {
    removeBranch: PropTypes.func,
    opened: PropTypes.bool,
    closeRemoveBranchDialog: PropTypes.func,
    branchId: PropTypes.number,
    branchName: PropTypes.string,
    organizationId: PropTypes.number,
}

RemoveBranchDialogPure.defaultProps = {
    removeBranch: _.noop,
    opened: false,
    closeRemoveBranchDialog: _.noop,
    branchId: 0,
    branchName: '',
    organizationId: 0,
}

const mapStateToProps = () => createStructuredSelector({
    opened: selectors.branches.getIsOpenRemoveBranchDialog,
    branchId: selectors.branches.getRemoveBranchDialogId,
    branchName: selectors.branches.getRemoveBranchDialogName,
    organizationId: selectors.branches.getRemoveBranchDialogOrganizationId,
})

const mapDispatchToProps = (dispatch) => ({
    removeBranch: (param) => dispatch(actions.branches.removeBranch(param)),
    closeRemoveBranchDialog: () => dispatch(actions.branches.closeRemoveBranchDialog()),
})

export const RemoveBranchDialog = connect(mapStateToProps, mapDispatchToProps)(RemoveBranchDialogPure)
