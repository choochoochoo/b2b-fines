import React from 'react'
import i18next from 'i18next'

import { formatDate, formatSts, formatMoney } from '../general'

import style from './form-builder.css'

export const FormBuilder = (data) => data.map((field) => {
    let answer = null

    switch (field.type) {
        case 'discountDate':
            answer = (
                <div key={field.key}>
                    {i18next.t('fines.details.discount.date.from')} {formatDate(field.value)}
                </div>
            )

            break
        case 'mainSum':
            answer = (
                <div key={field.key} className={style.mainSum}>
                    {formatMoney(field.value)}
                </div>
            )

            break
        case 'sum':
            answer = (
                <div key={field.key}>
                    {formatMoney(field.value)}
                </div>
            )

            break
        case 'sts':
            answer = (
                <div key={field.key}>
                    {formatSts(field.value)}
                </div>
            )

            break
        case 'date':
            answer = (
                <div key={field.key}>
                    {formatDate(field.value)}
                </div>
            )

            break
        default:
            answer = (
                <div key={field.key}>
                    {field.value}
                </div>
            )
            break
    }

    return (
        <div
            key={field.key}
            className={style.row}
        >

            <div className={style.label}>
                {i18next.t(field.label)}
            </div>
            <div className={style.value}>
                {answer}
            </div>
        </div>
    )
})
