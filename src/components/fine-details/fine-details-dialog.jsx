import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'
import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux'
import classnames from 'classnames'
import i18next from 'i18next'

import { selectors, actions } from '../../__data__'
import config from '../../../config.json'
import {
    Accordion,
    Button,
    SvgIcon,
    getStatus,
    getStatusIcon,
    getFullStatus, Dialog,
} from '../general'

import style from './fine-detials-dialog.css'
import { FormBuilder } from './form-builder'
import { openImage } from './utils'

export const FineDetailsDialogPure = (
    {
        getFine,
        uin,
        opened,
        title,
        discountDate,
        actDate,
        vehiclesFields,
        violationFields,
        paymentRequisitesFields,
        otherRequisitesFields,
        closeFineDetailsDialog,
        isFoundByOrganization,
        isNotVehicleInDB,
        hasPhoto,
    }) => {
    useEffect(() => {
        if (uin) {
            getFine({
                uin
            })
        }
    }, [uin])

    function openPhoto () {
        const URL = `/api/fines/photo/${uin}`
        openImage(`${config.host}${URL}`)
    }

    const pdfUrl = `/api/fines/pdf/${uin}`

    const statusRes = getStatus(discountDate, actDate)

    return (
        <Dialog
            opened={opened}
            width="624px"
            closeClick={closeFineDetailsDialog}
            hideCloseButton
            dialogClass={style.dialogClass}
        >
            <div className={style.title}>
                {title}
            </div>
            <div className={style.description}>
                <div className={style.statusIcon}>
                    <SvgIcon
                        icon={getStatusIcon(statusRes.status)}
                    />
                </div>
                {getFullStatus(statusRes, discountDate, actDate)}
            </div>
            <button className={style.buttonClose} onClick={closeFineDetailsDialog}>
                <SvgIcon
                    icon="closeDialog"
                    width="20"
                    height="20"
                />
            </button>
            <hr className={style.line} />

            <div className={style.panelTitle}>
                {i18next.t('fines.details.vehicle.panel.title')}
            </div>
            <div>
                { FormBuilder(vehiclesFields) }
                { (isFoundByOrganization || isNotVehicleInDB) &&
                    <div className={style.warning}>
                        {
                            isFoundByOrganization && <div>
                                {i18next.t('fines.details.other.requisites.panel.fine.found.by.organization')}
                            </div>
                        }
                        {
                            isNotVehicleInDB && <div>
                                {i18next.t('fines.details.other.requisites.panel.vehicle.is.not.in.db')}
                            </div>
                        }
                    </div>
                }
            </div>

            <div className={classnames(style.panelTitle, style.detailsPanel)}>
                {i18next.t('fines.details.violation.panel.title')}
                {
                    hasPhoto &&
                    <Button
                        title={i18next.t('fines.details.violation.panel.button.title')}
                        onClick={openPhoto}
                    />
                }
            </div>
            <div>
                { FormBuilder(violationFields) }
            </div>

            <Accordion
                title={i18next.t('fines.details.payment.requisites.panel.title')}
                collapsed
            >
                {FormBuilder(paymentRequisitesFields)}
            </Accordion>

            <Accordion
                title={i18next.t('fines.details.other.requisites.panel.title')}
                collapsed
            >
                {FormBuilder(otherRequisitesFields)}
            </Accordion>

            <div className={style.downloadButtonWrapper}>
                <a
                    href={pdfUrl}
                    className={style.downloadButton}
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    <div className={style.downloadButtonText}>
                        {i18next.t('fines.details.status.button.download.pdf')}
                    </div>
                </a>
            </div>
        </Dialog>
    )
}

FineDetailsDialogPure.propTypes = {
    getFine: PropTypes.func,
    uin: PropTypes.string,
    opened: PropTypes.bool,
    title: PropTypes.string,
    discountDate: PropTypes.string,
    actDate: PropTypes.string,
    vehiclesFields: PropTypes.array,
    violationFields: PropTypes.array,
    paymentRequisitesFields: PropTypes.array,
    otherRequisitesFields: PropTypes.array,
    closeFineDetailsDialog: PropTypes.func,
    isFoundByOrganization: PropTypes.bool,
    isNotVehicleInDB: PropTypes.bool,
    hasPhoto: PropTypes.bool,
}

FineDetailsDialogPure.defaultProps = {
    getFine: _.noop,
    uin: '',
    opened: false,
    title: '',
    discountDate: '',
    actDate: '',
    vehiclesFields: [],
    violationFields: [],
    paymentRequisitesFields: [],
    otherRequisitesFields: [],
    closeFineDetailsDialog: _.noop,
    isFoundByOrganization: false,
    isNotVehicleInDB: false,
    hasPhoto: false,
}

const mapStateToProps = () => createStructuredSelector({
    title: selectors.fineDetails.getTitle,
    discountDate: selectors.fineDetails.getDiscountDate,
    actDate: selectors.fineDetails.getActDate,
    vehiclesFields: selectors.fineDetails.getVehiclesFields,
    violationFields: selectors.fineDetails.getViolationFields,
    paymentRequisitesFields: selectors.fineDetails.getPaymentRequisitesFields,
    otherRequisitesFields: selectors.fineDetails.getOtherRequisitesFields,
    opened: selectors.fineDetails.getIsDialogOpen,
    uin: selectors.fineDetails.getUin,
    isFoundByOrganization: selectors.fineDetails.getIsFoundByOrganization,
    isNotVehicleInDB: selectors.fineDetails.getIsNotVehicleInDB,
    hasPhoto: selectors.fineDetails.getHasPhoto,
})

const mapDispatchToProps = (dispatch) => ({
    getFine: (param) => dispatch(actions.getFine(param)),
    closeFineDetailsDialog: () => dispatch(actions.closeFineDetailsDialog()),
})

export const FineDetailsDialog = connect(mapStateToProps, mapDispatchToProps)(FineDetailsDialogPure)
