import _ from 'lodash'

export const openImage = (url) => {
    const cfg = {
        status: 'no',
        toolbar: 'no',
        dependent: 'no',
        location: 'no',
        scrollbars: 'no',
        menubar: 'no',
        resizable: 'yes',
        screenX: 50,
        screenY: 50,
        width: 600,
        height: 600,
        overflow: 'hidden'
    }

    const configWin = _.map(cfg, (val, ind) => `${ind}=${val}`).join(',')

    const nameDoc = 'Image of fine'

    const printWindow = window.open(
        url,
        nameDoc,
        configWin
    )

    printWindow.focus()
}
