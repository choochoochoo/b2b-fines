import React from 'react'
import { render } from 'react-dom'
import { Router, Route, Switch } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import { Provider } from 'react-redux'
import i18next from 'i18next'

import ruLang from '../locales/ru.json'

import store from './__data__/store'
import {
    Fines,
    Vehicles,
    Login,
    Group,
    Organizations,
    Faq,
} from './components'
import * as actions from './__data__/actions'

export const HISTORY = createBrowserHistory()

store.dispatch(actions.getProfile({
    onFailHandler: (response) => {
        if (response.status === 401) {
            HISTORY.push('/')
        }
    }
}))

function runMyApp () {
    i18next.init({
        lng: 'ru',
        resources: ruLang,
        keySeparator: false,
    }).then(() => {
        render(
            <Provider store={store}>
                <Router history={HISTORY}>
                    <Switch>
                        <Route exact path="/" component={Login} />
                        <Route exact path="/login" component={Login} />
                        <Route exact path="/fines" component={Fines} />
                        <Route exact path="/vehicles" component={Vehicles} />
                        <Route path="/organizations/:id?" component={Organizations} />
                        <Route exact path="/groups/:id" component={Group} />
                        <Route exact path="/faq" component={Faq} />
                    </Switch>
                </Router>
            </Provider>,
            document.getElementById('root')
        )
    })
}
if (!global.Intl) {
    require.ensure([
        'intl',
        'intl/locale-data/jsonp/en.js'
    ], (require) => {
        require('intl')
        require('intl/locale-data/jsonp/en.js')
        runMyApp()
    })
} else {
    runMyApp()
}
