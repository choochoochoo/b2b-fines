docker build -t choochoochoo/b2b-fines .

docker run -it choochoochoo/b2b-fines sh

docker run -p 80:9096 -d  choochoochoo/b2b-fines:latest

docker push choochoochoo/b2b-fines

java -jar test.jar ./fine-bill-template.html fine-bill.pdf

java -jar test.jar ./fine-details-template.html fine-details.pdf


docker container ls
docker rm -f <container-name>
