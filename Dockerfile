FROM node:alpine
WORKDIR /usr/app/front

# соберем статику

RUN mkdir temp
RUN mkdir public
COPY . ./temp
COPY ./config.production.json ./temp/config.json
WORKDIR /usr/app/front/temp
RUN npm run build
RUN cp -r ./public/www/js ../public
RUN cp -r ./public/www/img ../public
WORKDIR /usr/app/front
RUN rm -rf temp

# соберем сервер

COPY ./server ./
RUN npm install
CMD ["npm", "start"]

EXPOSE 9096
