const TerserPlugin = require('terser-webpack-plugin')
const merge = require('webpack-merge')
const baseWebpackConfig = require('./webpack.config')

const buildWebpackConfig = merge(baseWebpackConfig, {
    mode: 'production',
    optimization: {
        minimizer: [new TerserPlugin()],
    },
})

module.exports = new Promise((resolve) => {
    resolve(buildWebpackConfig)
})
