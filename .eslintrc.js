module.exports = {
  parser: 'babel-eslint',
  extends: [
    './eslint-config',
  ],
  // settings: {
  //   'import/resolver': {
  //     webpack: {
  //       config: './webpack.config.js'
  //     }
  //   }
  // },
  rules: {
    "global-require": 0,
    "no-console": 0,
    "import/no-dynamic-require": 0,
    "no-magic-numbers": 0,
    "no-sync": 0,
    "no-underscore-dangle": 0
  }
}
