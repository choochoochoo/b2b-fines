const path = require('path')
const postcssPresetEnv = require('postcss-preset-env')
const marked = require('marked')

const renderer = new marked.Renderer()

module.exports = {
    entry: [
        './src/index.jsx'
    ],
    output: {
        path: path.join(__dirname, 'public/www/'),
        filename: 'js/bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                resolve: {
                    extensions: ['.js', '.jsx']
                },
                use: {
                    loader: 'babel-loader',
                },
            },
            { test: /\.svg$/, loader: 'svg-inline-loader' },
            {
                test: /\.css$/,
                use: [
                    require.resolve('style-loader'),
                    {
                        loader: require.resolve('css-loader'),
                        options: {
                            importLoaders: 1,
                            modules: {
                                localIdentName: '[name]__[local]___[hash:base64:5]'
                            },
                        },
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            ident: 'postcss',
                            plugins: () => [
                                postcssPresetEnv(),
                                require('postcss-nested')
                            ]
                        }
                    }
                ]
            },
            {
                test: /\.(png|jpe?g|gif)$/i,
                loader: require.resolve('file-loader'),
                options: {
                    name: 'img/[hash:base64:5].[ext]'
                }
            },
            {
                test: /\.md$/,
                use: [
                    {
                        loader: 'html-loader'
                    },
                    {
                        loader: 'markdown-loader',
                        options: {
                            pedantic: true,
                            renderer
                        }
                    }
                ]
            }
        ]
    },
    devServer: {
        historyApiFallback: {
            index: '/',
        },
        proxy: {
            '/api/**': {
                target: 'http://localhost:3000',
                // target: 'http://mamapapa.pw:9090',
                secure: false,
                changeOrigin: true
            },
        }
    },
}
