const express = require('express')
const utils = require('./utils')
const packageJson = require('./package.json')
const config = require('./config.json')
const cors = require('cors')
const app = express()
const proxy = require('express-http-proxy')

const http = require('http')
const https = require('https')
const fs = require('fs')

app.use(cors())
app.use('/static', express.static(__dirname + '/public'))
app.use('/img', express.static(__dirname + '/public/img'))
app.use('/favicons', express.static(__dirname + '/public/favicons'))

app.set('view engine', 'ejs')

const renderStub = (response) => {
    response.end(utils.getHtml('/views/index.ejs', { bundle_version: packageJson.version }))
}

app.use('/api', proxy(config.host, {
    proxyReqPathResolver: function (req) {
        return `/api${req.url}`
    }
}))

app.get('/', function (request, response) {
    renderStub(response)
})

app.get('/login', function (request, response) {
    renderStub(response)
})

app.get('/vehicles', function (request, response) {
    renderStub(response)
})

app.get('/fines', function (request, response) {
    renderStub(response)
})

app.get('/faq', function (request, response) {
    renderStub(response)
})

app.get('/groups/*', function (request, response) {
    renderStub(response)
})

app.get('/organizations', function (request, response) {
    renderStub(response)
})

app.get('/organizations/*', function (request, response) {
    renderStub(response)
})

const serverPortHttp = 9096
const serverPortHttps = 9097

console.log(`http server started on ${serverPortHttp}`)
console.log(`https server started on ${serverPortHttps}`)

// console.log(`debug mode ${process.env.DEBUG}`)

// app.listen(serverPortHttp)

process.on('uncaughtException', function (err) {
    console.log('Caught exception: ' + err)
})

const options = {
    key: fs.readFileSync('certificate/private.pem'),
    cert: fs.readFileSync('certificate/flicky.cert')
}

if (process.env.DEBUG === 'true') {
    app.listen(serverPortHttp)
} else {
    http.createServer(function (req, res) {
        res.writeHead(301, { 'Location': 'https://' + req.headers['host'] + req.url })
        res.end()
    }).listen(serverPortHttp)

    https.createServer(options, app, (req, res) => {
        res.writeHead(200)
        res.end('hello world\n')
    }).listen(serverPortHttps)
}
