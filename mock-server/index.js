const express = require("express")
const app = express()
const path = require('path')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')

app.use(cookieParser())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.get('/api/profiles', (req, res) => {
    // setTimeout(() => {
    //     if(req.cookies['test-session']) {
    //         const pathToFile = path.resolve(
    //             __dirname,
    //             `./jsons/profiles.json`
    //         )
    //
    //         res.header('Content-Type', 'application/json').sendFile(pathToFile)
    //     } else {
    //         res.status(401).send({ status: false, error: 'Не авторизован' })
    //     }
    // }, 500)

    const pathToFile = path.resolve(
        __dirname,
        `./jsons/profiles.json`
    )

    res.header('Content-Type', 'application/json').sendFile(pathToFile)
})

app.post('/api/login', (req, res) => {
    setTimeout(() => {
        if(req.body.username === 'test') {
            res.cookie('test-session', 'my-user', { maxAge: 900000, httpOnly: true })

            res.send({
                "status": true,
            })
        } else {
            res.send({
                "status": false,
                "error": "Неверная учетка"
            })
        }

        // res.send({
        //     "status": true,
        // })

    }, 500)
})

app.get('/api/logout', (req, res) => {
    res.cookie('test-session', 'my-user', { maxAge: 1 })

    setTimeout(() => {
        res.send({
            'status': true,
        })
    }, 500)
})

app.post('/api/organizations', (req, res) => {
    const pathToFile = path.resolve(
        __dirname,
        './jsons/organizations/create.json'
    )

    setTimeout(() => {
        res.header('Content-Type', 'application/json').sendFile(pathToFile)
    }, 500)
})

app.all('/api/*', (req, res) => {
    const fileName = req.params[0]

    const page = req.query.paginationOffset
    const suffixPage = page ? `-${page}` : ''

    const size = req.query.paginationSize
    const suffixSize = size ? `-${size}` : ''

    const sort = req.query.sort
    const suffixSort = sort ? `-${sort}` : ''

    const pathToFile = path.resolve(
        __dirname,
        `./jsons/${fileName}${suffixPage}${suffixSize}${suffixSort}.json`
    )

    // res.status(500).send({ status: false, error: 'Сервер не доступен' })

    setTimeout(() => {
        res.header('Content-Type', 'application/json').sendFile(pathToFile)
    }, 500)
})

app.listen(3000)
